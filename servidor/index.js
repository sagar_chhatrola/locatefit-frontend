import express from 'express';
import path from 'path';
import bcrypt from 'bcrypt';
import { Usuarios, Ordenes, Conversation } from './data/db';
import { Types } from 'mongoose';
const { ObjectId } = Types;
import crypto from 'crypto';
import recoverPasswordEmail from './emails/recoverPasswordEmail';
// Conectamos graphql con express por HTTP
import { ApolloServer } from 'apollo-server-express';
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  google_customer_secret,
  twiterr_api_key,
  twitter_api_secret,
  FACEBOOK_SECRET
} from './config.js';
// Importamos el schema de datos
import { typeDefs } from './data/schema';
import { resolvers } from './data/resolvers';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { Productos } from './data/db';
import { getMaxListeners } from 'cluster';
import { chargeToken } from './services/stripe';
import cons from 'consolidate';
const nodemailer = require('nodemailer');
dotenv.config({ path: 'variables.env' });
const request = require('request');
const engines = require("consolidate");
const paypal = require("paypal-rest-sdk");
const http  = require("http")
var socketIO = require('socket.io');


var passport = require('passport');
require('./passport')();

const app = express();
app.use(express.static(__dirname + 'public'));
app.use(express.static(path.resolve(__dirname, './../cliente/build')))
app.engine("ejs", engines.ejs);
app.set("views", "./views");
app.set("view engine", "ejs");


//////////////socketio////////////////

/*io.on('connection', socket => {
  console.log('a user connected');
  socket.on('chat messaje', msg => {
    console.log(msg);
    socket.emit('chat message', msg)
  })
});*/


//////////paypal payment app /////////////

app.get("/payment-paypal-app", (req, res) => {
  res.render("index");
});

paypal.configure({
  mode: "live", //sandbox or live
  client_id:
      "AbIQ9Xh0c1qJBUjzLFCVVDnIACiJUwtFiZ02Et76OXoqpTDc92_aqPlhzhBHm2_iIj4F5_ebNE_M9eZD",
  client_secret:
      "EJEfFLaULf4aczbxfYYxuZTCEYaXg_Bzd6meHU1xvqddm6BcQlW_EAHmKuzYbYDj3DaLltYC_WeFfmEn"
});

app.get("/paypal", (req, res) => {
  const { price, order } = req.query;
  var create_payment_json = {
      intent: "sale",
      payer: {
          payment_method: "paypal"
      },
      redirect_urls: {
          return_url: `http://localhost:4000/success?price=${price}&order=${order}`,
          cancel_url: "http://localhost:4000/cancel"
      },
      transactions: [
          {
            amount: {
              currency: "EUR",
              total: req.query.price
                    },
              description: "Locatefit S.L"
          }
      ]
  };

  paypal.payment.create(create_payment_json, function(error, payment) {
      if (error) {
          throw error;
      } else {
          console.log("Create Payment Response");
          console.log(payment);
          res.redirect(payment.links[1].href);
      }
  });
});

app.get("/success", (req, res) => {
  var PayerID = req.query.PayerID;
  var paymentId = req.query.paymentId;
  const { price, order } = req.query;
  var execute_payment_json = {
      payer_id: PayerID,
      transactions: [
          {
              amount: {
                  currency: "EUR",
                  total: price
              }
          }
      ]
  };

  paypal.payment.execute(paymentId, execute_payment_json, function(
    error,
    payment
) {
    if (error) {
        console.log(error.response);
        throw error;
    } if(payment){
          Ordenes.findOneAndUpdate({_id:ObjectId(order)},{$set:{estado: 'Nueva', progreso: '50', status: 'active',pagoPaypal:payment}},(err,order)=>{
            res.render("success");
          })
          console.log("Get Payment Response");
          console.log(payment);
      }
  });
});

app.get("/cancel", (req, res) => {
  res.render("cancel");
});

//////////paypal payment app end /////////////

const bodyParser = require('body-parser');

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const client = require('twilio')(accountSid, authToken);

app.use(
  bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended: true
  })
);

app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }));

app.use('/assets', express.static(path.join(__dirname, 'uploads')));

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers['authorization'];

    if (token !== 'null') {
      try {
        // verificarl el token que viene del cliente
        const usuarioActual = await jwt.verify(token, process.env.SECRETO);
        req.usuarioActual = usuarioActual;

        return {
          usuarioActual
        };
      } catch (err) {
        // console.log('err: ', err);
      }
    }
  }
});

server.applyMiddleware({ app });

app.use(function(req, res, next) {
  //to allow cross domain requests to send cookie information.
  res.header('Access-Control-Allow-Credentials', true);

  // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
  res.header('Access-Control-Allow-Origin', req.headers.origin);

  // list of methods that are supported by the server
  res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');

  res.header(
    'Access-Control-Allow-Headers',
    'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN'
  );

  next();
});

//////////////////////

var createToken = function(auth) {
  return jwt.sign(
    {
      id: auth.id
    },
    process.env.SECRETO,
    {
      expiresIn: 60 * 120
    }
  );
};

function generateToken(req, res, next) {
  req.token = createToken(req.auth);
  return next();
}
function sendToken(req, res) {
  res.setHeader('x-auth-token', req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

  app.get('/send-message', (req, res) =>{
    const { recipient, textmessage } = req.query
    client.messages
      .create({
        body: textmessage,
        from: '+16027866941',
        to: '+34' + recipient
      })
      .then(message => console.log(message.sid));
  })

  app.get('/save-userid-notification', (req, res) =>{
    const { UserId, id } = req.query
    if(UserId && id){
      Usuarios.findOneAndUpdate({_id:ObjectId(id)},{$set:{UserID: UserId}},(err,user)=>{
      }).catch(err=>{
        console.log(err)
      })
    }
  })

  app.get('/send-push-notification', (req, res) =>{
    const { IdOnesignal, textmessage } = req.query
    console.log('esta es la noti push', IdOnesignal, textmessage)
    var sendNotification = function(data) {
      var headers = {
        "Content-Type": "application/json; charset=utf-8"
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
      };
      var https = require('https');
      var req = https.request(options, function(res) {  
        res.on('data', function(data) {
          console.log("Response:");
          console.log(JSON.parse(data));
        });
      });
      
      req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
      });
      
      req.write(JSON.stringify(data));
      req.end();
    };
    
    var message = { 
      app_id: "d7778157-666e-4d2b-a840-e9f8d1423251",
      contents: {"en": `"${textmessage}"`},
      ios_badgeCount: 1,
      ios_badgeType: "Increase",
      include_player_ids: [`${IdOnesignal}`]
    };
    sendNotification(message);
  })


///////////////////////???/////
// Stripe charge token

app.post('/stripe/chargeToken',(req,res)=>{
  let data = req.body
  if(data.stripeToken && data.amount!=undefined){
    chargeToken(data.amount,data.stripeToken).then(response=>{
        console.log(response)
        if(response.status == 'succeeded'){
          Ordenes.findOneAndUpdate({_id:ObjectId(data.orderId)},{$set:{estado: 'Nueva', progreso: '50', status: 'active',stripePaymentIntent:response}},(err,order)=>{
            res.status(200).send({success:true,data:response})
          })
        }
        else{
          res.status(401).send({success:false,data:response})
        }
    }).catch(err=>{
      console.log(err)
      res.status(401).send({success:false,error:err})
    })
  }
  else{
    res.status(403).send({success:false,error:'Invalid token'})
  }
})


///////////////////////???/////

app.post('/forgotpassword', (req, res) => {
  const email = req.body.email;
  Usuarios.find({ email: email }, (err, data) => {
    if (data.length < 1 || err) {
      res.send({
        noEmail: true,
        message: 'Aún no tenemos este correo eletrónico'
      });
    } else {
      const token = crypto.randomBytes(20).toString('hex');

      // write to database

      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.EMAIL_ADDRESS,
          pass: `${process.env.EMAIL_PASSWORD}`
        }
      });
      console.log(recoverPasswordEmail(token));
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: 'Recuperar contraseña Locatefit',
        text: 'Recuperar contraseña Locatefit',
        html: recoverPasswordEmail(token)
      };

      console.log('Sending');
      transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
          console.log('err:', err);
        } else {
          var newvalues = { $set: { forgotPasswordToken: token } };

          Usuarios.findOneAndUpdate(
            { email: email },
            newvalues,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false
            },
            (err, updated) => {
              console.log(updated);
              res.status(200).json({ message: 'Email Sent' });
            }
          );
        }
      });
    }
  });
});

app.post('/tokenValidation', (req, res) => {
  Usuarios.find({ forgotPasswordToken: req.body.token }, (err, data) => {
    if (err || data.length < 1) {
      if (err) console.log(err);
      res.status(200).json({ isValid: false, email: '' });
    } else if (data.length > 0) {
      res.status(200).json({ isValid: true, email: data[0].email });
    }
  });
});

app.post('/resetPassword', (req, res) => {
  var newvalues = { $unset: { forgotPasswordToken: req.body.token } };

  Usuarios.findOneAndUpdate(
    { email: req.body.email },
    newvalues,
    {
      //options
      returnNewDocument: true,
      new: true,
      strict: false,
      useFindAndModify: false
    },
    (err, updated) => {
      if (err) console.log(err);
      bcrypt.genSalt(10, (err, salt) => {
        if (err) console.log(err);

        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) console.log(err);
          var newvalues2 = { $set: { password: hash } }; // "5db997597eb2771b2a1e062d"
          Usuarios.findOneAndUpdate(
            { email: req.body.email },
            newvalues2,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false
            },
            (err, updated) => {
              if (err) console.log(err);

              console.log(updated);
              res.status(200).json({ changed: true });
            }
          );
        });
      });
    }
  );
});

///// conform email
app.post('/verfiyEmail', (req, res) => {
  // req.body.token
  var newvalues = {
    $unset: { verificationToken: req.body.token, isNotVerified: true }
  };

  Usuarios.findOneAndUpdate(
    { verificationToken: req.body.token },
    newvalues,
    {
      //options
      returnNewDocument: true,
      new: true,
      strict: false,
      useFindAndModify: false
    },
    (err, updated) => {
      if (err) return res.json({ verified: false });

      console.log(updated);
      res.status(200).json({ verified: true });
    }
  );
});

app.post('/api/v1/auth/twitter/reverse', function(req, res) {
  request.post(
    {
      url: 'https://api.twitter.com/oauth/request_token',
      oauth: {
        oauth_callback: 'https%3A%2F%2Flocalhost%3A3000%2Flogin',
        consumer_key: twiterr_api_key,
        consumer_secret: twitter_api_secret
      }
    },
    function(err, r, body) {
      if (err) {
        return res.send(500, { message: e.message });
      }
      var jsonStr =
        '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
      res.send(JSON.parse(jsonStr));
    }
  );
});

app.post(
  '/api/v1/auth/twitter',
  (req, res, next) => {
    request.post(
      {
        url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
        oauth: {
          consumer_key: twiterr_api_key,
          consumer_secret: twitter_api_secret,
          token: req.query.oauth_token
        },
        form: { oauth_verifier: req.query.oauth_verifier }
      },
      function(err, r, body) {
        if (err) {
          return res.send(500, { message: err.message });
        }

        const bodyString =
          '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
        const parsedBody = JSON.parse(bodyString);

        req.body['oauth_token'] = parsedBody.oauth_token;
        req.body['oauth_token_secret'] = parsedBody.oauth_token_secret;
        req.body['user_id'] = parsedBody.user_id;

        next();
      }
    );
  },
  passport.authenticate('twitter-token', { session: false }),
  function(req, res, next) {
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }
    req.auth = {
      id: req.user.id
    };

    return next();
  },
  generateToken,
  sendToken
);

app.post(
  '/api/v1/auth/facebook',
  passport.authenticate('facebook-token', { session: false }),
  function(req, res, next) {
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }
    req.auth = {
      id: req.user.id
    };

    next();
  },
  generateToken,
  sendToken
);

app.post(
  '/api/v1/auth/google',
  passport.authenticate('google-token', { session: false }),
  function(req, res, next) {
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }
    req.auth = {
      id: req.user.id
    };

    next();
  },
  generateToken,
  sendToken
);

app.post(
  '/api/v1/auth/social/mobile',
  async function(req, res, next) {
    let data = req.body

    require('mongoose')
          .model('usuarios')
          .schema.add({ isSocial: Boolean });

        // // check if email exists
        let emailExists = await Usuarios.findOne({
          email: data.email
        });
       
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(data.token, salt, (err, hash) => {
              if (err) console.log(err);
              Usuarios.findOneAndUpdate(
                { email: data.email },
                { password: hash },
                (err, updated) => {
                  if (err) {
                    res.status(500).json({error:err})
                  }

                  let nuevoUsuario = updated;
                  res.json({ nuevoUsuario, token: data.token })
                }
              );
            });
          });
    }
    else{
      const nuevoUsuario = await new Usuarios({
        usuario: data.email,
        password: data.token,
        email: data.email,
        nombre:data.firstName,
        apellidos: data.lastName,
        isSocial: true
      });
  
      nuevoUsuario.id = nuevoUsuario._id;
  
      nuevoUsuario.save(error => {
        if(error){
          return res.status(500).send({error})
        }
        else{
          return res.json({ nuevoUsuario, token: data.token });
        }
      });
    }
  }
);

app.get('*', (req, res) => {
  res.send('it works')
});


//////////////socketio////////////////

const serversocket = http.createServer(app)
const io = socketIO(serversocket)
let connectedUsers = {};

///here makes the connection
  io.on('connection', socket => {
      ///////here you get the db messages
      socket.on('online_user', id => {
        connectedUsers[id.id] = socket.id;
        Conversation.findOne({cliente: id.cliente, profesional: id.profesional}).exec((err, res) => {
          if(res){
            socket.emit('messages', res)
          }
        })
        console.log(`new user connected with an id of ${id.id}`)
        socket.emit('message', {connected: socket.connected, time: socket.handshake.time, user: connectedUsers})
      })

    //////here he hears the client's message and issues the specified user
      socket.on('private_message', input => {
        io.to(connectedUsers[input.receptor]).emit('messages', input.input);
        Conversation.find({cliente:input.cliente, profesional: input.profesional}).exec((err, res) => {
          const datos = input.input.messagechat
          if(res.length === 0){
            let newMessageChat = new Conversation({
              messagechat:{
                _id: datos[0]._id,
                text: datos[0].text,
                createdAt: datos[0].createdAt,
                user: {
                  _id: datos[0].user._id,
                  name: datos[0].user.name,
                  avatar: datos[0].user.avatar
                }
              },
              profesional: input.profesional,
              cliente: input.cliente,
            })
            //////////here save in db and send to specific user
            newMessageChat.save( (err, res) =>{
              if (err) throw err
            })
          }else{ 
            Conversation.updateOne({_id: res[0]._id},{$push: { messagechat:  { $each: [input.input.messagechat[0]], $position: 0 }}, upsert: true, new: true}).exec((err, res) => {
              if(err){
                console.log(err)
              }
            })
          }
        })

        ////////this is the scheme to save in db
       

      //////////Send Notification////////
      const datos = input.input.messagechat
      if (datos[0]){
        var sendNotification = function(data) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8"
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
          };
          
          var https = require('https');
          var req = https.request(options, function(res) {  
            res.on('data', function(data) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });
          
          req.on('error', function(e) {
            console.log("ERROR:");
            console.log(e);
          });
          
          req.write(JSON.stringify(data));
          req.end();
        };
        var message = { 
          app_id: "d7778157-666e-4d2b-a840-e9f8d1423251",
          headings: {"en": datos[0].user.name},
          contents: {"en": datos[0].text},
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [input.Userid],
        };
        
        sendNotification(message);

      }else{
        return null
      }
      /////////////end///////////////////
    
    })
 /////////////disconnect user///////////////////
    socket.on('disconnect', () => {
        console.log('User disconnect:', socket.id)
    })
});

serversocket.listen({ port: 4000 }, () => {
  console.log(
    `El servidor está corriendo en http://localhost:4000${server.graphqlPath}`
  );
});



