'use strict';
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  google_customer_secret,
  twiterr_api_key,
  twitter_api_secret,
  FACEBOOK_SECRET
} from './config.js';
import bcrypt from 'bcrypt';
import { Usuarios } from './data/db';
var passport = require('passport');
var TwitterTokenStrategy = require('passport-twitter-token');
var FacebookTokenStrategy = require('passport-facebook-token');
var GoogleTokenStrategy = require('passport-google-token').Strategy;

module.exports = function() {
  passport.use(
    new TwitterTokenStrategy(
      {
        consumerKey: twiterr_api_key,
        consumerSecret: twitter_api_secret,
        includeEmail: true
      },
      async (token, tokenSecret, profile, done) => {
        require('mongoose')
          .model('usuarios')
          .schema.add({ isSocial: Boolean });

        // // check if email exists
        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value
        });
        const my = { ...emailExists }; // && my._doc.isSocial
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(token, salt, (err, hash) => {
              if (err) console.log(err);
              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: token });
                }
              );
            });
          });
        }

        // // check if username exists
        // const usernameExists = await Usuarios.findOne({ usuario: input.usuario });
        // console.log('usernameExists: ', usernameExists);
        // if (usernameExists) {
        //     return { success: false, message: STATUS_MESSAGES.USERNAME_EXISTS, data: null }
        // }

        const nuevoUsuario = await new Usuarios({
          id: profile.id,
          usuario: profile.emails[0].value,
          password: token,
          email: profile.emails[0].value,
          nombre:
            profile.name.givenName.length > 0
              ? profile.name.givenName
              : profile.username,
          apellidos: profile.name.familyName,
          isSocial: true
        });

        nuevoUsuario.id = nuevoUsuario._id;

        nuevoUsuario.save(error => {
          return done(error, { nuevoUsuario, token: token });
        });
      }
    )
  );

  passport.use(
    new FacebookTokenStrategy(
      {
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_SECRET
      },
      async (accessToken, refreshToken, profile, done) => {
        require('mongoose')
          .model('usuarios')
          .schema.add({ isSocial: Boolean });

        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value
        });
        const my = { ...emailExists }; // && my._doc.isSocial
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);
              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        }

        // // check if username exists
        // const usernameExists = await Usuarios.findOne({ usuario: input.usuario });
        // console.log('usernameExists: ', usernameExists);
        // if (usernameExists) {
        //     return { success: false, message: STATUS_MESSAGES.USERNAME_EXISTS, data: null }
        // }

        const nuevoUsuario = await new Usuarios({
          id: profile.id,
          usuario: profile.emails[0].value,
          password: accessToken,
          email: profile.emails[0].value,
          nombre: profile.name.givenName,
          apellidos: profile.name.familyName,
          isSocial: true
        });
        nuevoUsuario.id = nuevoUsuario._id;

        nuevoUsuario.save(error => {
          return done(error, { nuevoUsuario, token: accessToken });
        });
      }
    )
  );

  passport.use(
    new GoogleTokenStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: google_customer_secret
      },
      async (accessToken, refreshToken, profile, done) => {
        require('mongoose')
          .model('usuarios')
          .schema.add({ isSocial: Boolean });

        // check if email exists
        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value
        });
        const my = { ...emailExists }; // && my._doc.isSocial
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);
              // var newvalues2 = { $set: { password: hash } }; // "5db997597eb2771b2a1e062d"
              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  console.log(updated);
                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        }

        // // check if username exists
        // const usernameExists = await Usuarios.findOne({ usuario: input.usuario });
        // console.log('usernameExists: ', usernameExists);
        // if (usernameExists) {
        //     return { success: false, message: STATUS_MESSAGES.USERNAME_EXISTS, data: null }
        // }

        const nuevoUsuario = await new Usuarios({
          id: profile.id,
          usuario: profile.emails[0].value,
          password: accessToken,
          email: profile.emails[0].value,
          nombre: profile.name.givenName,
          apellidos: profile.name.familyName,
          isSocial: true
        });
        nuevoUsuario.id = nuevoUsuario._id;

        nuevoUsuario.save(error => {
          return done(error, { nuevoUsuario, token: accessToken });
        });
      }
    )
  );
};
