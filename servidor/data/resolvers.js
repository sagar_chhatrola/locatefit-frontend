import bcrypt from 'bcrypt';
import fs, { read } from 'fs';
import crypto from 'crypto';
import confirmEmail from '../emails/confirmEmail';
// Generar token
import dotenv from 'dotenv';
dotenv.config({ path: 'variables.env' });

import jwt from 'jsonwebtoken';
import { Types } from 'mongoose';
const { ObjectId } = Types;
import {
  UsuarioFavoritoProducto,
  UsuarioDireccion,
  UsuarioDeposito,
  UsuarioValoracion,
  UsuarioPago,
  Categories,
  Clientes,
  Productos,
  Usuarios,
  Cupones,
  Ordenes,
  ProductoVisitar,
  Notification,
  Inspiration,
  UsuarioFavoritoInspiracion,
  Conversation
} from './db';
import { STATUS_MESSAGES } from './status-messages';
const nodemailer = require('nodemailer');

const crearToken = (usuarioLogin, secreto, expiresIn) => {
  const { usuario } = usuarioLogin;

  return jwt.sign({ usuario }, secreto, { expiresIn });
};

const actualizarProductoVisitar = producto_id => {
  return new Promise(async (reject, resolve) => {
    if (!producto_id) {
      reject({
        success: false,
        message: STATUS_MESSAGES.PRODUCTO_ID_MISSING
      });
    }

    const date = new Date();
    const mes_id = date.getMonth();
    const ano = date.getFullYear();
    const condition = {
      producto_id: producto_id,
      mes_id,
      ano
    };

    const productoVisitar = await ProductoVisitar.findOne(condition);

    if (productoVisitar) {
      ProductoVisitar.findOneAndUpdate(
        { _id: productoVisitar.id },
        { visitas: ++productoVisitar.visitas },
        error => {
          if (error) {
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG
            });
          } else {
            resolve({
              success: true,
              message: STATUS_MESSAGES.PRODUCTO_VISITAS_UPDATED
            });
          }
        }
      );
    } else {
      const data = { producto_id: producto_id, visitas: 1, mes_id, ano };
      const nuevoProductoVisitar = new ProductoVisitar(data);
      nuevoProductoVisitar.save(error => {
        if (error) {
          reject({
            success: false,
            message: STATUS_MESSAGES.S_WENT_WRONG
          });
        } else {
          resolve({
            success: true,
            message: STATUS_MESSAGES.PRODUCTO_VISITAS_INSERTED
          });
        }
      });
    }
  });
};

export const resolvers = {
  Query: {

    getConversation: (root, { profeional }) => {
      console.log(profeional)
      if (profeional) {
        return new Promise((resolve, reject) => {
          Conversation.find({ $or: [{ "profesional": ObjectId(profeional) }, { "cliente": ObjectId(profeional) }] }).sort({ "$natural": -1 }).exec((error, conversation) => {
            if (error) {
              console.log(error)
              reject({ success: false, message: "Hay un problema con tu solicitud", conversation: [] })
            }
            else {
              resolve({ success: true, message: 'success', conversation: conversation })
              console.log('>>>>>>>>>>>>>>>>>', conversation)
            }
          });
        });
      } else {
        return null
      }
    },

    
  



    getMessageNoRead: (root, { conversationID, profeional }) => {
      if (profeional) {
        return new Promise((resolve, reject) => {
          Conversation.aggregate([
            {
                $match: {
                    "_id": ObjectId(conversationID)
                }
            },
            {
                $unwind: '$messagechat'
            },
            {
                $match: {
                    "messagechat.read" : false,
                }
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1}
                }
            }
        ]).exec((error, conversation) => {
            if (error) {
              console.log(error)
              reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG, conversation: [] })
            }
            else {
              resolve({ success: true, message: 'success', conversation: conversation })
            }
          });
        });
      } else {
        return null
      }
    },



    getUsuarioDeposito: async (root, { page = 0, dateRange }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          total: 0,
          list: []
        };
      }
      const usuario = await Usuarios.findOne({ usuario: usuarioActual.usuario });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          total: 0,
          list: []
        };
      }
      return new Promise((resolve, reject) => {
        const limit = 10;
        let skip = page * (limit - 1);
        let total;
        let condition = { usuarioid: usuario._id };
        if (dateRange && dateRange.fromDate && dateRange.toDate) {
          condition.fecha = { $gte: dateRange.fromDate, $lte: dateRange.toDate };
        }
        UsuarioDeposito.count(condition, (err, count) => {
          if (err) total = 0;
          else total = count;
        });
        if (total == 0)
          resolve({ success: true, message: '', total: 0, list: [] });

        UsuarioDeposito.find(condition)
          .skip(skip)
          .limit(limit)
          .exec((error, usuarioDepositos) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                total: 0,
                list: []
              });
            else
              resolve({
                success: true,
                message: '',
                total: total,
                list: usuarioDepositos
              });
          });
      });
    },
    getUsuarioFavoritoProductos: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        UsuarioFavoritoProducto.find(
          { usuarioId: usuario._id },
          (error, usuarioFavoritoProductos) => {
            if (error) {
              console.log('error ==>: ', error);

              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                list: []
              });
            }
            else {
              // console.log('usuarioFavoritoProductos ==> : ', usuarioFavoritoProductos)
              resolve({
                success: true,
                message: '',
                list: usuarioFavoritoProductos
              });
            }
          }
        );
      });
    },


    getUsuarioFavoritoInspiracion: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        UsuarioFavoritoInspiracion.find(
          { usuarioId: usuario._id },
          (error, usuarioFavoritoInspiracion) => {
            if (error) {
              console.log('error ==>: ', error);

              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                list: []
              });
            }
            else {
              resolve({
                success: true,
                message: '',
                list: usuarioFavoritoInspiracion
              });
            }
          }
        );
      });
    },

    getUsuarioDireccion: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioDireccion.findOne(
          { usuario_id: usuario._id },
          (error, usuarioDireccion) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                data: null
              });
            else
              resolve({ success: true, message: '', data: usuarioDireccion });
          }
        );
      });
    },
    getUsuarioPago: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioPago.findOne(
          { usuario_id: usuario._id },
          (error, usuarioPagos) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                data: null
              });
            else resolve({ success: true, message: '', data: usuarioPagos });
          }
        );
      });
    },
    getUsuario: (root, { id }) => {
      return new Promise((resolve, reject) => {
        Usuarios.findById(id, (error, producto) => {
          if (error) reject(error);
          else resolve(producto);
        });
      });
    },

    getNotifications: (root, { userId }) => {
      if (userId) {
        return new Promise((resolve, reject) => {
          Notification.find({ user: userId, read: false }).populate('user').populate('cliente').populate('profesional').populate('orden').sort({ "$natural": -1 }).exec((error, notification) => {
            if (error) {
              console.log(error)
              reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG, notifications: [] })
            }
            else {
              resolve({ success: true, message: 'success', notifications: notification })
            }
          });
        });
      } else {
        return null
      }
    },

    getStatistics: (root, { userId, userType }) => {
      return new Promise((resolve, reject) => {
        let matchQuery1 = userType == 'cliente' ? { cliente: ObjectId(userId), estado: 'Valorada' } : { profesional: ObjectId(userId), estado: 'Valorada' }
        let matchQuery2 = userType == 'cliente' ? { cliente: ObjectId(userId), estado: 'Devuelto' } : { profesional: ObjectId(userId), estado: 'Devuelto' }
        Ordenes.aggregate([{ $match: matchQuery1 },
        {
          $group: {
            _id: { month: { $substr: ['$created_at', 5, 2] }, estado: 'Valorada' },
            stripePaymentIntent: { "$first": "$stripePaymentIntent" },
            pagoPaypal: { "$first": "$pagoPaypal" },
            paypalAmount: { $sum: "$pagoPaypal.montoPagado" },
            stripeAmount: { $sum: "$stripePaymentIntent.amount" },
            finishedCount: { $sum: 1 },
            created_at: { "$first": "$created_at" }
            ,
          }
        }, { $project: { your_year_variable: { $year: '$created_at' }, paypalAmount: 1, finishedCount: 1, stripeAmountDivide: { $divide: ["$stripeAmount", 100] } } },
        { $match: { your_year_variable: 2020 } }
        ]).then(res => {
          let ordenes = { name: 'Ordenes', 'Ene': 0, 'Feb': 0, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0, 'Nov': 0, 'Dic': 0 }
          let ganacias = { name: 'Ganacias', 'Ene': 0, 'Feb': 0, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0, 'Nov': 0, 'Dic': 0 }
          let devoluciones = { name: 'Devoluciones', 'Ene': 0, 'Feb': 0, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0, 'Nov': 0, 'Dic': 0 }

          for (let i = 0; i < res.length; i++) {
            let month = res[i]._id.month
            if (month == '01') {
              ganacias['Ene'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Ene'] = res[i].finishedCount
            }
            else if (month == '02') {
              ganacias['Feb'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Feb'] = res[i].finishedCount
            }
            else if (month == '03') {
              ganacias['Mar'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Mar'] = res[i].finishedCount
            }
            else if (month == '04') {
              ganacias['Abr'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Abr'] = res[i].finishedCount
            }
            else if (month == '05') {
              ganacias['May'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['May'] = res[i].finishedCount
            }
            else if (month == '06') {
              ganacias['Jun'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Jun'] = res[i].finishedCount
            }
            else if (month == '07') {
              ganacias['Jul'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Jul'] = res[i].finishedCount
            }
            else if (month == '08') {
              ganacias['Aug'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Aug'] = res[i].finishedCount
            }
            else if (month == '09') {
              ganacias['Sep'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Sep'] = res[i].finishedCount
            }
            else if (month == '10') {
              ganacias['Oct'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Oct'] = res[i].finishedCount
            }

            else if (month == '11') {
              ganacias['Nov'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Nov'] = res[i].finishedCount
            }
            else if (month == '12') {
              ganacias['Dic'] = (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) + (isNaN(res[i].stripeAmountDivide) ? 0 : res[i].stripeAmountDivide)
              ordenes['Dic'] = res[i].finishedCount
            }
          }

          Ordenes.aggregate([{ $match: matchQuery2 },
          {
            $group: {
              _id: { month: { $substr: ['$created_at', 5, 2] }, estado: 'Devuelto' },


              returnedCount: { $sum: 1 },
              created_at: { "$first": "$created_at" }
              ,
            }
          }, { $project: { your_year_variable: { $year: '$created_at' }, returnedCount: 1 } },
          { $match: { your_year_variable: 2020 } }
          ]).then(res1 => {
            for (let i = 0; i < res1.length; i++) {
              let month = res1[i]._id.month
              if (month == '01') {
                devoluciones['Ene'] = res1[i].returnedCount
              }
              else if (month == '02') {
                devoluciones['Feb'] = res1[i].returnedCount
              }
              else if (month == '03') {
                devoluciones['Mar'] = res1[i].returnedCount
              }
              else if (month == '04') {
                devoluciones['Abr'] = res1[i].returnedCount
              }
              else if (month == '05') {
                devoluciones['May'] = res1[i].returnedCount
              }
              else if (month == '06') {
                devoluciones['Jun'] = res1[i].returnedCount
              }
              else if (month == '07') {
                devoluciones['Jul'] = res1[i].returnedCount
              }
              else if (month == '08') {
                devoluciones['Aug'] = res1[i].returnedCount
              }
              else if (month == '09') {
                devoluciones['Sep'] = res1[i].returnedCount
              }
              else if (month == '10') {
                devoluciones['Oct'] = res1[i].returnedCount
              }
              else if (month == '11') {
                devoluciones['Nov'] = res1[i].returnedCount
              }
              else if (month == '12') {
                devoluciones['Dic'] = res1[i].returnedCount
              }
            }
            resolve({ success: true, message: "", data: [ordenes, ganacias, devoluciones] })
            //console.log(ordenes,ganacias,devoluciones)
          })
        }).catch(err => {
          console.log(err)
        })

      });
    },
    uploads: (parent, args) => { },
    getCategories: () => {
      return Categories.find({});
    },

    getInspiration: () => {
      return Inspiration.find({}).sort({ "$natural": -1 });;
    },

    getPro: () => {
      return Productos.find();
    },

    getCategory: async (root, { id }) => {
      return new Promise((resolve, reject) => {
        Categories.findById(id, (error, category) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else resolve({ success: true, message: '', data: category });
        }).sort({ "$natural": -1 });
      });
    },

    getProductoCity: async (root, { city }) => {
      return new Promise((resolve, reject) => {
        Productos.find({ city: city }, (error, producto) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else {
            resolve({ success: true, message: '', data: producto });
          }
        }).sort({ "$natural": -1 });
      });
    },

    getProductoChat: async (root, { id }) => {
      return new Promise((resolve, reject) => {
        Productos.find({ _id: id }, (error, producto) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else {
            resolve({ success: true, message: '', data: producto });
          }
        }).sort({ "$natural": -1 });
      });
    },

    getProductoSearch: async (root, { search, city }) => {
      return new Promise((resolve, reject) => {
        Productos.find({ $text: { $search: `"\"${search} \""` }, city: city },
          { score: { $meta: "textScore" } },
          (error, producto) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                data: null
              }, console.log(error));
            else {
              resolve({ success: true, message: '', data: producto });
            }
          }).sort({ score: { $meta: "textScore" } });
      });
    },

    getClientes: () => {
      return Clientes.find({});
    },
    getCliente: (root, { id }) => {
      return new Promise((resolve, object) => {
        Clientes.findById(id, (error, cliente) => {
          if (error) rejects(error);
          else resolve(cliente);
        });
      });
    },

    getProductos: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        Productos.find({ created_by: usuario._id }, (error, productos) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              list: []
            });
          else resolve({ success: true, message: '', list: productos });
        }).sort({ "$natural": -1 });
      });
    },
    getProducto: async (
      root,
      { id, updateProductVisit },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      return new Promise((resolve, reject) => {
        if (updateProductVisit) actualizarProductoVisitar(id);
        Productos.findOne({ _id: id }, (error, producto) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else {
            resolve({ success: true, message: '', data: producto });
          }
        });
      });
    },
    obtenerUsuario: (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return null;
      }
      const usuario = Usuarios.findOne({ usuario: usuarioActual.usuario });

      return usuario;
    },
    getCupon: async (root, { clave }, { usuarioActual }) => {
      try {
        if (!usuarioActual) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }
        const usuario = await Usuarios.findOne({
          usuario: usuarioActual.usuario
        });
        if (!usuario) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }
        return new Promise((resolve, reject) => {
          Cupones.findOne({ clave }, (error, cupon) => {
            if (error) {
              console.log('resolvers -> query -> getCupon -> error1 ', error);
              return reject(error);
            } else {
              return resolve(cupon);
            }
          });
        });
      } catch (error) {
        console.log('resolvers -> query -> getCupon -> error2 ', error);
        return {
          success: false,
          message: STATUS_MESSAGES.S_WENT_WRONG,
          data: null
        };
      }
    },
    getOrden: async (root, { id }, { usuarioActual }) => {
      try {
        if (!usuarioActual) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        const usuario = await Usuarios.findOne({
          usuario: usuarioActual.usuario
        });

        if (!usuario) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        return Ordenes.findOne({ _id: ObjectId(id) }).populate('descuento');
      } catch (error) {
        console.log('resolvers -> query -> getOrden -> error2 ', error);
        return {
          success: false,
          message: STATUS_MESSAGES.S_WENT_WRONG,
          data: null
        };
      }
    },
    getFullOrden: async (root, { id }, { usuarioActual }) => {
      try {
        if (!usuarioActual) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        const usuario = await Usuarios.findOne({
          usuario: usuarioActual.usuario
        });

        if (!usuario) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        let orden = await Ordenes.findById(ObjectId(id))
          .populate('cupon')
          .populate('cliente')
          .populate('profesional')
          .populate('producto');

        return new Promise((resolve, reject) => {
          if (orden) {
            resolve(orden);
          } else {
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          }
        });
      } catch (error) {
        console.log('resolvers -> query -> getOrden -> error2 ', error);
        return {
          success: false,
          message: STATUS_MESSAGES.S_WENT_WRONG,
          data: null
        };
      }
    },
    getProductoVisitas: async (root, { productId }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        ProductoVisitar.find(
          { producto_id: productId },
          (error, productoVisitas) => {
            if (error) {
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                list: []
              });
            } else {
              resolve({
                success: true,
                message: '',
                list: productoVisitas
              });
            }
          }
        );
      });
    },
    getOrdenesByProfessional: async (root, { profesional, dateRange }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        let condition = { profesional };
        //let condition = { profesional: "5d8556514c10b81c6a65295a" };
        if (dateRange && dateRange.fromDate && dateRange.toDate) {
          condition.created_at = { $gte: dateRange.fromDate, $lte: dateRange.toDate };
        }
        Ordenes.find(condition, [], { sort: { created_at: -1 } }, (error, ordenes) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              list: []
            });
          else {
            resolve({ success: true, message: '', list: ordenes });
          }
        });
      });
    },
    getPedidosByCliente: async (root, { cliente, dateRange }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        let condition = { cliente };
        //let condition = { cliente: "5d8556514c10b81c6a65295a" };
        if (dateRange && dateRange.fromDate && dateRange.toDate) {
          condition.created_at = { $gte: dateRange.fromDate, $lte: dateRange.toDate };
        }
        Ordenes.find(condition, [], { sort: { created_at: -1 } }, (error, ordenes) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              list: []
            });
          else {
            resolve({ success: true, message: '', list: ordenes });
          }
        });
      });
    },
    getUsuarioValoracion: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioValoracion.findOne(
          { usuario_id: usuario._id },
          (error, UsuarioValoracion) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                data: null
              });
            else resolve({ success: true, message: '', data: UsuarioValoracion });
          }
        );
      });
    },
    getProfessionalRating: async (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          list: []
        };
      }

      return new Promise((resolve, reject) => {
        Ordenes.find({ profesional: usuario._id, estado: 'Valorada' }, [], { sort: { updated_at: -1 } }, (error, ordenes) => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              list: []
            });
          else resolve({ success: true, message: '', list: ordenes });
        });
      });
    }
  },
  Mutation: {
    createNotification: async (
      root,
      { input },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      let newNotification = new Notification({
        user: input.user,
        cliente: input.cliente,
        profesional: input.profesional,
        type: input.type,
        orden: input.orden
      })
      return new Promise((resolve, reject) => {
        newNotification.save(error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.NOTIFICATION_CREATED
            });
        });
      });
    },

    readMessage: async (
      root,
      { conversationID },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      // run it
      return new Promise((resolve, reject) => {
        Conversation.updateMany({ _id: ObjectId(conversationID) }, { '$set': { 'messagechat.$[].read': true } }, { 'multi': true }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.NOTIFICATION_READ
            });
        });
      });
    },


    readNotification: async (
      root,
      { notificationId },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      return new Promise((resolve, reject) => {
        Notification.findOneAndUpdate({ _id: ObjectId(notificationId) }, { $set: { read: true } }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.NOTIFICATION_READ
            });
        });
      });
    },
    crearUsuarioFavoritoInspiration: async (
      root,
      { InspirationID },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoUsuarioFavoritoInspiracion = await new UsuarioFavoritoInspiracion({
        usuarioId: usuario._id,
        InspirationID
      });
      nuevoUsuarioFavoritoInspiracion.id = nuevoUsuarioFavoritoInspiracion._id;
      return new Promise((resolve, reject) => {
        nuevoUsuarioFavoritoInspiracion.save(error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.FAVOURITE_ADDED
            });
        });
      });
    },

    deleteConversation: async (
      root,
      { id },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        Conversation.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: "Conversacón eliminada con éxito"
            });
        });
      });
    },



    eliminarUsuarioFavoritoInspiracion: async (
      root,
      { id },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioFavoritoInspiracion.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.FAVOURITE_DELETED
            });
        });
      });
    },



    crearUsuarioFavoritoProducto: async (
      root,
      { productoId },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoUsuarioFavoritoProducto = await new UsuarioFavoritoProducto({
        usuarioId: usuario._id,
        productoId
      });
      nuevoUsuarioFavoritoProducto.id = nuevoUsuarioFavoritoProducto._id;
      return new Promise((resolve, reject) => {
        nuevoUsuarioFavoritoProducto.save(error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.FAVOURITE_ADDED
            });
        });
      });
    },
    eliminarUsuarioFavoritoProducto: async (
      root,
      { id },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioFavoritoProducto.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.FAVOURITE_DELETED
            });
        });
      });
    },
    eliminarUsuarioDireccion: async (root, { id }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioDireccion.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.ADDRESS_DELETED
            });
        });
      });
    },
    crearUsuarioDireccion: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoUsuarioDireccion = await new UsuarioDireccion({
        nombre: input.nombre,
        calle: input.calle,
        ciudad: input.ciudad,
        provincia: input.provincia,
        codigopostal: input.codigopostal,
        telefono: input.telefono,
        usuario_id: usuario._id
      });
      nuevoUsuarioDireccion.id = nuevoUsuarioDireccion._id;
      return new Promise((resolve, reject) => {
        nuevoUsuarioDireccion.save(error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.ADDRESS_ADDED,
              data: nuevoUsuarioDireccion
            });
        });
      });
    },
    crearUsuarioDeposito: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoUsuarioDeposito = await new UsuarioDeposito({
        fecha: new Date(input.fecha),
        estado: input.estado,
        total: input.total,
        usuarioid: input.usuarioid
      });
      nuevoUsuarioDeposito.id = nuevoUsuarioDeposito._id;
      return new Promise((resolve, reject) => {
        nuevoUsuarioDeposito.save(error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.DEPOSITO_ADDED,
              data: nuevoUsuarioDeposito
            });
        });
      });
    },
    eliminarUsuarioDeposito: async (root, { id }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioDeposito.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.DEPOSITO_DELETED
            });
        });
      });
    },
    eliminarCliente: (root, { id }) => {
      return new Promise((resolve, object) => {
        Clientes.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.CUENTA_ELIMINADA_ERROR
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.CUENTA_ELIMINADA
            });
        });
      });
    },
    eliminarPago: async (root, { id }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioPago.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.PAYMENT_METHOD_DELETED
            });
        });
      });
    },
    crearPago: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      const nuevoPago = await new UsuarioPago({
        nombre: input.nombre,
        iban: input.iban,
        usuario_id: usuario._id
      });

      nuevoPago.id = nuevoPago._id;

      return new Promise((resolve, reject) => {
        nuevoPago.save(error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.PAYMENT_METHOD_ADDED,
              data: nuevoPago
            });
        });
      });
    },
    
    singleUpload(parent, { file }) {
      const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
      const ext = matches[1];
      const base64_data = matches[2];
      const buffer = new Buffer(base64_data, 'base64');

      const filename = `${Date.now()}-file.${ext}`;
      const filenameWithPath = `${__dirname}/../uploads/images/${filename}`;

      return new Promise((resolve, reject) => {
        fs.writeFile(filenameWithPath, buffer, error => {
          if (error) reject(error);
          else resolve({ filename });
        });
      });
    },

    crearCategory: (root, { input }) => {
      const nuevoCategory = new Categories({
        id: input.id,
        title: input.title,
        image: input.image,
        description: input.description
      });

      nuevoCategory.id = nuevoCategory._id;

      return new Promise((resolve, object) => {
        nuevoCategory.save(error => {
          if (error) reject(error);
          else resolve(nuevoCategory);
        });
      });
    },

    crearInspiration: (root, { input }) => {
      const nuevoinspiration = new Inspiration({
        id: input.id,
        title: input.title,
        image: input.image,
        image1: input.image1,
        image2: input.image2,
        description: input.description
      });

      nuevoinspiration.id = nuevoinspiration._id;

      return new Promise((resolve, object) => {
        nuevoinspiration.save(error => {
          if (error) reject(error);
          else resolve(nuevoinspiration);
        });
      });
    },

    eliminarInspiration: (root, { id }) => {
      return new Promise((resolve, object) => {
        Inspiration.findOneAndDelete({ _id: id }, error => {
          if (error) rejects(error);
          else resolve('Eliminado correctamente');
        });
      });
    },


    eliminarCategory: (root, { id }) => {
      return new Promise((resolve, object) => {
        Categories.findOneAndDelete({ _id: id }, error => {
          if (error) rejects(error);
          else resolve('Eliminado correctamente');
        });
      });
    },
    crearCliente: (root, { input }) => {
      const nuevoCliente = new Clientes({
        id: input.id,
        nombre: input.nombre,
        apellido: input.apellido,
        email: input.email,
        telefonos: input.telefonos,
        productos: input.productos
      });

      nuevoCliente.id = nuevoCliente._id;

      return new Promise((resolve, object) => {
        nuevoCliente.save(error => {
          if (error) reject(error);
          else resolve(nuevoCliente);
        });
      });

      // const id = require('crypto').randomBytes(10).toString('hex');
    },
    actualizarCliente: (root, { input }) => {
      return new Promise((resolve, object) => {
        Clientes.findOneAndUpdate(
          { _id: input.id },
          input,
          { new: true },
          (error, cliente) => {
            if (error) rejects(error);
            else resolve(cliente);
          }
        );
      });
    },
    crearProducto: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoProducto = await new Productos({
        category_id: input.category_id,
        city: input.city,
        currency: input.currency,
        description: input.description,

        domingo: input.domingo,
        domingo_from: input.domingo_from,
        domingo_to: input.domingo_to,

        jueves: input.jueves,
        jueves_from: input.jueves_from,
        jueves_to: input.jueves_to,

        lunes: input.lunes,
        lunes_from: input.lunes_from,
        lunes_to: input.lunes_to,

        martes: input.martes,
        martes_from: input.martes_from,
        martes_to: input.martes_to,

        miercoles: input.miercoles,
        miercoles_from: input.miercoles_from,
        miercoles_to: input.miercoles_to,

        number: input.number,

        sabado: input.sabado,
        sabado_from: input.sabado_from,
        sabado_to: input.sabado_to,

        time: input.time,
        title: input.title,

        viernes: input.viernes,
        viernes_from: input.viernes_from,
        viernes_to: input.viernes_to,

        fileList: input.fileList,

        created_by: usuario._id
      });
      nuevoProducto.id = nuevoProducto._id;
      return new Promise((resolve, reject) => {
        nuevoProducto.save(error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.PRODUCT_ADDED,
              data: nuevoProducto
            });
        });
      });
    },
    crearUsuario: async (root, { input }) => {
      require('mongoose')
        .model('usuarios')
        .schema.add({ isNotVerified: Boolean, verificationToken: String });
      const token = crypto.randomBytes(20).toString('hex');
      // check if email exists
      const emailExists = await Usuarios.findOne({ email: input.email });
      if (emailExists) {
        return {
          success: false,
          message: STATUS_MESSAGES.EMAIL_EXISTS,
          data: null
        };
      }
      // check if username exists
      const usernameExists = await Usuarios.findOne({ usuario: input.usuario });
      if (usernameExists) {
        return {
          success: false,
          message: STATUS_MESSAGES.USERNAME_EXISTS,
          data: null
        };
      }
      const nuevoUsuario = await new Usuarios({
        id: input.id,
        UserID: input.UserID,
        usuario: input.usuario,
        password: input.password,
        email: input.email,
        nombre: input.nombre,
        apellidos: input.apellidos,
        ciudad: input.ciudad,
        telefono: input.telefono,
        tipo: input.tipo,
        productos: input.productos,
        isNotVerified: true,
        verificationToken: token
      });
      // console.log('here')
      // console.log(nuevoUsuario)
      nuevoUsuario.id = nuevoUsuario._id;

      // delete as this check has been performed above
      // const comprobarUsuario = await Usuarios.findOne({ email: input.email }, function (err, adventure) { });
      // if (comprobarUsuario) {
      //     throw new Error('El ususario ya existe en la bbdd');
      // }
      const email = input.email;
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.EMAIL_ADDRESS,
          pass: `${process.env.EMAIL_PASSWORD}`
        }
      });
      //http://localhost:3000/confirm/${token}\n
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: 'Verifica tu correo electrónico de Locatefit',
        text: `Verifica tu correo electrónico de Locatefit`,
        html: confirmEmail(token)
      };

      await transporter.sendMail(mailOptions, (err, response) => {
        console.log(err);
      });
      return new Promise((resolve, object) => {
        nuevoUsuario.save(error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: null
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.USER_ADDED,
              data: nuevoUsuario
            });
        });
      });
    },
    autenticarUsuario: async (root, { email, password }) => {
      const emailUsuario = await Usuarios.findOne({ email });
      if (!emailUsuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.USER_NOT_FOUND,
          data: null
        };
      }
      const my = { ...emailUsuario };
      if (my._doc.isNotVerified) {
        return {
          success: false,
          message: STATUS_MESSAGES.EMAIL_NOT_VERIFIED,
          data: null
        };
      }
      const passwordCorrecto = await bcrypt.compare(
        password,
        emailUsuario.password
      );
      if (!passwordCorrecto) {
        return {
          success: false,
          message: STATUS_MESSAGES.INCORRECT_PASSWORD,
          data: null
        };
      }
      return {
        success: true,
        message: '',
        data: {
          token: crearToken(emailUsuario, process.env.SECRETO, '2400hr'),
          id: emailUsuario._id
        }
      };
    },
    crearCupon: async (root, { input }, { usuarioActual }) => {
      try {
        if (!usuarioActual) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }
        const usuario = await Usuarios.findOne({
          usuario: usuarioActual.usuario
        });
        if (!usuario) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }
        const nuevoCupon = new Cupones({ ...input });
        return new Promise((resolve, reject) => {
          nuevoCupon.save((error, cupon) => {
            if (error) {
              console.log(
                'resolvers -> mutation -> crearCupon -> error1 ',
                error
              );
              return reject(error);
            } else {
              return resolve(cupon);
            }
          });
        });
      } catch (error) {
        console.log('resolvers -> mutation -> crearCupon -> error2 ', error);
        return new Promise((_resolve, reject) => {
          return reject({
            success: false,
            message: STATUS_MESSAGES.S_WENT_WRONG,
            data: null
          });
        });
      }
    },
    actualizarUsuario: async (root, { input }) => {
      return new Promise((resolve, reject) => {
        Usuarios.findOneAndUpdate(
          { _id: input.id },
          input,
          { new: true },
          (error, _usuario) => {
            if (error) reject(error);
            else resolve(_usuario);
          }
        );
      });
    },
    editarProducto: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }

      return new Promise((resolve, reject) => {
        Productos.findOneAndUpdate(
          { _id: input.id },
          input,
          { new: true },
          (error, producto) => {
            if (error)
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG,
                data: null
              });
            else
              resolve({
                success: true,
                message: STATUS_MESSAGES.PRODUCT_UPDATED,
                data: producto
              });
          }
        );
      });
    },
    eliminarProducto: (root, { id }, { usuarioActual }) => {
      return new Promise((resolve, object) => {
        Productos.findOneAndDelete({ _id: id }, error => {
          if (error)
            rejects({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.PRODUCT_UPDATED
            });
        });
      });
    },
    crearModificarOrden: async (root, { input }, { usuarioActual }) => {
      try {
        if (!usuarioActual) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        const usuario = await Usuarios.findOne({
          usuario: usuarioActual.usuario
        });
        if (!usuario) {
          return {
            success: false,
            message: STATUS_MESSAGES.NOT_LOGGED_IN,
            data: null
          };
        }

        let orden;
        if (input.id) {
          orden = await Ordenes.findById(ObjectId(input.id)).exec();
        }

        let producto;
        if (!!input.producto) {
          producto = await Productos.findById(ObjectId(input.producto)).exec();
          if (!producto) {
            return new Promise((_resolve, reject) => {
              return reject({
                success: false,
                message: STATUS_MESSAGES.PRODUCTO_NO_EXISTE,
                data: null
              });
            });
          }
        }
        if (!producto) {
          producto = await Productos.findById(orden.producto).exec();
          if (!producto) {
            return new Promise((_resolve, reject) => {
              return reject({
                success: false,
                message: STATUS_MESSAGES.PRODUCTO_NO_EXISTE,
                data: null
              });
            });
          }
        }

        let direccion;
        if (!!input.direccion) {
          direccion = await UsuarioDireccion.findById(
            ObjectId(input.direccion)
          ).exec();
        }

        let cupon;
        if (!!input.clave) {
          cupon = await Cupones.findOne({ clave: input.clave }).exec();
        } else if (!!input.cupon) {
          cupon = ObjectId(input.cupon);
        }
        const { nota = '', aceptaTerminos = false } = input;
        if (!orden || !orden._id) {
          if (!producto) {
            return new Promise((_resolve, reject) => {
              return reject({
                success: false,
                message: STATUS_MESSAGES.ORDEN_NO_TIENE_PRODUCTO,
                data: null
              });
            });
          }
          orden = new Ordenes({
            endDate: input.endDate,
            time: input.time,
            cupon: cupon && cupon._id ? cupon._id : cupon,
            nota,
            aceptaTerminos,
            direccion,
            producto: producto._id,
            cantidad: input.cantidad || 1,
            cliente: usuario._id,
            profesional: ObjectId(producto.created_by)
          });
        } else {
          const { id, clave, ...rest } = input;
          orden.set({
            ...rest,
            cliente: usuario._id,
            profesional: ObjectId(producto.created_by),
            direccion
          });
          if (!orden.cliente) {
            orden.set({ cliente: usuario._id });
          }
          if (!orden.profesional) {
            orden.set({ profesional: ObjectId(producto.created_by) });
          }
          if (!orden.cupon) {
            orden.set({ cupon: cupon && cupon._id ? cupon._id : cupon });
          }
        }

        if (!orden.producto) {
          return new Promise((_resolve, reject) => {
            return reject({
              success: false,
              message: STATUS_MESSAGES.ORDEN_NO_TIENE_PRODUCTO,
              data: null
            });
          });
        }

        return new Promise((resolve, reject) => {
          orden.save((error, ordenGuardada) => {
            if (error) {
              console.log(
                'resolvers -> mutation -> crearModificarOrden -> error1 ',
                error
              );
              return reject(error);
            } else {
              if (!ordenGuardada.descuento && cupon && cupon._id) {
                ordenGuardada.descuento = cupon;
              }
              return resolve(ordenGuardada);
            }
          });
        });
      } catch (error) {
        console.log(
          'resolvers -> mutation -> crearModificarOrden -> error2 ',
          error
        );
        return new Promise((_resolve, reject) => {
          return reject({
            success: false,
            message: STATUS_MESSAGES.S_WENT_WRONG,
            data: null
          });
        });
      }
    },
    agregarPagoPaypalOrden: async (root, { input }, { usuarioActual }) => {
      console.log('agregarPagoPaypalOrden', { root, input, usuarioActual });
      try {
        let orden;
        if (input.id) {
          orden = await Ordenes.findById(ObjectId(input.id)).exec();
        }

        if (!orden) {
          return {
            success: false,
            message: STATUS_MESSAGES.ORDEN_NO_EXISTE,
            data: null
          };
        }

        const { id, pagoPaypal } = input;

        orden.set({ estado: 'Nueva', progreso: '50', status: 'active', pagoPaypal });

        return new Promise((resolve, reject) => {
          orden.save((error, ordenGuardada) => {
            if (error) {
              console.log(
                'resolvers -> mutation -> agregarPagoPaypalOrden -> error1 ',
                error
              );
              return reject(error);
            } else {
              return resolve(ordenGuardada);
            }
          });
        });
      } catch (error) {
        console.log(
          'resolvers -> mutation -> agregarPagoPaypalOrden -> error2 ',
          error
        );
        return new Promise((_resolve, reject) => {
          return reject({
            success: false,
            message: STATUS_MESSAGES.S_WENT_WRONG,
            data: null
          });
        });
      }
    },

    crearUsuarioValoracion: async (
      root,
      { input },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const nuevoUsuarioValoracion = await new UsuarioValoracion({
        rating,
        likes,
        dislikes,
        coment: input.coment,
        orden: orden._id,
        producto: producto._id,
        cliente: usuario._id,
        profesional: ObjectId(producto.created_by)
      });
      nuevoUsuarioValoracion.id = nuevoUsuarioValoracion._id;
      return new Promise((resolve, reject) => {
        nuevoUsuarioValoracion.save(error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.ORDEN_VALORED
            });
        });
      });
    },


    eliminarUsuarioValoracion: async (
      root,
      { id },
      { usuarioActual }
    ) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN,
          data: null
        };
      }
      return new Promise((resolve, reject) => {
        UsuarioValoracion.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({ success: false, message: STATUS_MESSAGES.S_WENT_WRONG });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.ORDEN_VALORED_DELETE
            });
        });
      });
    },

    eliminarUsuario: (root, { id }) => {
      return new Promise((resolve, object) => {
        Usuarios.findOneAndDelete({ _id: id }, error => {
          if (error)
            reject({
              success: false,
              message: STATUS_MESSAGES.CUENTA_ELIMINADA_ERROR
            });
          else
            resolve({
              success: true,
              message: STATUS_MESSAGES.CUENTA_ELIMINADA
            });
        });
      });
    },
    ordenProceed: async (root, { ordenId, estado, progreso, status, coment, rate, descripcionproblem }, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario
      });
      if (!usuario) {
        return {
          success: false,
          message: STATUS_MESSAGES.NOT_LOGGED_IN
        };
      }

      return new Promise((resolve, reject) => {
        const message = (estado === 'Aceptado') ? STATUS_MESSAGES.ORDEN_ACEPTADO : STATUS_MESSAGES.ORDEN_RECHAZADO;
        let dataToUpdate = { estado };
        if (progreso) dataToUpdate.progreso = progreso;
        if (descripcionproblem) dataToUpdate.descripcionproblem = descripcionproblem;
        if (coment) dataToUpdate.coment = coment;
        if (rate) dataToUpdate.rate = rate;
        if (status) dataToUpdate.status = status;
        Ordenes.findOneAndUpdate(
          { _id: ordenId },
          dataToUpdate,
          error => {
            if (error) {
              reject({
                success: false,
                message: STATUS_MESSAGES.S_WENT_WRONG
              });
            } else {
              resolve({
                success: true,
                message
              });
            }
          }
        );
      });
    }
  },

  Conversation: {
    prfsnal(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.find({ _id: parent.profesional }, [], { sort: { createdAt: -1 } }, (error, prfsnal) => {
          if (error) reject(error);
          else resolve(prfsnal);
        });
      });
    },

    client(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.find({ _id: parent.cliente }, [], { sort: { createdAt: -1 } }, (error, client) => {
          if (error) reject(error);
          else resolve(client);
        });
      });
    },
  },


  UsuarioFavoritoInspiracion: {
    inspiracion(parent) {
      return new Promise((resolve, reject) => {
        Inspiration.findById(parent.InspirationID, (error, inspiracion) => {
          if (error) {
            console.log('erro in chiild: ', error)
            reject(error);
          }
          else {
            resolve(inspiracion);
          }
        });
      });
    }
  },

  Inspiration: {
    anadidoFavoritoinspiration(parent, args, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        Usuarios.findOne(
          { usuario: usuarioActual.usuario },
          (error, usuario) => {
            if (error) reject(error);
            else {
              if (!usuario) resolve(false);
              else {
                UsuarioFavoritoInspiracion.findOne(
                  { usuarioId: usuario._id, InspirationID: parent._id },
                  (error, usuarioFavoritoInspiracion) => {
                    if (error) reject(error);
                    else {
                      if (!usuarioFavoritoInspiracion) resolve(false);
                      else resolve(true);
                    }
                  }
                );
              }
            }
          }
        );
      });
    },

  },

  UsuarioFavoritoProducto: {
    producto(parent) {
      return new Promise((resolve, reject) => {
        Productos.findById(parent.productoId, (error, producto) => {
          if (error) {
            console.log('erro in chiild: ', error)
            reject(error);
          }
          else {
            resolve(producto);
          }
        });
      });
    }
  },

  Producto: {
    visitas(parent, args, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        ProductoVisitar.find(
          { producto_id: parent._id },
          (error, productoVisitas) => {
            if (error) reject(error);
            else {
              if (productoVisitas.length === 0) resolve(0);
              else {
                let count = 0;
                productoVisitas.forEach(element => {
                  count += element.visitas;
                });
                resolve(count);
              }
            }
          }
        );
      });
    },
    anadidoFavorito(parent, args, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        Usuarios.findOne(
          { usuario: usuarioActual.usuario },
          (error, usuario) => {
            if (error) reject(error);
            else {
              if (!usuario) resolve(false);
              else {
                UsuarioFavoritoProducto.findOne(
                  { usuarioId: usuario._id, productoId: parent._id },
                  (error, usuarioFavoritoProducto) => {
                    if (error) reject(error);
                    else {
                      if (!usuarioFavoritoProducto) resolve(false);
                      else resolve(true);
                    }
                  }
                );
              }
            }
          }
        );
      });
    },
    category(parent) {
      return new Promise((resolve, reject) => {
        Categories.findById(parent.category_id, (error, category) => {
          if (error) reject(error);
          else resolve(category);
        });
      });
    },
    creator(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findById(parent.created_by, (error, usuario) => {
          if (error) reject(error);
          else resolve(usuario);
        });
      });
    },
    ordenes(parent, args, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        Ordenes.find(
          { producto: parent._id },
          (error, ordenes) => {
            if (error) reject(error);
            else resolve(ordenes);
          }
        );
      });
    },
    professionalRating(parent) {
      return new Promise((resolve, reject) => {
        Ordenes.find({ producto: parent._id, estado: 'Valorada' }, [], { sort: { updated_at: -1 } }, (error, ordenes) => {
          if (error) reject(error);
          else resolve(ordenes)
        });
      });
    }
  },
  Category: {
    productos(parent, args, context, { variableValues: { price, city } }) {
      let condition = { category_id: parent._doc._id };
      if (price) condition.number = { $lte: price };
      if (city) condition.city = city;
      return new Promise((resolve, reject) => {
        Productos.find(condition, (error, productos) => {
          if (error) reject(error);
          else resolve(productos);
        });
      });
    }
  },

  Orden: {
    product(parent) {
      return new Promise((resolve, reject) => {
        Productos.findOne({ _id: parent.producto }, (error, producto) => {
          if (error) reject(error);
          else resolve(producto);
        });
      });
    },
    client(parent) {
      return new Promise((resolve, reject) => {
        UsuarioDireccion.findOne({ usuario_id: parent.cliente }, (error, client) => {
          if (error) reject(error);
          else resolve(client);
        });
      });
    },
    prfsnl(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent.profesional }, (error, client) => {
          if (error) reject(error);
          else resolve(client);
        });
      });
    },
    professionalRating(parent) {
      return new Promise((resolve, reject) => {
        Ordenes.find({ profesional: parent.profesional, estado: 'Valorada' }, [], { sort: { updated_at: -1 } }, (error, ordenes) => {
          if (error) reject(error);
          else resolve(ordenes)
        });
      });
    }
  },
  ProfessionalRating: {
    customer(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent.cliente }, (error, customer) => {
          if (error) reject(error);
          else resolve(customer);
        });
      });
    }
  }
};
