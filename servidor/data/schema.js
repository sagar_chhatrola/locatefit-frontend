const { gql } = require('apollo-server-express');

export const typeDefs = gql`

  type ProfessionalListRating {
    success: Boolean!
    message: String
    list: [ProfessionalRating]
  }
  type ProfessionalRating {
    id: ID
    coment: String
    updated_at: String
    rate: Int
    customer: Usuario
  }

  type GeneralResponse {
    success: Boolean!
    message: String!
  
  }

  type Conversationgeneralresponse {
    success: Boolean!
    message: String!
    conversation: [Conversation]
  
  }

  type MessageNoRead {
        _id: String
        count: Int
  }

  type messagenoReadResponse {
    success: Boolean!
    message: String
    conversation: [MessageNoRead]
  }


type Conversation {
   _id: String
   prfsnal: [Usuario]
   client: [Usuario]
   messagechat: [ConversationList]
}

  type ConversationList {
    _id: String
    text: String
    read: Boolean
    createdAt: String
    user: Userchat
  }

  type Userchat {
  _id: String
  name: String
  avatar: String
}

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type ProductoListResponse {
    success: Boolean!
    message: String
    list: [Producto]
  }

  type OrdenListResponse {
    success: Boolean!
    message: String
    list: [Orden]
  }

  type ValoracionListResponse {
    success: Boolean!
    message: String
    list: [Valoracion]
  }
  
  type UsuarioDireccionResponse {
    success: Boolean!
    message: String
    data: UsuarioDireccion
  }

  type UsuarioDepositoResponse {
    success: Boolean!
    message: String
    total: Int
    list: [UsuarioDeposito]
  }

  type UsuarioDepositoCreateResponse {
    success: Boolean!
    message: String
    data: UsuarioDeposito
  }

  type PagoResponse {
    success: Boolean!
    message: String
    data: Pago
  }

  type ProductoResponse {
    success: Boolean!
    message: String
    data: Producto
  }

  type ProductoVisitasResponse {
    success: Boolean!
    message: String
    list: [ProductoVisitas]
  }

  type CategoryResponse {
    success: Boolean!
    message: String
    data: Category
  }

  type InspirationResponse {
    success: Boolean!
    message: String
    data: Inspiration
  }

  type ProductocityResponse {
    success: Boolean!
    message: String
    data: [Producto]
  }


  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type UsuarioFavoritoProductosResponse {
    success: Boolean!
    message: String
    list: [UsuarioFavoritoProducto]
  }

  type UsuarioFavoritoInspiracionResponse {
    success: Boolean!
    message: String
    list: [UsuarioFavoritoInspiracion]
  }

  type UsuarioFavoritoProducto {
    id: ID
    productoId: ID
    producto: Producto
  }

  type UsuarioFavoritoInspiracion {
    id: ID
    InspiracionID: ID
    inspiracion: Inspiration
  }

  type Category {
    id: ID
    title: String
    image: String
    description: String
    productos: [Producto]
  }

  type Inspiration {
    id: ID
    title: String
    image: String
    image1: String
    image2: String
    description: String
    created_at: String
    anadidoFavoritoinspiration: Boolean
  }

  type UsuarioDireccion {
    id: ID
    nombre: String
    calle: String
    ciudad: String
    provincia: String
    codigopostal: String
    telefono: String
    usuario_id: String
  }

  type UsuarioDeposito {
    id: ID
    fecha: String
    estado: String
    total: String
    usuarioid: String
    created_at: String
  }

  type Pago {
    id: ID
    nombre: String
    iban: String
    usuario_id: String
  }

  type Cliente {
    id: ID
    nombre: String
    apellido: String
    email: String
    telefonos: [Telefono]
    productos: [Product]
  }

  type Cupon {
    id: ID
    clave: String
    descuento: Float
    tipo: String
  }

  type Orden {
    id: ID
    cupon: ID
    nota: String
    aceptaTerminos: Boolean
    endDate: String
    time: String
    direccion: UsuarioDireccion
    producto: ID
    cantidad: Int
    cliente: ID
    profesional: ID
    descuento: Cupon
    estado: String
    status: String
    progreso: String
    created_at: String
    product: Producto
    client: UsuarioDireccion
    pagoPaypal: PagoPaypal
    prfsnl: Usuario
    coment: String
    rate: Int
    professionalRating: [ProfessionalRating]
    customer: Usuario
  }

  type Valoracion {
    id: ID
    coment: String
    rating: Int
    likes: Int
    dislikes: Int
    cliente: ID
    Orden: ID
    producto: ID
    profesional: ID
    product: Producto
    prfsnl: Usuario
  }

  input valoraciones {
    id: ID
    coment: String
    rating: Int
    cliente: ID
    likes: Int
    dislikes: Int
    Orden: ID
    producto: ID
    profesional: ID
    
  }

  type PagoPaypal {
    idPago: String
    idPagador: String
    fechaCreacion: String
    emailPagador: String
    nombrePagador: String
    apellidoPagador: String
    estadoPago: String
    moneda: String
    montoPagado: String
  }

  input PagoPaypalInput {
    idPago: String
    idPagador: String
    fechaCreacion: String
    emailPagador: String
    nombrePagador: String
    apellidoPagador: String
    estadoPago: String
    moneda: String
    montoPagado: String
  }

  type FullOrden {
    id: ID
    cupon: Cupon
    nota: String
    aceptaTerminos: Boolean
    direccion: UsuarioDireccion
    producto: Producto
    cantidad: Int
    cliente: Usuario
    profesional: Usuario
    pagoPaypal: PagoPaypal
  }

  type Usuario {
    id: ID
    usuario: String
    UserID: String
    Verified: Boolean
    password: String
    email: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    tipo: [tipoUsuario]
    productos: [Product]
    foto_del_perfil: String
    fotos_tu_dni: [String]
    profesion: String
    descripcion: String
    fecha_de_nacimiento: String
    notificacion: Int
    grado: Int
    estudios: String
    formularios_de_impuesto: [String]
    fb_enlazar: String
    twitter_enlazar: String
    instagram_enlazar: String
    youtube_enlazar: String
    propia_web_enlazar: String
    created_at: String
  }

  type Notification {
    _id: String
    user: Usuario
    cliente: Usuario
    profesional: Usuario
    orden: Orden
    read: Boolean
    type: String
    createdAt: String
  }



  type Statistics {
    name: String
    Ene : Int
    Feb : Int
    Mar : Int
    Abr : Int
    May : Int
    Jun : Int
    Jul : Int
    Aug : Int
    Sep : Int
    Oct : Int
    Nov : Int
    Dic : Int
  }

  type getNotificationResponse {
    success: Boolean!
    message: String
    notifications:[Notification]
  }

  type getStatisticsResponse {
    success: Boolean!
    message: String
    data:[Statistics]
  }

  type Token {
    token: String!
  }

  type AutenticarUsuarioResponse {
    success: Boolean!
    message: String
    data: AutenticarUsuario
  }

  type AutenticarUsuario {
    token: String!
    id: String!
  }

  enum tipoUsuario {
    CORTE
    CORTEYAFEITADO
  }

  type Product {
    id: ID
    category_id: String
    city: String
    currency: String
    description: String
    number: Int
    time: String
    title: String
    created_by: String
  }

  type Producto {
    id: ID
    category_id: String
    city: String
    currency: String
    description: String

    domingo: String
    domingo_from: String
    domingo_to: String

    jueves: String
    jueves_from: String
    jueves_to: String

    lunes: String
    lunes_from: String
    lunes_to: String

    martes: String
    martes_from: String
    martes_to: String

    miercoles: String
    miercoles_from: String
    miercoles_to: String

    number: Int

    sabado: String
    sabado_from: String
    sabado_to: String

    time: String
    title: String

    viernes: String
    viernes_from: String
    viernes_to: String

    fileList: [String]

    created_by: String

    category: Category
    creator: Usuario

    anadidoFavorito: Boolean
    visitas: Int

    ordenes: [Orden]

    valoraciones: [Valoracion]

    professionalRating: [ProfessionalRating]
  }

  type Telefono {
    telefono: String
  }

  type ProductoVisitas {
    id: ID
    producto_id: String
    mes_id: Int
    ano: Int
    visitas: Int
  }

  type Query {
    uploads: [File]
    getUsuarioDireccion: UsuarioDireccionResponse
    getUsuarioDeposito(page: Int dateRange: DateRangeInput): UsuarioDepositoResponse
    getUsuarioPago: PagoResponse
    getUsuarioValoracion: ValoracionListResponse
    getCategories: [Category]
    getInspiration: [Inspiration]
    getCategory(id: ID, price: Int, city: String): CategoryResponse
    getProductoCity(city: String): ProductocityResponse
    getProductoChat(id: ID!): ProductocityResponse
    getProductoSearch(city: String search: String): ProductocityResponse

    getClientes: [Cliente]
    getCliente(id: ID): Cliente

    getUsuarioFavoritoProductos: UsuarioFavoritoProductosResponse
    getUsuarioFavoritoInspiracion: UsuarioFavoritoInspiracionResponse

    getUsuarios: [Usuario]
    getUsuario(id: ID): Usuario
    getNotifications(userId: ID): getNotificationResponse

    getMessageNoRead(conversationID: ID! profeional: ID!): messagenoReadResponse


    getStatistics(userId: ID,userType:String): getStatisticsResponse

    # getProductos: [Producto]
    getProductos: ProductoListResponse
    getPro: [Producto]
    getProducto(id: ID, updateProductVisit: Boolean): ProductoResponse
    getProductoVisitas(productId: ID): ProductoVisitasResponse

    getConversation(profeional: ID): Conversationgeneralresponse

    obtenerUsuario: Usuario

    getCupon(clave: String): Cupon
    getOrden(id: ID): Orden
    getFullOrden(id: ID): FullOrden

    # Ordenes
    getOrdenesByProfessional(profesional: ID dateRange: DateRangeInput ): OrdenListResponse
    getPedidosByCliente(cliente: ID dateRange: DateRangeInput): OrdenListResponse
    
    getProfessionalRating: ProfessionalListRating
  }

  """
  Campos para las reservas
  """
  input DateRangeInput {
    fromDate: String
    toDate: String
  }

  input NotificationInput {
    user: String
    cliente: String
    profesional: String
    orden: String
    read: Boolean
    type: String
  }

  input FileInput {
    filename: String
    mimetype: String
    encoding: String
  }
  input TelefonoInput {
    telefono: String
  }

  input UsuarioDireccionInput {
    nombre: String
    calle: String
    ciudad: String
    provincia: String
    codigopostal: String
    telefono: String
  }

  input UsuarioDepositoInput {
    fecha: String
    estado: String
    total: String
    usuarioid: String
  }

  input PagoInput {
    nombre: String
    iban: String
  }

  input CategoryInput {
    id: ID
    title: String!
  }

  input InspirationInput {
    id: ID
    title: String!
    description: String
    image: String
    image1: String
    image2: String
    anadidoFavoritoinspiration: Boolean
  }

  input CuponCreationInput {
    clave: String!
    descuento: Float
    tipo: String
  }

  input OrdenCreationInput {
    endDate: String
    time: String
    clave: String
    nota: String
    id: ID
    aceptaTerminos: Boolean
    cupon: ID
    direccion: ID
    producto: ID
    cantidad: Int
  }

  input AgregarPagoOrdenInput {
    id: ID
    pagoPaypal: PagoPaypalInput
  }

  input ClienteInput {
    id: ID
    nombre: String!
    apellido: String
    email: String!
    telefonos: [TelefonoInput]
    productos: [ProductInput]
  }

  input UsuarioInput {
    id: ID
    usuario: String!
    password: String!
    email: String!
    UserID: String
    Verified: Boolean
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    tipo: [tipoUsuario]
    productos: [ProductInput]
    foto_del_perfil: String
    fotos_tu_dni: [String]
    profesion: String
    descripcion: String
    fecha_de_nacimiento: String
    notificacion: Int
    grado: Int
    estudios: String
    formularios_de_impuesto: [String]
    fb_enlazar: String
    twitter_enlazar: String
    instagram_enlazar: String
    youtube_enlazar: String
    propia_web_enlazar: String
  }

  input ActualizarUsuarioInput {
    id: ID!
    email: String!
    UserID: String
    Verified: Boolean
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    foto_del_perfil: String
    fotos_tu_dni: [String]
    profesion: String
    descripcion: String
    fecha_de_nacimiento: String
    notificacion: Int
    grado: Int
    estudios: String
    formularios_de_impuesto: [String]
    fb_enlazar: String
    twitter_enlazar: String
    instagram_enlazar: String
    youtube_enlazar: String
    propia_web_enlazar: String
  }

  input ProductInput {
    id: ID
    city: String
    category_id: String
    currency: String
    description: String

    number: Int

    time: String
    title: String

    created_by: String
  }

  input ProductoInput {
    id: ID
    city: String!
    category_id: String!
    currency: String!
    description: String!

    domingo: String
    domingo_from: String
    domingo_to: String

    jueves: String
    jueves_from: String
    jueves_to: String

    lunes: String
    lunes_from: String
    lunes_to: String

    martes: String
    martes_from: String
    martes_to: String

    miercoles: String
    miercoles_from: String
    miercoles_to: String

    number: Int

    sabado: String
    sabado_from: String
    sabado_to: String

    time: String
    title: String

    viernes: String
    viernes_from: String
    viernes_to: String

    fileList: [String]

    created_by: String
  }

  input EditarProductoInput { 
    id: ID
    title: String
    number: Int
    currency: String
    time: String
    city: String
    description: String
    lunes: String
    lunes_from: String
    lunes_to: String
    martes: String
    martes_from: String
    martes_to: String
    miercoles: String
    miercoles_from: String
    miercoles_to: String
    jueves: String
    jueves_from: String
    jueves_to: String
    viernes: String
    viernes_from: String
    viernes_to: String
    sabado: String
    sabado_from: String
    sabado_to: String
    domingo: String
    domingo_from: String
    domingo_to: String
    category_id: String
    fileList: [String]
  }

  input ProductVisitarInput {
    producto_id: String!
  }

  """
  Mutations para crear clientes
  """
  type Mutation {
    singleUpload(file: Upload): File
    crearUsuarioFavoritoProducto(productoId: ID!): GeneralResponse
    eliminarUsuarioFavoritoProducto(id: ID!): GeneralResponse
    deleteConversation(id: ID!): GeneralResponse


    crearUsuarioFavoritoInspiration(InspirationID: ID!): GeneralResponse
    eliminarUsuarioFavoritoInspiracion(id: ID!): GeneralResponse

    """
    UsuarioDireccion
    """
    crearUsuarioDireccion(
      input: UsuarioDireccionInput
    ): UsuarioDireccionResponse
    eliminarUsuarioDireccion(id: ID!): GeneralResponse
    
    """
    Notification
    """
    readNotification(notificationId: ID!): GeneralResponse
    createNotification(input: NotificationInput): GeneralResponse

    readMessage(conversationID: ID!): GeneralResponse

    """
    UsuarioDeposito
    """
    crearUsuarioDeposito(
      input: UsuarioDepositoInput
    ): UsuarioDepositoCreateResponse
    eliminarUsuarioDeposito(id: ID!): GeneralResponse

    """
    UsuarioPago
    """
    crearPago(input: PagoInput): PagoResponse
    eliminarPago(id: ID!): GeneralResponse

    """
    Crear nuevos categories
    """
    crearCategory(input: CategoryInput): Category
    eliminarCategory(id: ID!): String

    crearCliente(input: ClienteInput): Cliente
    actualizarCliente(input: ClienteInput): Cliente
    eliminarCliente(id: ID!): GeneralResponse

    """
    Crear nuevos inspiration
    """
    crearInspiration(input: InspirationInput): Inspiration
    eliminarInspiration(id: ID!): String
    """
    Crear nuevos usuarios
    """
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
    actualizarUsuario(input: ActualizarUsuarioInput): Usuario
    eliminarUsuario(id: ID!): GeneralResponse

    """
    Crear nuevos productos
    """
    crearProducto(input: ProductoInput): ProductoResponse
    editarProducto(input: EditarProductoInput): ProductoResponse
    eliminarProducto(id: ID!): GeneralResponse

    autenticarUsuario(
      email: String!
      password: String!
    ): AutenticarUsuarioResponse

    """
    Crear cupones
    """
    crearCupon(input: CuponCreationInput): Cupon

    """
    Ordenes
    """
    crearModificarOrden(input: OrdenCreationInput): Orden

    agregarPagoPaypalOrden(input: AgregarPagoOrdenInput): FullOrden

    actualizarProductoVisitar(producto_id: String!): GeneralResponse

    ordenProceed(ordenId: ID! estado: String! progreso: String! status: String! nota: String coment: String rate: Int, descripcionproblem: String ): GeneralResponse

    crearUsuarioValoracion(input: valoraciones): Valoracion
    eliminarUsuarioValoracion(id: ID!): GeneralResponse
  }
`;
