import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
dotenv.config({ path: 'variables.env' });

mongoose.Promise = global.Promise;

mongoose.connect(process.env.MONGODB_URI,
  { useNewUrlParser: true, useUnifiedTopology: true}
);



mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


const usuarioFavoritoProductoSchema = new mongoose.Schema(
  {
    usuarioId: String,
    productoId: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioFavoritoProducto = mongoose.model(
  'usuarioFavoritoProducto',
  usuarioFavoritoProductoSchema
);

const usuarioFavoritoInspiracionSchema = new mongoose.Schema(
  {
    usuarioId: String,
    InspirationID: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioFavoritoInspiracion = mongoose.model(
  'usuarioFavoritoInspiracion',
  usuarioFavoritoInspiracionSchema
);

const usuarioDireccionSchema = new mongoose.Schema(
  {
    nombre: String,
    calle: String,
    ciudad: String,
    provincia: String,
    codigopostal: String,
    telefono: String,
    usuario_id: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioDireccion = mongoose.model(
  'usuarioDireccion',
  usuarioDireccionSchema
);

const usuarioDepositoSchema = new mongoose.Schema(
  {
    fecha: Date,
    estado: String,
    total: String,
    usuarioid: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioDeposito = mongoose.model(
  'usuarioDeposito',
  usuarioDepositoSchema
);

const usuarioPagoSchema = new mongoose.Schema(
  {
    nombre: String,
    iban: String,
    usuario_id: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioPago = mongoose.model('usuarioPago', usuarioPagoSchema);

const categoriesSchema = new mongoose.Schema(
  {
    title: String,
    image: String,
    description: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const Categories = mongoose.model('categories', categoriesSchema);

const inspirationSchema = new mongoose.Schema(
  {
    title: String,
    image: String,
    image1: String,
    image2: String,
    description: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const Inspiration = mongoose.model('inspiration', inspirationSchema);

const clientesSchema = new mongoose.Schema(
  {
    nombre: String,
    apellido: String,
    email: String,
    telefonos: Array,
    productos: Array
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const Clientes = mongoose.model('clientes', clientesSchema);

// Temperaturas
const productosSchema = new mongoose.Schema(
  {
    category_id: String,
    title: {type: String, text: true},
    city: String,
    currency: String,
    description: String,

    domingo: String,
    domingo_from: String,
    domingo_to: String,

    jueves: String,
    jueves_from: String,
    jueves_to: String,

    lunes: String,
    lunes_from: String,
    lunes_to: String,

    martes: String,
    martes_from: String,
    martes_to: String,

    miercoles: String,
    miercoles_from: String,
    miercoles_to: String,

    number: Number,

    sabado: String,
    sabado_from: String,
    sabado_to: String,

    time: String,
    

    viernes: String,
    viernes_from: String,
    viernes_to: String,

    fileList: [String],

    created_by: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const Productos = mongoose.model('productos', productosSchema);

const usuariosSchema = new mongoose.Schema(
  {
    usuario: String,
    UserID: String,
    Verified: Boolean,
    password: String,
    email: String,
    nombre: String,
    apellidos: String,
    ciudad: String,
    telefono: String,
    tipo: Array,
    productos: Array,
    foto_del_perfil: String,
    fotos_tu_dni: Array,
    profesion: String,
    descripcion: String,
    fecha_de_nacimiento: String,
    notificacion: Number,
    grado: Number,
    estudios: String,
    formularios_de_impuesto: Array,
    fb_enlazar: String,
    twitter_enlazar: String,
    instagram_enlazar: String,
    youtube_enlazar: String,
    propia_web_enlazar: String
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

// hashear los password antes de guardar
usuariosSchema.pre('save', function(next) {
  // Si el password no esta hasheado...
  if (!this.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

const Usuarios = mongoose.model('usuarios', usuariosSchema);

const cuponesSchema = new mongoose.Schema({
  clave: { type: String, unique: true },
  descuento: Number,
  tipo: {
    type: String,
    enum: ['porcentaje', 'dinero'],
    default: 'porcentaje'
  }
});

const Cupones = mongoose.model('cupones', cuponesSchema);

const pagoPaypalSchema = new mongoose.Schema({
  idPago: String,
  idPagador: String,
  fechaCreacion: String,
  emailPagador: String,
  nombrePagador: String,
  apellidoPagador: String,
  estadoPago: String,
  moneda: String,
  montoPagado: Number
});

const ordenesSchema = new mongoose.Schema(
  {
    endDate: String,
    time: String,
    producto: { type: mongoose.Schema.Types.ObjectId, ref: 'productos' },
    cupon: { type: mongoose.Schema.Types.ObjectId, ref: 'cupones' },
    nota: String,
    coment: String,
    descripcionproblem: String,
    rate: Number,
    aceptaTerminos: Boolean,
    direccion: usuarioDireccionSchema,
    cantidad: { type: Number, default: 1 },
    cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
    profesional: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
    pagoPaypal: {type:mongoose.Schema.Types.Mixed, pagoPaypalSchema},
    stripePaymentIntent:{type:mongoose.Schema.Types.Mixed},
    estado: { type: String, enum: ['Pendiente de pago', 'Nueva', 'Aceptado', 'Rechazado', 'Finalizada', 'Devuelto', 'Valorada', 'Resolución'], default: "Pendiente de pago" },
    progreso: { type: String, enum: ['0', '25', '50', '75', '100'], default: "25" },
    status: { type: String, enum: ['success', 'exception', 'normal', 'active'], default: "active" }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

ordenesSchema.virtual('descuento', {
  ref: 'cupones',
  localField: 'cupon',
  foreignField: '_id',
  justOne: true
});

const Ordenes = mongoose.model('ordenes', ordenesSchema);



const usuarioValoracionSchema = new mongoose.Schema(
  {
    rating: Number,
    coment: String,
    likes: Number,
    dislikes: Number,
    Orden: ordenesSchema,
    producto: { type: mongoose.Schema.Types.ObjectId, ref: 'productos' },
    cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
    profesional: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const UsuarioValoracion = mongoose.model(
  'UsuarioValoracion',
  usuarioValoracionSchema
);

const productVisitarSchema = new mongoose.Schema(
  {
    producto_id: String,
    mes_id: Number,
    ano: Number,
    visitas: Number
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const ProductoVisitar = mongoose.model('productoVisitar', productVisitarSchema);

const notificacionSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
    cliente:{type: mongoose.Schema.Types.ObjectId, ref: 'usuarios'},
    profesional: { type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' },
    type:{ type: mongoose.Schema.Types.String },
    read: {type: mongoose.Schema.Types.Boolean, default: false},
    createdAt:{ type:mongoose.Schema.Types.Date,default: Date.now },
    orden: { type: mongoose.Schema.Types.ObjectId, ref: 'ordenes' },
  }
);
const Notification = mongoose.model('notification', notificacionSchema);


const conversationsSchema = new mongoose.Schema(
  {
    profesional: {type: mongoose.Schema.Types.ObjectId, ref: 'usuarios'},
    cliente: {type: mongoose.Schema.Types.ObjectId, ref: 'usuarios'},
    messagechat: [{
      _id: String,
      text: String,
      read: {type: mongoose.Schema.Types.Boolean, default: false},
      createdAt: String,
      user: {
        _id:{type: mongoose.Schema.Types.ObjectId, ref: 'usuarios'},
        name: String,
        avatar: String  
      }
    }]
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
const Conversation = mongoose.model('conversations', conversationsSchema);

export {
  UsuarioFavoritoProducto,
  UsuarioDireccion,
  UsuarioDeposito,
  UsuarioValoracion,
  UsuarioPago,
  Categories,
  Clientes,
  Productos,
  Usuarios,
  Cupones,
  Ordenes,
  ProductoVisitar,
  Notification,
  Inspiration,
  UsuarioFavoritoInspiracion,
  Conversation
};
