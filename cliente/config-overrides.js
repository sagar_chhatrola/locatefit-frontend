const { override, fixBabelImports, addLessLoader, disableEsLint } = require("customize-cra");

module.exports = override(
  disableEsLint(),
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: false
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#95CA3E" }
  }),
);
