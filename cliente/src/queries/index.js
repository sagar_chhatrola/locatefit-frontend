import gql from 'graphql-tag';

export const USUARIO_FAVORITO_PRODUCTS = gql`
  {
    getUsuarioFavoritoProductos {
      success
      message
      list {
        id
        productoId
        producto {
          id
          title
          number
          city
          fileList
          professionalRating {
          id
          rate
          coment
          updated_at
          customer {
            id
            nombre
            foto_del_perfil            
          }
          }
          currency
          creator {
            nombre
          }
        }
      }
    }
  }
`;

export const USUARIO_USUARIO_VALORACION = gql`
  {
    getUsuarioValoracion {
      message
      success
      list{
        id
        coment
        rating
        producto
        likes
        dislikes
        cliente
        producto
        Orden
      }
    }
}
`;

export const USUARIO_DIRECCION_QUERY = gql`
  {
    getUsuarioDireccion {
      success
      message
      data {
        id
        nombre
        calle
        ciudad
        provincia
        codigopostal
        telefono
      }
    }
  }
`;

export const USUARIO_DEPOSITO_QUERY = gql`
  query getUsuarioDeposito($page: Int, $dateRange: DateRangeInput) {
    getUsuarioDeposito(page: $page, dateRange: $dateRange) {
      success
      message
      total
      list {
        id
        fecha
        estado
        total
        created_at
      }
    }
  }
`;

export const USUARIO_PAGO_QUERY = gql`
  {
    getUsuarioPago {
      success
      message
      data {
        id
        nombre
        iban
      }
    }
  }
`;

export const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      UserID
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

export const CATEGORIES_QUERY = gql`
  {
    getCategories{
    id
    title
    image
    description
    productos{
      id
    }
  }
  }
`;

export const INSPIRATION_QUERY = gql`{
    getInspiration{
      id
      title
      image
      image1
      image2
      description
    }
  }
`;

export const CATEGORY_QUERY = gql`
  query getCategory($id: ID!, $price: Int, $city: String) {
    getCategory(id: $id, price: $price, city: $city) {
      success
      message
      data {
        id
        title
        productos {
          id
          city
          category_id
          currency
          description

          domingo
          domingo_from
          domingo_to

          jueves
          jueves_from
          jueves_to

          lunes
          lunes_from
          lunes_to

          martes
          martes_from
          martes_to

          miercoles
          miercoles_from
          miercoles_to

          number

          sabado
          sabado_from
          sabado_to

          time
          title

          viernes
          viernes_from
          viernes_to

          fileList

          anadidoFavorito

          created_by

          category {
            id
            title
          }

          creator {
            id
            usuario
            email
            nombre
            apellidos
            ciudad
            telefono
            tipo
            foto_del_perfil
            fotos_tu_dni
            profesion
            descripcion
            fecha_de_nacimiento
            notificacion
            grado
            estudios
            formularios_de_impuesto
            fb_enlazar
            twitter_enlazar
            instagram_enlazar
            youtube_enlazar
            propia_web_enlazar
          }
          professionalRating {
            rate
          }
        }
      }
    }
  }
`;

export const CATEGORIES_QUERY_FOR_AUTOCOMPLETE = gql`
  {
    getCategories {
      id
      title
    }
  }
`;

export const PRODUCTS_QUERY = gql`
  {
    getProductos {
      success
      message
      list {
        id
        city
        category_id
        currency
        description

        domingo
        domingo_from
        domingo_to

        jueves
        jueves_from
        jueves_to

        lunes
        lunes_from
        lunes_to

        martes
        martes_from
        martes_to

        miercoles
        miercoles_from
        miercoles_to

        number

        sabado
        sabado_from
        sabado_to

        time
        title

        viernes
        viernes_from
        viernes_to

        fileList

        created_by

        category {
          id
          title
        }
      }
    }
  }
`;

export const PRODUCT_QUERY = gql`
  query getProducto($id: ID!, $updateProductVisit: Boolean) {
    getProducto(id: $id, updateProductVisit: $updateProductVisit) {
      success
      message
      data {
        id
        city
        category_id
        currency
        description

        domingo
        domingo_from
        domingo_to

        jueves
        jueves_from
        jueves_to

        lunes
        lunes_from
        lunes_to

        martes
        martes_from
        martes_to

        miercoles
        miercoles_from
        miercoles_to

        number

        sabado
        sabado_from
        sabado_to

        time
        title

        viernes
        viernes_from
        viernes_to

        created_by

        fileList

        anadidoFavorito

        visitas

        category {
          id
          title
        }
        creator {
          id
          usuario
          email
          nombre
          apellidos
          ciudad
          telefono
          tipo
          foto_del_perfil
          fotos_tu_dni
          profesion
          descripcion
          fecha_de_nacimiento
          notificacion
          grado
          estudios
          formularios_de_impuesto
          fb_enlazar
          twitter_enlazar
          instagram_enlazar
          youtube_enlazar
          propia_web_enlazar
        }
        ordenes {
          id
          cantidad
          endDate
          time
        }
        professionalRating {
          id
          rate
          coment
          updated_at
          customer {
            id
            nombre
            foto_del_perfil            
          }
        }
      }
    }
  }
`;

export const PRO_QUERY = gql`
  {
    getPro {
      id
      city
      category_id
      currency
      description

      domingo
      domingo_from
      domingo_to

      jueves
      jueves_from
      jueves_to

      lunes
      lunes_from
      lunes_to

      martes
      martes_from
      martes_to

      miercoles
      miercoles_from
      miercoles_to

      number

      sabado
      sabado_from
      sabado_to

      time
      title

      viernes
      viernes_from
      viernes_to

      fileList

      created_by

      category {
        id
        title
      }
      creator {
        id
        usuario
        email
        nombre
        apellidos
        ciudad
        telefono
        tipo
        foto_del_perfil
        fotos_tu_dni
        profesion
        descripcion
        fecha_de_nacimiento
        notificacion
        grado
        estudios
        formularios_de_impuesto
        fb_enlazar
        twitter_enlazar
        instagram_enlazar
        youtube_enlazar
        propia_web_enlazar
      }
    }
  }
`;

export const CLIENTES_QUERY = gql`
  {
    getClientes {
      id
      nombre
      apellido
      productos {
        nombre
        codigo
      }
    }
  }
`;

// Usuarios

export const USUARIO_ACTUAL = gql`
  query obtenerUsuario {
    obtenerUsuario {
      id
      usuario
      email
      nombre
      apellidos
    }
  }
`;

// Cupon
export const GET_CUPON = gql`
  query getCupon($clave: String!) {
    getCupon(clave: $clave) {
      id
      clave
      descuento
      tipo
    }
  }
`;
// orden
export const GET_ORDEN = gql`
  query getOrden($id: ID!) {
    getOrden(id: $id) {
      id
      cupon
      nota
      aceptaTerminos
      direccion {
        id
        nombre
        calle
        ciudad
        provincia
        codigopostal
        telefono
        usuario_id
      }
      producto
      cantidad
      cliente
      profesional
      descuento {
        clave
        descuento
        tipo
      }
    }
  }
`;

export const GET_PRODUCTO_VISITAS = gql`
  query getProductoVisitas($productId: ID!) {
    getProductoVisitas(productId: $productId) {
      success
      message
      list {
        id
        producto_id
        mes_id
        ano
        visitas
      }
    }
  }
`;

export const GET_FULL_ORDEN = gql`
  query getFullOrden($id: ID!) {
    getFullOrden(id: $id) {
      id
      producto {
        id
        anadidoFavorito
        city
        currency
        created_by
        description
        fileList
        number
        time
        title
      }
      cupon {
        id
        descuento
        tipo
        clave
      }
      direccion {
        id
        calle
        ciudad
        codigopostal
        nombre
        telefono
        provincia
        usuario_id
      }
      cliente {
        id
        nombre
        apellidos
        notificacion
        telefono
        foto_del_perfil
        fotos_tu_dni
        fecha_de_nacimiento
        ciudad
        descripcion
        email
        grado
        usuario
        tipo
      }
      cantidad
      profesional {
        id
        nombre
        apellidos
        notificacion
        telefono
        foto_del_perfil
        fotos_tu_dni
        fecha_de_nacimiento
        ciudad
        descripcion
        email
        grado
        usuario
        tipo
      }
      nota
      aceptaTerminos
    }
  }
`;

export const PROFESSIONAL_ORDENES_QUERY = gql`
  query getOrdenesByProfessional($profesional: ID!, $dateRange: DateRangeInput) {
    getOrdenesByProfessional(profesional: $profesional, dateRange: $dateRange) {
      success
      message
      list {
        id
        cupon
        nota
        aceptaTerminos
        endDate
        time
        producto
        cantidad
        cliente
        profesional
        estado
        progreso
        status
        created_at
        product {
          id
          title 
          number
          currency
          fileList
          time
        }
        client {
          nombre
          calle
          ciudad
          provincia
          codigopostal
          telefono
        }
        pagoPaypal {
          idPago
          idPagador
          fechaCreacion
          emailPagador
          nombrePagador
          apellidoPagador
          estadoPago
          moneda
          montoPagado
        }
      }
    }
  }
`;

export const CLIENTE_PEDIDO_QUERY = gql`
  query getPedidosByCliente($cliente: ID!, $dateRange: DateRangeInput) {
    getPedidosByCliente(cliente: $cliente, dateRange: $dateRange) {
      success
      message
      list {
        id
        cupon
        nota
        aceptaTerminos
        producto
        cantidad
        cliente
        estado
        endDate
        time
        progreso
        status
        created_at
        product {
          id
          title 
          number
          currency
          fileList
          time
        }
        pagoPaypal {
          idPago
          idPagador
          fechaCreacion
          emailPagador
          nombrePagador
          apellidoPagador
          estadoPago
          moneda
          montoPagado
        }
        prfsnl {
          id
          nombre
          apellidos
          UserID
          profesion
          notificacion
          telefono
          foto_del_perfil
          fotos_tu_dni
          fecha_de_nacimiento
          ciudad
          descripcion
          email
          grado
          usuario
          tipo
        }
        professionalRating {
          rate
        }
      }
    }
  }
`;

export const PROFESSIONAL_RATING_QUERY = gql`
  {
    getProfessionalRating {
      success
      message
      list {
        id
        coment
        rate
        updated_at
        customer {
          id
          nombre
          apellidos
          foto_del_perfil
        }
      }
    }
  }
`;

export const GET_MESSAGE_CHAT = gql`
query getMessage($cliente: ID!) {
    getMessage (cliente: $cliente){
    message
    success
    messagechats{
      _id
      text
      createdAt
      user{
        _id
        avatar
      }
    }
  }
    }
`;

export const GET_NOTIFICATIONS = gql`
query getNotifications($userId: ID!) {
  getNotifications(userId: $userId) {
      success
      message
      notifications{
        _id
        user{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        profesional{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        cliente{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        orden{
          id
        }
        type
        read
        createdAt
      }
  }
}
`;

export const GET_STATISTICS = gql`
query getStatistics($userId: ID!,$userType: String!) {
  getStatistics(userId: $userId,userType: $userType) {
      success
      message
      data{
        name
        Ene
        Feb
        Mar
        Abr
        May
        Jun
        Jul
        Aug
        Sep
        Oct
        Nov
        Dic
      }
  }
}
`;


