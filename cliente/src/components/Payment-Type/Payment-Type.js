import React, { Component } from "react";
import "./Payment-Type.css";
import Formulariopayment from "./Formulariopayment";
import Transacciones from './transacciones';
import { Icon, DatePicker, Button } from "antd";
import moment from 'moment';

const { RangePicker } = DatePicker;

const dateFormat = 'MM/DD/YYYY'; // compatible con la fecha y hora del servidor para evitar conflictos de API
const fromDate = '11/07/2019';
const toDate = '12/07/2019';


class Payment extends Component {

  state = {
    dateRange: null
  };

  fromDate = fromDate;
  toDate = toDate;

  onDateRangeChange = (dates, dateStrings) => {
    this.fromDate = dateStrings[0];
    this.toDate = dateStrings[1];
  }

  aplicarDateRange = e => {
    e.preventDefault();
    this.setState({ dateRange: { fromDate: this.fromDate, toDate: this.toDate } });
  }

  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>MIS PAGOS</h3>
          <p>
            Añade tu datos de pago para retirar tu ganacias del los servicios prestados, y tambien podrás ver tu transacciones.
          </p>
        </div>

        <div className="cont">
          <span className="subtitle">
            <Icon type="bank" /> Datos bancarios para pagos de servicio realizado.
          </span>
          <Formulariopayment />
        </div>


        <div className="cont">
          <span className="subtitle">
            <Icon type="euro" /> Tu transacciones en Locatefit.
          </span>
          <div className="subtitle322">
              Filtral por fecha <br />
            <RangePicker
              style={{paddingBottom: 20, paddingTop: 10}}
              defaultValue={[moment(fromDate, dateFormat), moment(toDate, dateFormat)]}
              format={dateFormat}
              onChange={this.onDateRangeChange}
            /> <Button onClick={this.aplicarDateRange} style={{marginBottom: 20, marginTop: 10}} type='dashed'>Aplicar</Button>
          </div>

          <Transacciones dateRange={this.state.dateRange} />
          
        </div>

        
      </div>
    );
  }
}

export default Payment;
