import React, { Component } from "react";
import { Query } from 'react-apollo';
import { Table } from "antd";

import { USUARIO_DEPOSITO_QUERY } from "../../queries";

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  getCheckboxProps: record => ({
    disabled: record.name === "Disabled User", // Column configuration not to be checked
    name: record.name
  })
};

const columns = [
  {
    title: "Nº de transacción",
    dataIndex: "id",
    render: text => <p>{text}</p>
  },
  {
    title: "Fecha",
    dataIndex: "fecha",
    render: fecha => new Date(Number(fecha)).toLocaleDateString()
  },
  {
    title: "Estado",
    dataIndex: "estado"
  },
  {
    title: "Total",
    dataIndex: "total",

  },
];

export default class TablaPedidos extends Component {
  state = {
    usuarioDepositos: [],
    pagination: {},
    loading: true,
  };

  refetch = null;

  componentWillReceiveProps({ dateRange }) {
    if (dateRange && this.refetch) {
      this.setState({ loading: true });
      this.refetch({ dateRange }).then(res => {
        if (res && res.data && res.data.getUsuarioDeposito && res.data.getUsuarioDeposito.success) {
          const pagination = { ...this.state.pagination };
          pagination.total = res.data.getUsuarioDeposito.total;

          this.setState({
            loading: false,
            usuarioDepositos: res.data.getUsuarioDeposito.list,
            pagination
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      });
    }
  }

  handleTableChange = (pagination) => {
    this.refetch({ page: pagination.current });
  };

  formatResult = data => {
    if (data && data.getUsuarioDeposito && data.getUsuarioDeposito.success) {
      const pagination = { ...this.state.pagination };
      pagination.total = data.getUsuarioDeposito.total;

      this.setState({
        loading: false,
        usuarioDepositos: data.getUsuarioDeposito.list,
        pagination
      });
    } else {
      this.setState({
        loading: false,
      });
    }

  }

  render() {
    return (
      <Query query={USUARIO_DEPOSITO_QUERY} onCompleted={this.formatResult}>
        {({ refetch }) => {
          this.refetch = refetch;
          return (
            <div className="">
              <Table
                rowSelection={rowSelection}
                columns={columns}
                dataSource={this.state.usuarioDepositos}
                pagination={this.state.pagination}
                loading={this.state.loading}
                onChange={pagination => this.handleTableChange(pagination)}
              />
            </div>
          )
        }}
      </Query>
    )
  }
}
