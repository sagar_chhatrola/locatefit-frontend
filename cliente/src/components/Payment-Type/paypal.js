import React from 'react';
import { PayPalButton } from 'react-paypal-button-v2';
import { Spin } from 'antd';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_FULL_ORDEN } from '../../queries';
import { AGREGAR_PAGO_PAYPAL_ORDEN } from '../../mutations';
import _get from 'lodash.get';
export default ({ orderId = null }) => {
  if (!orderId) {
    return <p>No hay orderId</p>;
  }
  const { loading, error, data } = useQuery(GET_FULL_ORDEN, {
    variables: { id: orderId }
  });
  const [agregarPagoPaypalOrden, { data: respuesta }] = useMutation(
    AGREGAR_PAGO_PAYPAL_ORDEN
  );
  if (loading)
    return (
      <div className='page-loader'>
        <Spin size='large'></Spin>
      </div>
    );
  if (error) return `Error! ${error.message}`;
  let cantidad = null;
  let descuento = null;
  let precioUnidad = null;
  let total = 0;
  let tipoDescuento = null;
  let descuentoFinal = 0;
  if (data) {
    console.log('paypal', { data, orderId });
    cantidad = _get(data, 'getFullOrden.cantidad', null);
    tipoDescuento = _get(data, 'getFullOrden.cupon.tipo', null);
    descuento = _get(data, 'getFullOrden.cupon.descuento', null);
    precioUnidad = _get(data, 'getFullOrden.producto.number', null);
    total = cantidad * precioUnidad;
    switch (tipoDescuento) {
      case 'porcentaje':
        descuentoFinal = total * (descuento / 100);
        break;
      case 'dinero':
        descuentoFinal = descuento;
        break;
    }
    total = total - descuentoFinal;
    console.log('calculos paypal', {
      cantidad,
      tipoDescuento,
      precioUnidad,
      descuento,
      descuentoFinal,
      total
    });
  }

  return (
    <PayPalButton
      amount={total}
      options = {
        {clientId: "AbIQ9Xh0c1qJBUjzLFCVVDnIACiJUwtFiZ02Et76OXoqpTDc92_aqPlhzhBHm2_iIj4F5_ebNE_M9eZD", currency: "EUR"}
      }
      // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
      onSuccess={(details, data) => {
        console.log({ details, data });
        const idPago = _get(data, 'orderID', null);
        const idPagador = _get(data, 'payerID', null);
        const fechaCreacion = _get(details, 'create_time', null);
        const emailPagador = _get(details, 'payer.email_address', null);
        const nombrePagador = _get(details, 'payer.name.given_name', null);
        const apellidoPagador = _get(details, 'payer.name.surname', null);
        const estadoPago = _get(details, 'status', null);
        const moneda = _get(
          details,
          'purchase_units.0.amount.currency_code',
          null
        );
        const montoPagado = _get(
          details,
          'purchase_units.0.amount.value',
          null
        );
        console.log('datos pago paypal', {
          idPago,
          idPagador,
          fechaCreacion,
          emailPagador,
          nombrePagador,
          apellidoPagador,
          estadoPago,
          moneda,
          montoPagado
        });
        const pagoPaypal = {
          idPago,
          idPagador,
          fechaCreacion,
          emailPagador,
          nombrePagador,
          apellidoPagador,
          estadoPago,
          moneda,
          montoPagado
        };
        agregarPagoPaypalOrden({
          variables: { input: { id: orderId, pagoPaypal } }
        });
        setTimeout(() => {
          window.location.href = '/thank-you';
        }, 1000);
      }}
      onError={(param1 = null, param2 = null, param3 = null) => {
        console.log({ param1, param2, param3 });
        setTimeout(() => {
          window.location.href = '/error-payment';
        }, 1000);
      }}
    />
  );
};
