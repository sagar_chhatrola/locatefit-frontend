import React, { Component } from "react";
import { Form, Button, Row, Col, Input, message, Spin } from "antd";
import { Query, Mutation } from 'react-apollo';

import { USUARIO_PAGO_QUERY } from '../../queries';
import { NUEVO_PAGO, ELIMINAR_PAGO } from '../../mutations';

class Formulariopayment extends Component {

  handleSubmit(event, crearPago, refetch) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const input = { ...values };

        crearPago({ variables: { input } }).then(async ({ data: res }) => {

          if (res && res.crearPago && res.crearPago.success) {
            message.success(res.crearPago.message);
            refetch();
            this.props.form.resetFields();
          }
          else if (res && res.crearPago && !res.crearPago.success)
            message.error(res.crearPago.message);

        });

      }
    });
  };

  handleEliminar(e, eliminarPago, id, refetch) {

    e.preventDefault();

    eliminarPago({ variables: { id } }).then(async ({ data }) => {

      if (data && data.eliminarPago && data.eliminarPago.success) {
        message.success(data.eliminarPago.message);
        refetch();
      }
      else if (data && data.eliminarPago && !data.eliminarPago.success)
        message.error(data.eliminarPago.message);


    });

  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 }
    };

    let isDisableForm = false;
    
    return (
      <Query query={USUARIO_PAGO_QUERY}>
        {({ loading, error, data: res, refetch }) => {
          if (loading) {
            return(
              <div className="page-loader">
              <Spin size="large"></Spin>
           </div>
            );
          }
          // if (loading) return null;
          if (error) console.log('error in USUARIO_PAGO_QUERY', error);
          // if (error) return console.log('error in USUARIO_PAGO_QUERY', error);

          if (res && res.getUsuarioPago && res.getUsuarioPago.data)
            isDisableForm = true;
          else
            isDisableForm = false;

          return <Row type="flex" gutter={16}>
            <Col md={12}>
              <Mutation mutation={NUEVO_PAGO}>
                {(crearPago) => {
                  return (
                    <Form {...formItemLayout} onSubmit={e => this.handleSubmit(e, crearPago, refetch)}>
                      <Form.Item {...formItemLayout} label="Nombre Titular">
                        {getFieldDecorator("nombre", {
                          rules: [
                            {
                              required: true,
                              message: "Añade un Titular"
                            }
                          ]
                        })(<Input placeholder="Añade Nombre del titular" disabled={isDisableForm} />)}
                      </Form.Item>

                      <Form.Item {...formItemLayout} label="IBAN Bancario">
                        {getFieldDecorator("iban", {
                          rules: [
                            {
                              required: true,
                              message: "Añade un IBAN Correcto"
                            }
                          ]
                        })(<Input placeholder="ES00 0000 0000 0000 0000 0000"  disabled={isDisableForm}/>)}
                      </Form.Item>

                      <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                        <Button disabled={isDisableForm} type="primary" htmlType="submit">
                          Añadir método de pago
                </Button>
                      </Form.Item>
                    </Form>

                  )
                }}
              </Mutation>
            </Col>
            <Col md={12}>
              <div className="viewDirection">
                <h5>Método de pago actual</h5>
                {(res && res.getUsuarioPago && res.getUsuarioPago.data) ?
                  <payment key={res.getUsuarioPago.data.id}>
                    <br />
                    <h5>{res.getUsuarioPago.data.nombre}</h5>
                    <p className="addres">{res.getUsuarioPago.data.iban}</p>
                    <Mutation mutation={ELIMINAR_PAGO}>
                      {(eliminarPago, { loading, error, data }) => {
                        return (
                          <Button
                            type="danger"
                            className="btnflex"
                            shape="round"
                            icon="delete"
                            style={{ marginRight: 10 }}
                            onClick={e => this.handleEliminar(e, eliminarPago, res.getUsuarioPago.data.id, refetch)}
                          >Eliminar</Button>
                        )
                      }}
                    </Mutation>
                  </payment>
                  : null}
              </div>
            </Col>
          </Row>
        }}
      </Query>
    );
  }
}

const WrappedDemo = Form.create({ name: "validate_other" })(Formulariopayment);

export default WrappedDemo;
