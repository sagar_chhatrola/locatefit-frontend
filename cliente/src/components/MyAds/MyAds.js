import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Myadscard from "./myadscard";
import { Icon } from "antd";

import "./MyAds.css";

class MyAds extends Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>Mis servicios</h3>
          <p>
            Aquí podrás gestionar los servicios que ya tienes y destacarlos para
            venderlos antes
          </p>
        </div>

        <div className="cont">
          <span className="subtitle">
            <Icon type="code-sandbox" />
            Administra y edita tus anuncios
          </span>
          <Myadscard />
        </div>
      </div>
    );
  }
}

export default MyAds;