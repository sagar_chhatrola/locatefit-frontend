import React, { Component } from "react";
import { Query, Mutation } from 'react-apollo';
import { Card, Icon, Modal, Popconfirm, message, Spin, Empty } from "antd";
import { PRODUCTS_QUERY } from './../../queries'
import { ELIMINAR_PRODUCT } from './../../mutations'

import EditProductModal from "./edit-product.modal";
import Estadisticas from './../estadisticas/visitasform';

import "antd/dist/antd.css";
import "./MyAds.css"
import { LOCAL_API_URL } from './../../config';

let productData = {};
let forceRefetchQuery;
function confirm(e, eliminarProducto, id, refetch) {

  e.preventDefault();

  eliminarProducto({ variables: { id } }).then(async ({ data }) => {

    if (data && data.eliminarProducto && data.eliminarProducto.success) {
      message.success(data.eliminarProducto.message);
      refetch();
    }
    else if (data && data.eliminarProducto && !data.eliminarProducto.success)
      message.error(data.eliminarProducto.message);


  });

}

function cancel(e) {
  message.error('Has cancelado la eliminación de este anuncio');
}

const { Meta } = Card;

class Myadscard extends Component {


  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      currentShowingProductId: null
    };

    this.onProductUpdate = this.onProductUpdate.bind(this);
    this.showModal1 = this.showModal1.bind(this);
  }

  state = { visible1: false };

  showModal1 = (e, productId) => {
    e.preventDefault();
    this.setState({
      currentShowingProductId: productId,
      visible1: true
    });
  };

  handleCancel1 = e => {
    console.log(e);
    this.setState({
      visible1: false,
      // currentShowingProductId: null
    });
  };

  editModalShow(e, producto, refetch) {
    e.preventDefault();
    productData = producto;
    this.setState({ visible: true });
    forceRefetchQuery = refetch;
  };

  onProductUpdate(status) {
    if (status) {
      this.setState({ visible: false });
      if (forceRefetchQuery) forceRefetchQuery();
    }
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <div>
        <Query query={PRODUCTS_QUERY}>
          {({ loading, error, data, refetch }) => {
            if (loading) {
              return (
                <div className="page-loader">
                  <Spin size="large"></Spin>
                </div>
              );
            }
            if (error) return console.log('error in PRODUCTS_QUERY', error);

            return <div className="category-card-producto">
            { data.getProductos.list.length > 0 ?
            <div className="category-card-producto">
              {
                data.getProductos.list.map(
                  (producto, i) => (
                    <Card
                    style={{width: 230, margin: 10}}
                      key={producto.id}
                      cover={producto.fileList.length > 0 ? <div>
                        <a href={'/deals/' + producto.id}>
                        <img
                          className="imagenprodt1"
                          alt="Imagen servicio"
                          src={producto.fileList.length > 0 ? LOCAL_API_URL + "/assets/images/" + producto.fileList[0] : ""}
                        /></a>
                      </div> : null}
                      actions={[
                        <a
                          title="Editar"
                          onClick={e => this.editModalShow(e, producto, refetch)}
                          href="Javascript:void;"
                        >
                          <Icon type="edit" key="testEdit" />
                        </a>,
                        <Mutation mutation={ELIMINAR_PRODUCT}>
                          {(eliminarProducto, { loading, error, data }) => {
                            return (
                              <Popconfirm
                                title="Estas seguro que deseas eliminar este anuncio"
                                onConfirm={e => confirm(e, eliminarProducto, producto.id, refetch)}
                                onCancel={cancel}
                                okText="Si"
                                cancelText="No"
                              >
                                <Icon type="delete" key="delete" />
                              </Popconfirm>
                            )
                          }}
                        </Mutation>,

                        <a
                          title="Estadísticas"
                          onClick={e => this.showModal1(e, producto.id)}
                          href="Javascript:void;"
                        >
                          <Icon type="line-chart"/>
                        </a>

                      ]}
                    >
                      <Meta
                        title={producto.title}
                      />

                      <Meta
                        title={producto.number + "€" + producto.currency}
                        description={producto.city}
                      />
                    </Card>
                  ))
              }
            </div> 
            : null 
          }

          {(data.getProductos.list.length === 0 || !data.getProductos.list) ?
            <div style={{textAlign: 'center', justifyContent: 'center'}}>
            <Empty
            image='https://user-images.githubusercontent.com/507615/54591670-ac0a0180-4a65-11e9-846c-e55ffce0fe7b.png'
            imageStyle={{
              height: 60
            }}
            description={
              <span>
                <h3>
                  <Icon type='notification' theme='twoTone' />
                </h3>
                  Aún no has publicado nungún servicio.
               </span>
            }
          ></Empty>
            </div>
            : null }
            </div>
          }}
        </Query>

        <Modal
          width="70%"
          title="Editar servicio"
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <EditProductModal onProductUpdate={this.onProductUpdate} data={productData} />
        </Modal>

        <Modal
          className='modalestadisticas'
          footer={null}
          title="Estadisticas del anuncio"
          visible={this.state.visible1}
          onCancel={this.handleCancel1}
          destroyOnClose={true}
        >
          <Estadisticas productId={this.state.currentShowingProductId} />
        </Modal>


      </div>
    );
  }
}

export default Myadscard;
