import React, { Component } from "react";
import { Query, Mutation } from 'react-apollo';
import { Input, Form, Select, Checkbox, Popover, TimePicker, Icon, Button, message } from "antd";
import moment from "moment";
import { CATEGORIES_QUERY } from '../../queries';
import { EDITAR_PRODUCT, UPLOAD_FILE } from '../../mutations';
import { LOCAL_API_URL } from './../../config';
import locale from 'antd/es/date-picker/locale/es_ES'

import "./edit-product.modal.css";

const imgBaseUrl = LOCAL_API_URL + '/assets/images/';

const infotittle = (<div> <p> Elige un titulo fácil con palabras llanas para mayor resultado en la busqueda. </p> </div>);
const infoprecio = (<div> <p>Elige el precion de tu servicio que sea justo y considerable.</p> </div>);
const infomoneda = (<div> <p>Aquí podras seleccionar el modo de tiempo.</p> </div>);
const infohora = (<div> <p>Elige la duración de tu servicio.</p> </div>);
const infodescricion = (<div> <p>La descricion es la parte fundamental a la hora de contratar un servicio se epecifico.</p> </div>);
const infociudad = (<div> <p>Seleciona la ciudad donde quieres prestar tus servicios.</p> </div>);
const infocate = (<div> <p>Seleciona la categría que más de asemeje a tu servicio.</p> </div>);

const { TextArea } = Input;
const timeFormatWithSeconds = 'hh:mm:ss';
const timeFormatWithoutSeconds = 'hh:mm';


function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class EditProductModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            imageUploadLoading: false,
            fileList: (this.props.data && this.props.data.fileList) || [],
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(event) {
        console.log('handleChange event: ', event);
        this.setState({ value: event.target.value });
    }

    handleSubmit(event, editarProducto) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {

            if (!err) {
                this.setState({ loading: true });

                values.lunes = values.lunes[0];
                values.martes = values.martes[0];
                values.miercoles = values.miercoles[0];
                values.jueves = values.jueves[0];
                values.viernes = values.viernes[0];
                values.sabado = values.sabado[0];
                values.domingo = values.domingo[0];

                values.number = Number(values.number);

                if (values.time) values.time = this.createTimeString(values.time);
                if (values.lunes_from) values.lunes_from = this.createTimeString(values.lunes_from);
                if (values.lunes_to) values.lunes_to = this.createTimeString(values.lunes_to);
                if (values.martes_from) values.martes_from = this.createTimeString(values.martes_from);
                if (values.martes_to) values.martes_to = this.createTimeString(values.martes_to);
                if (values.miercoles_from) values.miercoles_from = this.createTimeString(values.miercoles_from);
                if (values.miercoles_to) values.miercoles_to = this.createTimeString(values.miercoles_to);
                if (values.jueves_from) values.jueves_from = this.createTimeString(values.jueves_from);
                if (values.jueves_to) values.jueves_to = this.createTimeString(values.jueves_to);
                if (values.viernes_from) values.viernes_from = this.createTimeString(values.viernes_from);
                if (values.viernes_to) values.viernes_to = this.createTimeString(values.viernes_to);
                if (values.sabado_from) values.sabado_from = this.createTimeString(values.sabado_from);
                if (values.sabado_to) values.sabado_to = this.createTimeString(values.sabado_to);
                if (values.domingo_from) values.domingo_from = this.createTimeString(values.domingo_from);
                if (values.domingo_to) values.domingo_to = this.createTimeString(values.domingo_to);

                const input = { id: this.props.data.id, fileList: this.state.fileList, ...values };
                editarProducto({ variables: { input } }).then(async ({ data }) => {

                    if (data && data.editarProducto && data.editarProducto.success) {
                        this.setState({ loading: false });
                        message.success(data.editarProducto.message);
                        this.props.onProductUpdate(true);
                    }
                    else if (data && data.editarProducto && !data.editarProducto.success)
                        message.error(data.editarProducto.message);

                });

            }
        });
    }

    createTimeString(date) {
        if (!date) return '';
        let momentDate = moment(date);
        return `${momentDate.hours()}:${momentDate.minutes()}`;
    }

    handleEliminar(e, file) {

        e.preventDefault();

        const files = this.state.fileList.filter(f => f !== file);

        this.setState({ fileList: files }, () => {
            const { onChange } = this.props;
            if (onChange) {
                onChange({
                    ...this.state,
                });
            }
        });

    }


    render() {
        const { size } = this.props;
        const { getFieldDecorator } = this.props.form;

        return (
            <div className="container mt-5">
                <div className="card-title">
                    <h3>Editar anuncio</h3>
                    <p>Aquí podrás editar tu servicios</p>
                </div>
                <Mutation mutation={EDITAR_PRODUCT}>
                    {(editarProducto) => {
                        return (
                            <Form onSubmit={e => this.handleSubmit(e, editarProducto)}>

                                {/* Infoproducto Starts */}
                                <div className="cont">
                                    <span className="subtitle"> <Icon type="info" /> INFORMACIÓN DE TU PRODUCTO </span>
                                    <div className="titulodelprod">
                                        <Form.Item label="¿Qué estás servicio estas ofresiendo?">
                                            <Popover content={infotittle} trigger="click">
                                                {getFieldDecorator("title", {
                                                    rules: [{ required: true, message: 'Este campo es requerido.' }],
                                                    initialValue: this.props.data.title
                                                })(<Input placeholder="En pocas palabras" />)}
                                            </Popover>
                                        </Form.Item>
                                    </div>

                                    <span>
                                        <Form.Item label="Precio y duración del servicio">
                                            <Popover content={infoprecio} trigger="click">
                                                {getFieldDecorator("number", {
                                                    rules: [{ required: true, message: 'Este campo es requerido.' }],
                                                    initialValue: this.props.data.number
                                                })(<Input placeholder="0,00" type="text" size={size} style={{ width: "20%", marginRight: "1%" }} />)}
                                            </Popover>

                                            <Popover content={infomoneda} trigger="hover">
                                                {getFieldDecorator("currency", {
                                                    initialValue: this.props.data.currency || '/Hora' // must be this
                                                })(<Select placeholder="/Hora" size={size} style={{ width: "10%", marginRight: "3%" }} >
                                                    <Select.Option value="/Hora">/Hora</Select.Option>
                                                    <Select.Option value="/Servicio">/Servicio</Select.Option>
                                                    <Select.Option value="/M²">/M²</Select.Option>

                                                </Select>)}
                                            </Popover>

                                            <Popover content={infohora} trigger="hover">
                                                {getFieldDecorator("time", {
                                                    rules: [{ type: 'object', required: true, message: 'Este campo es requerido.' }], // not working
                                                    initialValue: this.props.data.time ? moment(this.props.data.time, timeFormatWithSeconds) : null
                                                })(<TimePicker locale={locale} />)}
                                            </Popover>
                                        </Form.Item>
                                    </span>

                                    <div className="categi">
                                        <p>Categoría:</p>

                                        <Query query={CATEGORIES_QUERY}>
                                            {({ loading, error, data, refetch }) => {
                                                if (loading) return null;
                                                if (error) return console.log('error in CATEGORIES_QUERY: ', error);
                                                return <Form.Item label="" >
                                                    <Popover content={infocate} trigger="hover">
                                                        {getFieldDecorator("category_id", {
                                                            rules: [{ required: true, message: 'Este campo es requerido.' }],
                                                            initialValue: this.props.data.category_id
                                                        })(<Select
                                                            showSearch
                                                            style={{ width: "100%" }}
                                                            placeholder="Seleciona una categoría"
                                                            optionFilterProp="children"
                                                            filterOption={(input, option) =>
                                                                option.props.children
                                                                    .toLowerCase()
                                                                    .indexOf(input.toLowerCase()) >= 0
                                                            }
                                                        >
                                                            {
                                                                data.getCategories.map(
                                                                    (cat, i) => (
                                                                        <Select.Option value={cat.id} key={i}>{cat.title}</Select.Option>
                                                                    )
                                                                )
                                                            }
                                                        </Select>)}
                                                    </Popover>
                                                </Form.Item>
                                            }}
                                        </Query>
                                    </div>
                                    
                                    <Form.Item label="Descripción">
                                        <Popover content={infodescricion} trigger="click">
                                            {getFieldDecorator("description", {
                                                rules: [{ required: true, message: 'Este campo es requerido.' }],
                                                initialValue: this.props.data.description
                                            })(<TextArea
                                                placeholder="Añade detalles ecenciales de tu sevicio."
                                                autosize={{ minRows: 3, maxRows: 5 }}
                                            />)}
                                        </Popover>
                                    </Form.Item>
                                </div>
                                {/* Infoproducto Ends */}

                                {/* Fotoproductos Starts */}
                                <div className="cont">
                                    <span className="subtitle"><Icon type="picture" /> FOTOS </span>
                                    <div className="clearfix">
                                        {this.state.fileList.map((file) => (
                                            <div className="imGpreview">
                                                <img style={{ width: '108px', height: '110px', marginTop: 10 }} src={imgBaseUrl + file} key={file} /><br />

                                                <Icon type="delete" onClick={e => this.handleEliminar(e, file)} />

                                            </div>
                                        ))}
                                        <Mutation mutation={UPLOAD_FILE}>
                                            {(singleUpload) => (
                                                <label>
                                                    <input
                                                        style={{ display: 'none' }}
                                                        type="file"
                                                        required
                                                        onChange={async ({ target: { validity, files: [file] } }) => {
                                                            this.setState({ imageUploadLoading: true });
                                                            let imgBlob = await getBase64(file);
                                                            validity.valid && singleUpload({ variables: { imgBlob } }).then((res) => {
                                                                this.setState({ fileList: [...this.state.fileList, res.data.singleUpload.filename] });
                                                                this.setState({ imageUploadLoading: false });
                                                            }).catch(error => {
                                                                this.setState({ imageUploadLoading: false });
                                                                console.log('fs error: ', error)
                                                            })
                                                        }}
                                                    />
                                                    {this.state.fileList.length <= 4 ? this.state.imageUploadLoading ? <div className="image-loader"></div> :
                                                        <div className="ant-upload-1">
                                                            <Icon type="plus" />
                                                            <div className="ant-upload-text">Añadir imagenes</div>
                                                        </div> : null}

                                                </label>
                                            )}
                                        </Mutation>
                                    </div>

                                </div>
                                {/* Fotoproductos Ends */}

                                {/* Disponibilidad Starts */}
                                <div className="cont">
                                    <span className="subtitle"> <Icon type="calendar" /> INFORMACIÓN DE DISPONIBILIDAD </span>

                                    <div className="dias">
                                        <p>Seleciona los días que tienes diponible</p>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("lunes", {
                                                initialValue: [this.props.data.lunes]
                                            })(<Checkbox.Group>
                                                <Checkbox value="Lunes" style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Lunes
                                    </Checkbox>
                                            </Checkbox.Group>)}



                                            {getFieldDecorator("lunes_from", {
                                                initialValue: this.props.data.lunes_from ? moment(this.props.data.lunes_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                            {getFieldDecorator("lunes_to", {
                                                initialValue: this.props.data.lunes_to ? moment(this.props.data.lunes_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("martes", {
                                                initialValue: [this.props.data.martes]
                                            })(<Checkbox.Group>
                                                <Checkbox value="Martes" style={{ top: "10px", bottom: "20px", width: "120px" }}>
                                                    Martes
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("martes_from", {
                                                initialValue: this.props.data.martes_from ? moment(this.props.data.martes_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                            {getFieldDecorator("martes_to", {
                                                initialValue: this.props.data.martes_to ? moment(this.props.data.martes_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("miercoles", {
                                                initialValue: [this.props.data.miercoles]
                                            })(<Checkbox.Group>
                                                <Checkbox value='Miércoles' style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Miércoles
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("miercoles_from", {
                                                initialValue: this.props.data.miercoles_from ? moment(this.props.data.miercoles_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                            {getFieldDecorator("miercoles_to", {
                                                initialValue: this.props.data.miercoles_to ? moment(this.props.data.miercoles_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("jueves", {
                                                initialValue: [this.props.data.jueves]
                                            })(<Checkbox.Group>
                                                <Checkbox value='Jueves' style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Jueves
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("jueves_from", {
                                                initialValue: this.props.data.jueves_from ? moment(this.props.data.jueves_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                            {getFieldDecorator("jueves_to", {
                                                initialValue: this.props.data.jueves_to ? moment(this.props.data.jueves_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}


                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("viernes", {
                                                initialValue: [this.props.data.viernes]
                                            })(<Checkbox.Group>
                                                <Checkbox value='Viernes' style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Viernes
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("viernes_from", {
                                                initialValue: this.props.data.viernes_from ? moment(this.props.data.viernes_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                            {getFieldDecorator("viernes_to", {
                                                initialValue: this.props.data.viernes_to ? moment(this.props.data.viernes_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("sabado", {
                                                initialValue: [this.props.data.sabado]
                                            })(<Checkbox.Group>
                                                <Checkbox value='Sábado' style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Sábado
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("sabado_from", {
                                                initialValue: this.props.data.sabado_from ? moment(this.props.data.sabado_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                            {getFieldDecorator("sabado_to", {
                                                initialValue: this.props.data.sabado_to ? moment(this.props.data.sabado_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="horas">
                                        <Form.Item label="">
                                            {getFieldDecorator("domingo", {
                                                initialValue: [this.props.data.domingo]
                                            })(<Checkbox.Group>
                                                <Checkbox value='Domingo' style={{ top: "10px", bottom: "20px", width: "120px" }} >
                                                    Domingo
                                    </Checkbox>
                                            </Checkbox.Group>)}
                                            {getFieldDecorator("domingo_from", {
                                                initialValue: this.props.data.domingo_from ? moment(this.props.data.domingo_from, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}

                                            {getFieldDecorator("domingo_to", {
                                                initialValue: this.props.data.domingo_to ? moment(this.props.data.domingo_to, timeFormatWithoutSeconds) : null
                                            })(<TimePicker
                                                locale={locale}
                                                style={{
                                                    top: "10px",
                                                    bottom: "20px",
                                                    width: "15%",
                                                    marginRight: "3%",
                                                    marginLeft: "3%"
                                                }}

                                                format={timeFormatWithoutSeconds}
                                            />)}
                                        </Form.Item>
                                    </div>

                                    <div className="botonpublic">
                                        <Button
                                            onClick={e => this.handleSubmit(e, editarProducto)}
                                            type="primary"
                                            loading={this.state.loading}
                                        >
                                            Actualizar
                                </Button>
                                    </div>
                                </div>
                                {/* Disponibilidad Ends */}

                            </Form>
                        )
                    }}
                </Mutation>
            </div>
        );
    }
}

const WrappedEditProductModal = Form.create({ name: "EditProductModalForm" })(EditProductModal);

export default WrappedEditProductModal;
// export default EditProductModal;
