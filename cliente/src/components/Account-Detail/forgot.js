import React, { Component } from 'react';
import { Form, Button, Input, Row } from 'antd';
import './Account-Detail.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { LOCAL_API_URL } from './../../config';

class Forgot extends Component {
  state = {
    id: '',
    isInvalid: true,
    password: '',
    confirmPassword: '',
    redirect: false
  };

  password = e => {
    console.log(e.target.value);
    this.setState(
      {
        password: e.target.value
      },
      () => {
        if (
          this.state.password === this.state.confirmPassword &&
          this.state.password !== ''
        ) {
          this.setState({
            isInvalid: false
          });
        }
      }
    );
  };
  confirmPassword = e => {
    this.setState(
      {
        confirmPassword: e.target.value
      },
      () => {
        if (
          this.state.password === this.state.confirmPassword &&
          this.state.password !== ''
        ) {
          this.setState({
            isInvalid: false
          });
        }
      }
    );
  };
  handleSubmit = e => {
    e.preventDefault();
    this.setState({
      redirect: true
    });
    const url =  LOCAL_API_URL + '/resetPassword';
    axios
      .post(url, {
        password: this.state.password,
        email: this.props.email,
        token: this.props.token
      })
      .then(res => {
        // console.log(res.data);
      })
      .catch(err => {
        console.log('error:', err);
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    } else
      return (
        <Row>
          <div className="reetablecer">
            <h3>Reetablecer contraseña</h3>
            <p>aqui podrás reetablecer tu contraseña.</p>

            <Form onSubmit={this.handleSubmit}>
              <Input.Password
                className="reetablecer"
                placeholder="Nueva contraseña"
                onChange={this.password}
              />
              <Input.Password
                className="reetablecer"
                placeholder="Confirmar contraseña"
                onChange={this.confirmPassword}
              />

              <Button
                type="secondary"
                htmlType="submit"
                disabled={this.state.isInvalid}
              >
                Reetablecer contraseña
              </Button>
            </Form>
          </div>
        </Row>
      );
  }
}

export default Forgot;
