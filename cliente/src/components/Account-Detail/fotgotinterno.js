import React, { Component } from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import './../../containers/recover-password/recover-password.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { LOCAL_API_URL } from './../../config';

class Formulario extends Component {
  state = {
    redirect: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          redirect: true
        });
        // console.log('Received values of form: ', values);
        const url = LOCAL_API_URL + '/forgotpassword';
        axios
          .post(url, values)
          .then(res => {})
          .catch(err => {
            console.log('error:', err);
          });
      }
      setTimeout(() => {
        localStorage.removeItem('token', '');
        localStorage.removeItem('id', '');
        setTimeout(() => window.location.href = '/login', 300);
      }, 200);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    if (this.state.redirect) {
      return  message.success('Te hemos enviado un email para que puedas cambiar tu contraseña');
    } else
      return (
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [
                { required: true, message: 'Por favor ingrese su Email!' }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="Correo electrónico"
              />
            )}
          </Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Enviadme el enlace
          </Button>
          <br />
        </Form>
      );
  }
}

const WrappedDemo = Form.create({ name: 'validate_other' })(Formulario);

export default WrappedDemo;
