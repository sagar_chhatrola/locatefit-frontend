import React, { Component } from "react";
import {
  Form,
  Select,
  Switch,
  Radio,
  Slider,
  Button,
  Row,
  Input,
  Upload,
  Icon,
  DatePicker,
  message,
  Spin,
  Modal
} from "antd";
import { Query, Mutation } from 'react-apollo';

import moment from "moment";
import { USER_DETAIL } from "../../queries";
import { UPLOAD_FILE, ACTUALIZAR_USUARIO, ELIMINAR_USUARIO } from "../../mutations";
import "./Account-Detail.css";
import { LOCAL_API_URL } from './../../config';
import locale from 'antd/es/date-picker/locale/es_ES'


function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}


function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('Solo puedes subir archivos JPG/PNG!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('La imagen debe ser inferior a 2MB!');
  }
  return isJpgOrPng && isLt2M;
}


function onFocus() {
  // console.log("focus");
}
function onBlur() {
  // console.log("blur");
}
function onSearch(val) {
  // console.log("search:", val);
}





//formulario de detalles del usuario select

class Formularioaccount extends Component {

  constructor(props) {
    super(props);
     this.state = {
      errors: {},
      geoLocationEnabled: false,
      city: (this.props.value && this.props.value.city) || null,
     };

     this.getGeoLocation();
   }

  getGeoLocation() {
    if ("geolocation" in navigator) {

      navigator.geolocation.getCurrentPosition(
        (position) => {

          let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
          fetch(apiUrlWithParams)
            .then(response => response.json())
            .then(data => {

              let cityFound = false;

              for (let index = 0; index < data.results.length; index++) {

                for (let i = 0; i < data.results[index].address_components.length; i++) {

                  if (data.results[index].address_components[i].types.includes('locality')) {

                    this.setState({ city: data.results[index].address_components[i].long_name });
                    this.setState({ geoLocationEnabled: true });
                    cityFound = true;

                  }

                  if (cityFound) break;
                }

                if (cityFound) break;
              }

            })
            .catch(error => console.log('error in google geocode api: ', error));

        },
        (error) => console.log('geolocation error', error),


      );
    } else {
      console.log('this browser not supported HTML5 geolocation API')
    }
  }


  state = {
    loading: false,
    imageUrl: ''
  }


  handleChange = info => {
    if (info.file.status === 'Cargando') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'Hecho') {
      // Get this url from response in real world.

      // Arif for testing purpose
      // getBase64(info.file.originFileObj, imageUrl =>
      //   this.setState({
      //     imageUrl,
      //     loading: false,
      //   }),
      // );
    }
  };

  onCityChange = (value) => {

    this.setState(prevState => ({
      errors: { ...prevState.errors, city: false }
    }))
  
    this.setState({ city: value }, () => {
      this.triggerChange({ city: value });
    });
  };

  //formulario de detalles del usuario

  normFile = e => {
    console.log("normFile Upload event:", e);
    // if (Array.isArray(e)) {
    //   return e;
    // }
    // return e && e.fileList;
  };

  updateProfilePicUrl = filename => {
    if (filename) this.setState({ imageUrl: LOCAL_API_URL`/assets/images/${filename}` });
  };



  handleEliminar(e, eliminarUsuario, id) {

    e.preventDefault();

    const { confirm } = Modal;

    confirm({
      title: '¿Estás seguro de que deseas eliminar tu cuenta?',
      onOk() {
        return new Promise((resolve, reject) => {
          eliminarUsuario({ variables: { id } }).then(async ({ data }) => {

            if (data && data.eliminarUsuario && data.eliminarUsuario.success) {
              setTimeout(() => {
                localStorage.removeItem('token', '');
                localStorage.removeItem('id', '');

                message.success(data.eliminarUsuario.message);

                setTimeout(() => window.location.href = '/register', 300);

              }, 200);
              resolve();
            }
            else if (data && data.eliminarUsuario && !data.eliminarUsuario.success)
              setTimeout(() => {
                message.error(data.eliminarUsuario.message);
              }, 500);
            reject();
          });
        }).catch(() => console.log('error confirming eliminar usuario!'));
      },
      onCancel() { },
    });

  }

  render() {

    const srvrImgDirUrl = LOCAL_API_URL + '/assets/images/';
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Añadir foto del perfil</div>
      </div>
    );
    const { imageUrl } = this.state;

    //formulario de detalles del usuario

    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 }
    };

    const { TextArea } = Input;


    //fecha

    const dateFormat = "DD/MM/YYYY";
    let id = localStorage.getItem('id');
    const fotoDelPerfil = null;

    const ciudad = [];
    for (let i = 3; i === 3; i++) {
      ciudad.push(
      
      );
    }

    return (
      <div>
        <Mutation mutation={ACTUALIZAR_USUARIO}>
          {(actualizarUsuario) => (

            <Query query={USER_DETAIL} variables={{ id }}>
              {({ loading, error, data }) => {
                if (loading) {
                  return (
                    <div className="page-loader">
                      <Spin size="large"></Spin>
                    </div>
                  );
                }
                if (error) return console.log('error getting user detail: ', error);

                return <Row type="flex" gutter={16}>
                  {/* {data.getUsuario.foto_del_perfil ? this.updateProfilePicUrl(data.getUsuario.foto_del_perfil) : null} */}
                  <Form {...formItemLayout} onSubmit={e => {
                    e.preventDefault();
                    this.props.form.validateFields((err, values) => {
                      console.log('in render err: ', err);
                      console.log('in render values: ', values);
                      if (!err) {
                        const input = {
                          id: localStorage.getItem('id'),
                          email: values.email,
                          nombre: values.nombre,
                          apellidos: values.apellidos,
                          ciudad: this.state.city,
                          telefono: values.telefono,
                          foto_del_perfil: values.foto_del_perfil,
                          fotos_tu_dni: values.fotos_tu_dni,
                          profesion: values.profesion,
                          descripcion: values.descripcion,
                          fecha_de_nacimiento: values.fecha_de_nacimiento,
                          notificacion: values.notificacion ? 1 : 0,
                          grado: values.grado,
                          estudios: values.estudios,
                          formularios_de_impuesto: values.formularios_de_impuesto,
                          fb_enlazar: values.fb_enlazar,
                          twitter_enlazar: values.twitter_enlazar,
                          instagram_enlazar: values.instagram_enlazar,
                          youtube_enlazar: values.youtube_enlazar,
                          propia_web_enlazar: values.propia_web_enlazar
                        };
                        console.log('input; ', input)
                        actualizarUsuario({ variables: { input } }).then(res => {
                          message.success('El perfil ha sido actualizado exitosamente');
                        }).catch(err => message.error('Algo salió mal. Por favor, vuelva a intentarlo'))
                      }
                    });
                  }}>
                    <div>
                      <h3>Foto del perfil</h3>
                      <p>Aceptamos formatos .jpg, .png y mínimo 400px x 400px</p>
                    </div>

                    <Form.Item>
                      {getFieldDecorator("foto_del_perfil", {
                        initialValue: data.getUsuario.foto_del_perfil
                      })(
                        <Mutation mutation={UPLOAD_FILE}>
                          {(singleUpload) => (
                            <Upload
                              name="avatar"
                              listType="picture-card"
                              className="avatar-uploader"
                              showUploadList={false}
                              customRequest={async (data) => {
                                console.log('para ver que data', data)
                                let imgBlob = await getBase64(data.file);
                                singleUpload({ variables: { imgBlob } }).then((res) => {
                                  this.props.form.setFieldsValue({
                                    foto_del_perfil: res.data.singleUpload.filename,
                                  });
                                  // this.updateProfilePicUrl(res.data.singleUpload.filename)
                                }).catch(error => {
                                  console.log('fs error: ', error)
                                })
                              }}
                            >
                              {getFieldValue('foto_del_perfil') ? <img src={srvrImgDirUrl + getFieldValue('foto_del_perfil')} alt="avatar" style={{ width: '100%' }} /> : uploadButton}

                            </Upload>
                          )}
                        </Mutation>
                      )}
                    </Form.Item>


                    <div>
                      <h3>Datos Personales</h3>
                      <p>Añade tus datos personales y profesionales.</p>

                      <Form.Item {...formItemLayout} label="Nombre">
                        {getFieldDecorator("nombre", {
                          rules: [
                            {
                              required: true,
                              message: "Agrega tu nombre"
                            }
                          ],
                          initialValue: data.getUsuario.nombre
                        })(<Input placeholder="Nombre" />)}
                      </Form.Item>

                      <Form.Item {...formItemLayout} label="Apellidos">
                        {getFieldDecorator("apellidos", {
                          rules: [
                            {
                              required: true,
                              message: "Agrega tu apellidos"
                            }
                          ],
                          initialValue: data.getUsuario.apellidos
                        })(<Input placeholder="Apellidos" />)}
                      </Form.Item>

                      <Form.Item {...formItemLayout} label="Profesión">
                        {getFieldDecorator("profesion", {
                          rules: [
                            {
                              required: true,
                              message: "Agrega una profesión"
                            }
                          ],
                          initialValue: data.getUsuario.profesion
                        })(<Input placeholder="Profesión" />)}
                      </Form.Item>


                      <Form.Item {...formItemLayout} label="Correo">
                        {getFieldDecorator("email", {
                          rules: [
                            {
                              required: true,
                              message: "Agrega tu Correo electronico"
                            }
                          ],
                          initialValue: data.getUsuario.email
                        })(<Input type="email" placeholder="Correo Electronico" />)}
                      </Form.Item>

                      <Form.Item {...formItemLayout} label="Telefono">
                        {getFieldDecorator("telefono", {
                          rules: [
                            {
                              required: true,
                              message: "Agrega tu número movil"
                            }
                          ],
                          initialValue: data.getUsuario.telefono
                        })(<Input type="number" placeholder="Número movil" />)}
                      </Form.Item>

                      <Form.Item {...formItemLayout} label="Descripción">
                        {getFieldDecorator("descripcion", {
                          rules: [
                            {
                              required: true,
                              message: "Describete mejor"
                            }
                          ],
                          initialValue: data.getUsuario.descripcion
                        })(<TextArea placeholder="Descríbete para que te conozcan" />)}
                      </Form.Item>

                      <Form.Item label="Ciudad" hasFeedback>
                      {this.state.geoLocationEnabled ?
                          <Input disabled={true} value={this.state.city} />
                        :
                            <Select
                              showSearch
                              style={{ width: "100%" }}
                              placeholder="Seleciona tu ciudad"
                              optionFilterProp="children"
                              onChange={this.onCityChange}
                              onFocus={onFocus}
                              onBlur={onBlur}
                              onSearch={onSearch}
                              filterOption={(input, option) =>
                              option.props.children
                                  .toLowerCase()
                                  .indexOf(input.toLowerCase()) >= 0
                              }
                            >
                            {ciudad}
                            </Select>
                      }
                      </Form.Item>
                      <div className="fecha">
                        {/* label="Selecciona tu fecha de nacimiento" */}
                        <p>Selecciona tu fecha de nacimiento.</p>
                        <Form.Item >
                          {getFieldDecorator("fecha_de_nacimiento", {
                            rules: [
                              { required: true, message: "Especifica tu fecha de nacimiento!" }
                            ],
                            initialValue: data.getUsuario.fecha_de_nacimiento ? moment(data.getUsuario.fecha_de_nacimiento) : null
                          })(<DatePicker locale={locale} format={dateFormat} placeholder="Selecciona tu fecha de nacimiento" />
                          )}
                        </Form.Item>
                      </div>

                      <Form.Item label="Notificaciones:">
                        {getFieldDecorator("notificacion", {
                          initialValue: data.getUsuario.notificacion == 1 ? true : false,
                          valuePropName: 'checked'
                        })(
                          <Switch style={{backgroundColor: '#95ca3e'}} />
                        )}
                      </Form.Item>

                      <Form.Item label="Grado:">
                        <p>Experiencia en tu profesión.</p>
                        {getFieldDecorator("grado", { initialValue: data.getUsuario.grado ? data.getUsuario.grado : 0 })(
                          <Slider
                            marks={{
                              0: "0",
                              20: "1",
                              40: "2",
                              60: "3",
                              80: "4",
                              100: "5"
                            }}
                          />
                        )}
                      </Form.Item>

                      <Form.Item label="Estudios">
                        {getFieldDecorator("estudios", { initialValue: data.getUsuario.estudios })(
                          <Radio.Group>
                            <Radio.Button value="E S O">E S O</Radio.Button>
                            <Radio.Button value="Formación Profesional">Formación Profesional</Radio.Button>
                            <Radio.Button value="Bachiller">Bachiller</Radio.Button>
                            <Radio.Button value="Profesional">Profesional</Radio.Button>
                          </Radio.Group>
                        )}
                      </Form.Item>

                      <Form.Item label="Cargar" extra="Carga una foto de tu DNI">
                        {getFieldDecorator("fotos_tu_dni", {
                          // valuePropName: "fileList",
                          // getValueFromEvent: this.normFile
                          initialValue: data.getUsuario.fotos_tu_dni
                        })(
                          <Mutation mutation={UPLOAD_FILE}>
                            {(singleUpload) => (
                              // beforeUpload={async file => {
                              //   return getBase64(file).then(imgBlob => {
                              //     this.state.current_foto_tu_dni.push(imgBlob);
                              //     return true;
                              //   });
                              // }}
                              <Upload customRequest={async (data) => {
                                // let imgBlob = this.state.current_foto_tu_dni[this.state.current_foto_tu_dni.length - 1];
                                let imgBlob = await getBase64(data.file);
                                singleUpload({ variables: { imgBlob } }).then((res) => {
                                  this.props.form.setFieldsValue({
                                    fotos_tu_dni: [...getFieldValue('fotos_tu_dni'), res.data.singleUpload.filename],
                                  });

                                  // this.handleChange(res.data.singleUpload.filename)
                                }).catch(error => {
                                  console.log('fs error: ', error)
                                })
                              }} name="fotos_tu_dni" listType="picture" showUploadList={false}>
                                <Button> <Icon type="upload" /> Click para cargar </Button>
                              </Upload>
                            )}
                          </Mutation>
                        )}
                      </Form.Item>

                      <div style={{ margin: '0 0 24px 129px' }}>
                        {
                          getFieldValue('fotos_tu_dni').map(
                            (foto) => (
                              <img src={srvrImgDirUrl + foto} alt="avatar" key={foto} style={{ width: '100px', marginRight: '10px' }} />
                            ))
                        }
                      </div>

                      <Form.Item label="Cargar">
                        <div className="dropbox">
                          {getFieldDecorator("formularios_de_impuesto", {
                            initialValue: data.getUsuario.formularios_de_impuesto
                          })(
                            <Mutation mutation={UPLOAD_FILE}>
                              {(singleUpload) => (
                                <Upload.Dragger name="formularios_de_impuesto" customRequest={async (data) => {
                                  let imgBlob = await getBase64(data.file);
                                  singleUpload({ variables: { imgBlob } }).then((res) => {
                                    this.props.form.setFieldsValue({
                                      formularios_de_impuesto: [...getFieldValue('formularios_de_impuesto'), res.data.singleUpload.filename],
                                    });
                                  }).catch(error => {
                                    console.log('fs error: ', error)
                                  })
                                }} listType="picture-card" showUploadList={false}>
                                  <p className="ant-upload-drag-icon">
                                    <Icon type="inbox" />
                                  </p>
                                  <p className="ant-upload-text">
                                    Carga tu módelo 036 o 037 y tu resolución de alta de
                                    autónomo.
                              </p>
                                  <p className="ant-upload-hint">Archivo soportado JPG, PNG, PDF.</p>
                                </Upload.Dragger>
                              )}
                            </Mutation>
                          )}
                        </div>
                      </Form.Item>

                      <div style={{ margin: '0 0 24px 129px' }}>
                        {
                          getFieldValue('formularios_de_impuesto').map(
                            (foto) => (
                              <img src={srvrImgDirUrl + foto} alt="avatar" key={foto} style={{ width: '100px', marginRight: '10px' }} />
                            ))
                        }
                      </div>

                      <div style={{ marginBottom: 16 }}>
                        <Form.Item label="Facebook">
                          {getFieldDecorator("fb_enlazar", {
                            initialValue: data.getUsuario.fb_enlazar ? data.getUsuario.fb_enlazar : "https://facebook.com/"
                          })(
                            <Input />
                          )}
                        </Form.Item >
                      </div>

                      <div style={{ marginBottom: 16 }}>
                        <Form.Item label="Twitter" >
                          {getFieldDecorator("twitter_enlazar", {
                            initialValue: data.getUsuario.twitter_enlazar ? data.getUsuario.twitter_enlazar : "https://twitter.com/"
                          })(
                            <Input />
                          )}
                        </Form.Item>
                      </div>

                      <div style={{ marginBottom: 16 }}>
                        <Form.Item label="Instagram">
                          {getFieldDecorator("instagram_enlazar", {
                            initialValue: data.getUsuario.instagram_enlazar ? data.getUsuario.instagram_enlazar : "https://instagram.com/"
                          })(
                            <Input />
                          )}
                        </Form.Item>
                      </div>

                      <div style={{ marginBottom: 16 }}>
                        <Form.Item label="Linkedin">
                          {getFieldDecorator("youtube_enlazar", {
                            initialValue: data.getUsuario.youtube_enlazar ? data.getUsuario.youtube_enlazar : "https://linkedin.com/in/"
                          })(
                            <Input />
                          )}
                        </Form.Item>
                      </div>

                      <div style={{ marginBottom: 16 }}>
                        <Form.Item label="Sitio web">
                          {getFieldDecorator("propia_web_enlazar", {
                            initialValue: data.getUsuario.propia_web_enlazar ? data.getUsuario.propia_web_enlazar : " "
                          })(
                            <Input />
                          )}
                        </Form.Item>
                      </div>

                      <div className="boton">
                        <Button type="primary" htmlType="submit">
                          Guardar los cambios
                        </Button>
                      </div>
                      <Mutation mutation={ELIMINAR_USUARIO}>
                        {(eliminarUsuario) => {
                          return (
                            <Button onClick={e => this.handleEliminar(e, eliminarUsuario, id)} type="danger">
                              Eliminar cuenta
                        </Button>
                          )
                        }}
                      </Mutation>
                    </div>
                  </Form>
                </Row>
              }}
            </Query>
          )}
        </Mutation>
      </div>
    );
  }
}

const WrappedDemo = Form.create({ name: "validate_other" })(Formularioaccount);

export default WrappedDemo;


