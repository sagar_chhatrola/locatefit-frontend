import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import "./Account-Detail.css";
import Formularioaccount from "./Formularioaccount";
import { Icon } from "antd";
import Forgot from "./fotgotinterno"

class AccountDetail extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    password2: "",
    errors: {},
    active_index: 1
  };


  static getDerivedStateFromProps(nextProps) {
    if (nextProps.errors) {
      return {
        errors: nextProps.errors
      };
    }
    return null;
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };

    this.props.registerUser(newUser, this.props.history);
  };
  changeTab = index => {
    this.setState({ active_index: index });
  };

  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>DETALLES DE LA CUENTA</h3>
          <p>Aquí podrás ver y editar los datos de tu perfil</p>
        </div>

        <div className="cont">
          <span className="subtitle">
            <Icon type="user" />
            Área personal
          </span>
          <Formularioaccount />
        </div>

        <div className="cont">
          <span className="subtitle">
            <Icon type="lock" />
            Gestión de contraseña
          </span>

          <p>Escríbe tu correo electrónico para enviarte un enlace dondes podrás cambiar tu contraseña</p>
          <Forgot />
        </div>

      </div>
    );
  }
}

AccountDetail.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};


export default AccountDetail;
