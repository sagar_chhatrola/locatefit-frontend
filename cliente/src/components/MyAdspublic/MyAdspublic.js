import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { FaArrowRight } from "react-icons/fa";
import ProductList from "./../../containers/product/product-list";
import { connect } from "react-redux";

import "./MyAdspublic.css";

class MyAds extends Component {
  render() {
    return (
      <div className="card-content11">
        <div className="misprod">
          <div className="card-content1">
            <div className="card-title">
              <h3>Mis servicios</h3>
              <p>
                Aqui te muestro los servicios que soy capaz de realizar espero
                poder ayudarte.
              </p>
            </div>

            <ProductList />
            <ProductList />
            <ProductList />
            <ProductList />
            <ProductList />
            <ProductList />
          </div>

          <div className="page-number">
            <p>
              <a className="page-number-active" href="#page1">
                1
              </a>
              <a href="#page2">2</a>
              <a href="#page3">3</a> ... <a href="#page11">11</a>
              <a className="Botonsig" href="#Siguiente">
                Siguiente <FaArrowRight />
              </a>
            </p>
            <p className="page-number1">1 - 6 de 364 resultados</p>
          </div>
        </div>
      </div>
    );
  }
}

MyAds.propTypes = {};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  {}
)(withRouter(MyAds));
