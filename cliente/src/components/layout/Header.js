import React, { Component } from 'react';
import './Navbar.css';
import {
  Avatar,
  Badge,
  Icon,
  Popover,
  Empty,
  Button,
  message,
  Row,
  Col,
  List,
  Spin
} from 'antd';
import { Query, Mutation } from 'react-apollo';
import moment from 'moment';
import { USER_DETAIL, USUARIO_FAVORITO_PRODUCTS, GET_NOTIFICATIONS } from '../../queries';
import { ELIMINAR_USUARIO_FAVORITE_PRODUCTO, READ_NOTIFICATION } from '../../mutations';
import { LOCAL_API_URL } from './../../config';

const mainLogo = require('./../../assets/images/logo_main/logo_main.png');

const Header = ({
  session,
  onFavouriteDelete,
  favouriteAddedToWishListCounter
}) => {
  if (session && session.obtenerUsuario && session.obtenerUsuario.id) {
    localStorage.setItem('id', session.obtenerUsuario.id);
  }

  let barra = session.obtenerUsuario ? (
    <UsuarioAutenticado
      user={session}
      onFavouriteDelete={onFavouriteDelete}
      favouriteAddedToWishListCounter={favouriteAddedToWishListCounter}
    />
  ) : (
      <UsuarioNoAutenticado />
    );

  return <div>{barra}</div>;
};

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
      $('.navbar').addClass("shadow");
    } else {
      $(".navbar").removeClass("shadow");
    }
  });
});

const UsuarioNoAutenticado = () => {
  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-navbar'>
      <div className='dflex'>
        <a title='Locatefit' className='navbar-brand' href='/'>
          <img src={mainLogo} alt='' />
        </a>

        <div className='collapse1 navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav'>
            <li className='nav-item'>
              <a
                title='Iniciar sesión o Registrarte'
                className='rounded_button1'
                href='/become-profesional'
              >
                Conviértete en un profesional
              </a>
            </li>

            <li className='nav-item'>
              <a
                title='Iniciar sesión o Registrarte'
                className='rounded_button13'
                href='/login'
              >
                Iniciar sesión
              </a>
            </li>

            <li>
              <a title='Iniciar sesión' className='usermovil' href='/login'>
                <Icon type='user' />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

class UsuarioAutenticado extends Component {
  constructor(props) {
    super(props);
  }

  refetch = null;

  componentWillReceiveProps(props) {
    if (this.refetch && props.favouriteAddedToWishListCounter) this.refetch();
  }

  render() {
    let { user, onFavouriteDelete } = this.props;

    let nombre = user.obtenerUsuario.usuario;
    let id = localStorage.getItem('id');
    return (
      <nav className='navbar navbar-expand-lg navbar-light bg-navbar'>
        <div className='dflex'>
          <a title='Locatefit' className='navbar-brand' href='/'>
            <img src={mainLogo} alt='' />
          </a>

          <div
            className='collapse1 navbar-collapse'
            id='navbarSupportedContent'
          >
            <ul className='navbar-nav'>
              <li className='nav-item'>
                <Query query={GET_NOTIFICATIONS} variables={{ userId: localStorage.getItem('id') }}>
                  {({ loading, error, data, refetch }) => {
                    this.refetch = refetch;
                    if (loading) return <div className="page-loader">
                    <Spin size="large"></Spin>
                  </div>;
                    if (error)
                      return console.log('error getting user detail: ', error);

                    let notificaciones = [];
                    let contentnotiCount = 0;
                    if (
                      data &&
                      data.getNotifications.notifications

                    ) {
                      notificaciones =
                        data.getNotifications.notifications;
                      contentnotiCount =
                        data.getNotifications.notifications.length;
                    }
                    console.log(data)
                    return (
                      <Popover
                        className='filtroproductoSelect1'
                        content={contentnoti(
                          notificaciones,
                          refetch
                        )}
                        trigger='click'
                      >
                        <Badge count={contentnotiCount} overflowCount={9}>
                          <a
                            title='Notificaciones'
                            className='nav-link-icon'
                            href='#Notificaciones'
                          >
                            <Icon type='bell' />
                          </a>
                        </Badge>
                      </Popover>
                    );
                  }}
                </Query>
              </li>

              <li className='nav-item'>
                <Query query={USUARIO_FAVORITO_PRODUCTS}>
                  {({ loading, error, data, refetch }) => {
                    this.refetch = refetch;
                    if (loading) return <div className="page-loader">
                    <Spin size="large"></Spin>
                  </div>;
                    if (error)
                      return console.log('error getting user detail: ', error);

                    let usuarioFavoritoProductos = [];
                    let usuarioFavoritoProductosCount = 0;
                    if (
                      data &&
                      data.getUsuarioFavoritoProductos &&
                      data.getUsuarioFavoritoProductos.list
                    ) {
                      usuarioFavoritoProductos =
                        data.getUsuarioFavoritoProductos.list;
                      usuarioFavoritoProductosCount =
                        data.getUsuarioFavoritoProductos.list.length;
                    }
                    return (
                      <Popover
                        className='filtroproductoSelect1'
                        content={contentheart(
                          usuarioFavoritoProductos,
                          refetch,
                          onFavouriteDelete
                        )}
                        trigger='click'
                        placement='bottom'
                      >
                        <Badge
                          count={usuarioFavoritoProductosCount}
                          overflowCount={9}
                        >
                          <a
                            title='Favoritos'
                            className='nav-link-icon'
                            href='#fav'
                          >
                            <Icon type='heart' />
                          </a>
                        </Badge>
                      </Popover>
                    );
                  }}
                </Query>
              </li>
              <Query query={USER_DETAIL} variables={{ id }}>
                {({ loading, error, data }) => {
                  if (loading) return null;
                  if (error)
                    return console.log('error getting user detail: ', error);

                  return (
                    <li className='nav-item dropdown'>
                      <a
                        title='Mi perfil'
                        href='/profile'
                        className='userprofile'
                      >
                        <Avatar
                          size={30}
                          src={
                            LOCAL_API_URL + '/assets/images/' +
                            data.getUsuario.foto_del_perfil
                          }
                        ></Avatar>
                      </a>
                    </li>
                  );
                }}
              </Query>

              <Query query={USER_DETAIL} variables={{ id }}>
                {({ loading, error, data }) => {
                  if (loading) return null;
                  if (error)
                    return console.log('error getting user detail: ', error);

                  return (
                    <li className='nav-item dropdown'>
                      <a
                        title='Mi perfil'
                        href='/profile'
                        className='usernombre'
                      >
                        {data.getUsuario.nombre}
                      </a>
                    </li>
                  );
                }}
              </Query>

              <li className='publicar'>
                <a href='#Publicar'></a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

const contentheart = (usuarioFavoritoProductos, refetch, onFavouriteDelete) => (
  <div className='contenedor'>
    <h4>Mi Lista de deseos</h4>
    {usuarioFavoritoProductos.length > 0 ? (
      usuarioFavoritoProductos.map((listItem, i) => (
        <div key={i}>
          <div className='cardwishlist'>
            <Row>
              <Col span={9}>
                <img
                  className='imgeWish'
                  src={LOCAL_API_URL +
                    '/assets/images/' +
                    listItem.producto.fileList[0]
                  }
                  alt=''
                />
              </Col>

              <Col span={14}>
                <h6>{listItem.producto.title}</h6>
                <p> Ofrecido por: {listItem.producto.creator.nombre}</p>
              </Col>
            </Row>

            <div className='extras'>
              <Row>
                <Col span={8}>
                  <Icon
                    className='iconcasa'
                    type='environment'
                    theme='twoTone'
                    twoToneColor='#95ca3e'
                  />
                  <p className='ciiity'>{listItem.producto.city}</p>
                </Col>

                <Col span={8}>
                  <Icon type='star' theme='twoTone' twoToneColor='#FFCE4E' />
                  <p className='ciiity'>({listItem.producto.professionalRating.length})</p>
                </Col>

                <Col span={8}>
                  <p className='preciodato'>
                    {' '}
                    {listItem.producto.number + '€'}
                  </p>
                  <p className='ciiity'>{listItem.producto.currency}</p>
                </Col>
              </Row>
            </div>
            <div className='extras'>
              <Row>
                <Col span={16}>
                  <Button
                    className='bubb'
                    href={'/deals/' + listItem.producto.id}
                    block
                  >
                    Contratar profesional
                  </Button>
                </Col>
                <Col span={8}>
                  <Mutation mutation={ELIMINAR_USUARIO_FAVORITE_PRODUCTO}>
                    {(
                      eliminarUsuarioFavoritoProducto,
                      { loading, error, data }
                    ) => {
                      return (
                        <Icon
                          type='delete'
                          theme='twoTone'
                          twoToneColor='red'
                          style={{ cursor: 'pointer' }}
                          onClick={e =>
                            handleEliminar(
                              e,
                              eliminarUsuarioFavoritoProducto,
                              listItem.id,
                              refetch,
                              onFavouriteDelete
                            )
                          }
                        />
                      );
                    }}
                  </Mutation>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      ))
    ) : (
        <div>
          <Empty
            image='https://user-images.githubusercontent.com/507615/54591670-ac0a0180-4a65-11e9-846c-e55ffce0fe7b.png'
            imageStyle={{
              height: 60
            }}
            description={<span>No tiene servicio en la lista de deseos.</span>}
          ></Empty>
        </div>
      )}
  </div>
);

const contentnoti = (notificaciones, refetch) => (
  <div className='notifications'>
    <h4>Notificaciones</h4>
    {notificaciones.length > 0 ?
      <List className="ListNot"
        itemLayout="horizontal"
        dataSource={notificaciones}
        renderItem={item => {
          let title = ''
          let description = ''
          let avatar = ''
          switch (item.type) {
            case 'new_order':
              title = 'Nueva orden'
              description = `Enhorabuena ha recibido una nueva orden de ${item.cliente.nombre} ${item.cliente.apellidos}.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.cliente.foto_del_perfil
              break;
            case 'resolution_order':
              title = 'Orden en resolución'
              description = `${item.cliente.nombre} ${item.cliente.apellidos} ha reportado un problema con la orden estamos trabajando para solucionarlo.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.cliente.foto_del_perfil
              break;
            case 'valored_order':
              title = 'Orden valorada'
              description = `Enhorabuena ${item.cliente.nombre} ${item.cliente.apellidos} ha valorado tu trabajo.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.cliente.foto_del_perfil
              break;
            case 'accept_order':
              title = 'Orden Aceptada'
              description = `Genial ${item.profesional.nombre} ${item.profesional.apellidos} ha aceptado la orden.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.profesional.foto_del_perfil
              break;
            case 'reject_order':
              title = 'Orden rechazada'
              description = `Opps ${item.profesional.nombre} ${item.profesional.apellidos} ha rechazado la orden.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.profesional.foto_del_perfil
              break;
            case 'finish_order':
              title = 'Orden finalizada'
              description = `Genial ${item.profesional.nombre} ${item.profesional.apellidos} ha finalizado la orden es hora de valorarlo.`
              avatar = LOCAL_API_URL + '/assets/images/' +
                item.profesional.foto_del_perfil
          }
          return (
            <Mutation mutation={READ_NOTIFICATION} variables={{notificationId: item._id}}>
              {(readNotification) => {
                return (<List.Item>
                  <Badge count={!item.read} dot>
                    <List.Item.Meta
                      style={{cursor: 'pointer'}}
                      onClick={() => {
                        readNotification({ variables: { notificationId: item._id } }).then(
                          async ({ data: res }) => {
                            if (
                              res &&
                              res.readNotification &&
                              res.readNotification.success
                            ) {
                              message.success(res.readNotification.message);
                              refetch()
                              console.log(res.readNotification.message);
                            } else if (
                              res &&
                              res.readNotification &&
                              !res.readNotification.success
                            )
                              message.error(res.readNotification.message);
                              console.log(res.readNotification.message)
                          }
                        );
                      }
                      }
                      avatar={<Avatar src={avatar} />}
                      title={title}
                      description={description}
                    />
                    <div style={{ marginTop: 5, marginLeft: 47, flexDirection: 'column', cursor: 'pointer'}}>
                    <Icon style={{color: '#ddd', marginRight: 5}} type="clock-circle" /><span style={{ color: '#ddd' }}>{moment(Number(item.createdAt)).fromNow()}</span>
                      <Icon style={{color: '#ddd', marginLeft: 10, }} type="eye" /> <span style={{ color: '#ddd', marginLeft: 5 }}>Marcar como visto</span>
                    </div>
                  </Badge>
                </List.Item>
                );
              }}
            </Mutation>)
        }}
      /> : <Empty
        image='https://user-images.githubusercontent.com/507615/54591670-ac0a0180-4a65-11e9-846c-e55ffce0fe7b.png'
        imageStyle={{
          height: 60
        }}
        description={
          <span>
            <h3>
              <Icon type='check-circle' theme='twoTone' />
            </h3>
            Estas al día, no tiene notificaciones.
           </span>
        }
      ></Empty>
    }


  </div>
);

  




const handleEliminar = (
  e,
  eliminarUsuarioFavoritoProducto,
  id,
  refetch,
  onFavouriteDelete
) => {
  e.preventDefault();
  eliminarUsuarioFavoritoProducto({ variables: { id } }).then(
    async ({ data }) => {
      if (
        data &&
        data.eliminarUsuarioFavoritoProducto &&
        data.eliminarUsuarioFavoritoProducto.success
      ) {
        message.success(data.eliminarUsuarioFavoritoProducto.message);
        refetch();
        onFavouriteDelete();
      } else if (
        data &&
        data.eliminarUsuarioFavoritoProducto &&
        !data.eliminarUsuarioFavoritoProducto.success
      )
        message.error(data.eliminarUsuarioFavoritoProducto.message);
    }
  );
};

export default Header;
