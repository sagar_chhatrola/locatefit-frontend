import React, { Component } from "react";
import { FaTwitter, FaInstagram, FaFacebookSquare, FaLinkedinIn} from "react-icons/fa";
import "./Footer.css";
import Mailchimp from './react-mailchimp-form'
import playStoreImg from "./../../assets/images/play_store/play_store.png";
import appStoreImg from "./../../assets/images/apple_store/apple_store.png";
const mainLogo = require("./../../assets/images/logo_main/logo_main.png");
const country_flag = require("./../../assets/images/country_flag/country_flag.png");
const country_flag1 = require("./../../assets/images/country_flag/France.svg");
const country_flag2 = require("./../../assets/images/country_flag/ingles.png");
const country_flag3 = require("./../../assets/images/country_flag/Italy.svg");
const country_flag4 = require("./../../assets/images/country_flag/Portugal.svg");



class Footer extends Component {
  render() {
    return (
      <div>
        <section className="subscribeBgColor">
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <div>
                  <label className="subscribeText">
                    Entérate de todas las novedades y ofertas de profesionales
                  </label>
                  <p>Al suscribirte a nuestra Newlester te enterarás primero de las ofertas que tenemos y ademas regalamos miles de euros en cupones que podrás cajear al contratar profesionales en nuestar plataforma.</p>
                </div>
              </div>
              <div className="col-md-4 colmd">
                <div>

                  <Mailchimp
                    action='https://locatefit.us5.list-manage.com/subscribe/post?u=811d1aaf63534da5676ba23e7&amp;id=baa2df3e18'

                    //Adding multiple fields:
                    fields={[
                      {
                        name: 'EMAIL',
                        placeholder: 'Escribe tu Email',
                        type: 'email',
                        required: true,
                        className: 'subscribeInput'
                      },
                      {
                        name: 'FNAME',
                        placeholder: 'Nombre',
                        type: 'text',
                        required: true,
                        className: 'subscribeInput'
                      }
                    ]}
                    // Change predetermined language
                    messages={
                      {
                        sending: "Enviando...",
                        success: "¡Gracias por suscribirse!",
                        error: "Se ha producido un error interno inesperado.",
                        empty: "Debes escribir un correo electrónico.",
                        duplicate: "Demasiados intentos de suscripción para esta dirección de correo electrónico",
                        button: "Suscribirme!"
                      }
                    }
                    // Add a personalized class
                    className=''
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="bottomFooter">
          <div className="container">
            <div className="row">
              <div className="col-md-3">
                <div className="mainLogo">
                  <img src={mainLogo} width="130px" alt="" />
                </div>

                <label className="logoTitle">
                Los mejores profesionales, <br />tu eliges la fecha, hora y lugar.
                </label>

                <div className="redesfooter">
                  <h3>
                    <a className="redesfooter1" target="_blank" href="https://www.facebook.com/locatefit/" >
                      <FaFacebookSquare />
                    </a>
                    <a className="redesfooter2" target="_blank" href="https://www.twitter.com/locatefites/">
                      <FaTwitter />
                    </a>
                    <a className="redesfooter3" target="_blank" href="https://www.instagram.com/locatefit/">
                      <FaInstagram />
                    </a>
                    <a className="redesfooter4" target="_blank" href="https://es.linkedin.com/company/locatefit?trk=public_profile_topcard_current_company">
                      <FaLinkedinIn />
                    </a>
                  </h3>
                </div>
              </div>
              <div className="col-md-2">
                <label className="footerLabel">
                  Trabaja con
                  <br /> Nosotros
                </label>
                <ul className="footerLink">
                  <li>
                    <a
                      href="https://about.locatefit.es/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      ¿Quiénes somos?
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://about.locatefit.es/empleo">Empleos</a>
                  </li>
                  <li>
                    <a target="_blank" href="https://about.locatefit.es/profesionales">Profesionales</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2">
                <label className="footerLabel">
                  Atención al
                  <br /> Cliente
                </label>
                <ul className="footerLink">
                  <li>
                    <a className="ayuda" target="_blank" href="https://about.locatefit.es/preguntas-frecuentes">
                      Preguntas frecuentes
                    </a>
                  </li>
                  <li>
                    <a className="ayuda" target="_blank" href="https://about.locatefit.es/contacto">
                      Contacto
                    </a>
                  </li>
                  <li>
                    <a
                      href="httpS://blog.locatefit.es/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Blog
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-3">
                <label className="footerLabel">
                  Términos
                  <br />
                  Legales
                </label>
                <ul className="footerLink">
                  <li>
                    <a target="_blank" href="https://about.locatefit.es/condiciones-de-uso">Condiciones de uso</a>
                  </li>
                  <li>
                    <a target="_blank" href="https://about.locatefit.es/privacidad">Políticas de privacidad</a>
                  </li>
                  <li>
                    <a target="_blank" href="https://about.locatefit.es/cookies">Cookies</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2">
                <label className="footerLabel"></label>
                <div className="dropdown">
                  <button
                    className="btn footerDropdown dropdown-toggle"
                    type="button"
                    id="dropdownMenu2"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      className="flag"
                      src={country_flag}
                      height={15}
                      alt=""
                    />
                    Español
                  </button>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenu2"
                  >
                    <button className="dropdown-item" type="button">
                      <img
                        className="flag"
                        src={country_flag2}
                        height={15}
                        alt=""
                      />
                      Inglés
                    </button>

                    <button className="dropdown-item" type="button">
                      <img
                        className="flag"
                        src={country_flag1}
                        height={15}
                        alt=""
                      />
                      Francés
                    </button>
                    <button className="dropdown-item" type="button">
                      <img
                        className="flag"
                        src={country_flag4}
                        height={15}
                        alt=""
                      />
                      Portugés
                    </button>
                    <button className="dropdown-item" type="button">
                      <img
                        className="flag"
                        src={country_flag3}
                        height={15}
                        alt=""
                      />
                      Italiano
                    </button>
                  </div>
                </div>
                <div className="appss">
                  <a title="Apple Store" target="_blank" href="https://apps.apple.com/us/app/locatefit/id1499263781?mt=8&ign-mpt=uo%3D4">
                    <img className="app-store1" alt="Apple Store" height={50} src={appStoreImg} />
                  </a>
                  <a title="Play Store" target="_blank" href="https://play.google.com/store/apps/details?id=es.locatefit">
                    <img alt="Play Store" height={40} src={playStoreImg} />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <p className="logoTitle1">
            Copyright © 2019 Locatefit S. L. © de sus respectivos propietarios.
          </p>
        </section>
      </div>
    );
  }
}

export default Footer;
