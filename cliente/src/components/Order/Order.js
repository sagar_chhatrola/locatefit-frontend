import React, { Component } from "react";
import "./Order.css";
import TablaPedidos from "./TablaPedidos";
import { Icon, DatePicker, Button } from "antd";
import moment from 'moment';

const { RangePicker } = DatePicker;

const dateFormat = 'MM/DD/YYYY'; // compatible con la fecha y hora del servidor para evitar conflictos de API
const fromDate = '11/07/2019';
const toDate = '12/07/2019';


class Order extends Component {

  state = {
    dateRange: null
  };

  fromDate = fromDate;
  toDate = toDate;

  onDateRangeChange = (dates, dateStrings) => {
    this.fromDate = dateStrings[0];
    this.toDate = dateStrings[1];
  }

  aplicarDateRange = e => {
    e.preventDefault();
    this.setState({ dateRange: { fromDate: this.fromDate, toDate: this.toDate } });
  }
  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>PEDIDOS</h3>
          <p>
            Aquí podrás gestionar tus pedidos, ver su estado y todo sus detalles
          </p>
        </div>
        <div className="cont">
          <span className="subtitle">
            <Icon type="table" /> Listado de pedidos
          </span>
          <div className="subtitle1111">
            Filtral por fecha <br />
            <RangePicker
              style={{paddingBottom: 20, paddingTop: 10}}
              defaultValue={[moment(fromDate, dateFormat), moment(toDate, dateFormat)]}
              format={dateFormat}
              onChange={this.onDateRangeChange}
            /><Button onClick={this.aplicarDateRange} style={{marginBottom: 20, marginTop: 10, marginLeft: 5}} type='dashed'>Aplicar</Button>
          </div>
          <TablaPedidos dateRange={this.state.dateRange} />
        </div>
      </div>
    );
  }
}

Order.propTypes = {};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default Order;
