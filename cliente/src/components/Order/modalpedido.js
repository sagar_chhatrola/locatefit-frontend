import React, { Component } from 'react'
import { Progress, Switch, Card, Icon, Avatar, Button, Rate } from 'antd';
import { LOCAL_API_URL } from './../../config';

const { Meta } = Card;

class Modalpedido extends Component {

  constructor(props) {
    super(props);
  }

  averageRating = 0;

  calculateAverageRating(professionalRating) {
    let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };

    professionalRating.forEach(item => {
      if (item.rate == 1) rating['1'] += 1;
      else if (item.rate == 2) rating['2'] += 1;
      else if (item.rate == 3) rating['3'] += 1;
      else if (item.rate == 4) rating['4'] += 1;
      else if (item.rate == 5) rating['5'] += 1;
    });

    const averageRating = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / professionalRating.length;
    this.averageRating = averageRating.toFixed(1);

  }


  render() {
    let link = null;
    if (this.props.data) {
      const description = this.props.data.nota;
      const titulo = this.props.data.product.title;
      const date = new Date(this.props.data.endDate + ' ' + this.props.data.time);
      const year = date.getFullYear();
      // const year = date.getUTCFullYear();
      let month = date.getMonth() + 1;
      // let month = date.getUTCMonth() + 1;
      month = (month < 10) ? '0' + month : month;
      const day = date.getDate();
      // const day = date.getUTCDate();
      let hours = date.getHours();
      // let hours = date.getUTCHours();
      hours = (hours < 10) ? '0' + hours : hours;
      const minutes = date.getMinutes();
      // const minutes = date.getUTCMinutes();

      const startDateString = year + '' + month + '0' + day + 'T' + hours + '' + minutes + '000';

      const productoTime = this.props.data.product.time.split(':');
      let endHours = Number(hours) + (Number(productoTime[0]) * Number(this.props.data.cantidad));
      let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(this.props.data.cantidad));
      if (endMinutes > 60) {
        endHours++;
        endMinutes -= 60;
      }

      endHours = (endHours < 10) ? '0' + endHours : endHours;
      endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
      const endDateString = year + '' + month + '0' + day + 'T' + endHours + '' + endMinutes + '00';

      link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString + "/" + endDateString + "&details=" + description + "&sprop=name:Locatefit&sf=true";


      if (this.props.data.professionalRating.length > 0)
        this.calculateAverageRating(this.props.data.professionalRating)
      else this.averageRating = 0;

      console.log('averageRating: ', this.averageRating);
    }
    return (
      <div style={{padding: 20}}>
        <h6>Progreso del pedido</h6>
        <Progress strokeColor={{ from: '#108ee9', to: '#87d068', }}
          percent={this.props.data ? this.props.data.progreso : ''}
          status={this.props.data ? this.props.data.status : ''}
        />
        <Card style={{ width: '100%', marginTop: 16, marginBottom: 16 }}>
          <Meta
            title={
              <div>
                <h6>
                  Fecha del realizacion del servicio:
                </h6>
                <p>
                  {this.props.data ? this.props.data.endDate : ''} a las {this.props.data ? this.props.data.time : ''}
                </p>
              </div>
            }
          />

          <Button
            type="secondary"
            className="btnflex"
            shape="round"
            icon="calendar"
          >
            {this.props.data ?
              <a style={{ textDecoration: 'none', marginLeft: 10 }} target="_blank" href={link}> Añadir a Google Calendar</a>
              : 'Añadir a Google Calendar'
            }

          </Button>
        </Card>

        <h6>Informacón del Profesional</h6>
        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={
              <Avatar src={this.props.data ? LOCAL_API_URL + "/assets/images/" + this.props.data.prfsnl.foto_del_perfil : ''} />
            }
            title={<div><h6>{this.props.data ? this.props.data.prfsnl.nombre : ''}</h6> <h6>{this.props.data ? this.props.data.prfsnl.apellidos : ''}</h6></div>}

            description={
              <div>
                <p>Profesión: {this.props.data ? this.props.data.prfsnl.profesion : ''}</p>
                <p>Número móvil: {this.props.data ? this.props.data.prfsnl.telefono : ''}</p>
                <p>Ciudad: {this.props.data ? this.props.data.prfsnl.ciudad : ''}</p>
                <Rate disabled value={this.averageRating} />({this.averageRating})  <p>({this.props.data ? this.props.data.professionalRating.length : ''}) Opiniones</p>
              </div>
            }
          />
        </Card>
        <h6 style={{ marginTop: 16 }}>Informacón del servicio</h6>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={this.props.data && this.props.data.product.fileList.length > 0 ?
              <Avatar src={LOCAL_API_URL + "/assets/images/" + this.props.data.product.fileList[0]} /> :
              <Avatar src=" " />
            }
            title={this.props.data ? this.props.data.product.title : ''}
            description={this.props.data ? "Cantidad: " + this.props.data.cantidad + " " + this.props.data.product.currency : ''}
          />
        </Card>
      </div>
    )
  }
}

export default Modalpedido;
