import React, { Component } from "react";
import { Table, Button, Modal, DatePicker, Form, message, Rate, Card, Avatar, notification} from "antd";
import { Query, Mutation } from 'react-apollo';
import { useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { withApollo } from 'react-apollo';
import { CLIENTE_PEDIDO_QUERY } from "../../queries";
import { PROFESSIONAL_ORDEN_PROCEED, CREATE_NOTIFICATION } from "../../mutations";
import "./Order.css"
import TextArea from "antd/lib/input/TextArea";
import Modalpedidos from './modalpedido';
import { LOCAL_API_URL } from './../../config';

const openNotification = placement => {
  notification.error({
    message: 'Has recibido una nueva orden',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    placement,
  });
};

const openNotificationValored = placement => {
  notification.error({
    message: 'Orden Valorada',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    placement,
  });
};


const { Meta } = Card;

const { MonthPicker, RangePicker } = DatePicker;
const { confirm } = Modal;

function range(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}


function disabledDate(current) {
  // Can not select days before today and today
  return current && current < moment().startOf('day');
}

function disabledDateTime() {
  return {
    disabledHours: () => range(0, 24).splice(24, 24),
    disabledMinutes: () => range(60, 60),
  };
}


// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  getCheckboxProps: record => ({
    disabled: record.name === "Disabled User", // Column configuration not to be checked
    name: record.name
  })
};

class TablaPedidos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      visible1: false,
      visible2: false,
      dataLoading: true,
      formLoading: false,
      currentPedidos: null,
      ordenes: [],
      values: '',
      rate: 0,
      coment: '',
      messageTexte: 'Enhora buena el cliente a valorado tu trabajo.',
      messageTexteresolucion: 'El cliente a abierto una reclamación del servicio pronto tendrás noticias de la resolución',
      useridpush: ''
    };
  }

  refetch = null;
  clienteId = localStorage.getItem('id');
  
  componentDidUpdate({ dateRange }) {
    if (dateRange && this.refetch) {
      this.setState({ dataLoading: true });
      this.refetch({ cliente: this.clienteId, dateRange }).then(res => {
        console.log('in other: ', res)
        if (res && res.data && res.data.getPedidosByCliente && res.data.getPedidosByCliente.success) {
          this.setState({ ordenes: res.data.getPedidosByCliente.list, dataLoading: false });
        }
      });
    }
  }

  componentDidMount(){
    this.props.client
    .query({
            query: CLIENTE_PEDIDO_QUERY,
            variables: { cliente: this.clienteId }
            })
        .then(async (results)=> {
            const { loading, error, data } = results
            if (data) {
              const dataId = data && data.getPedidosByCliente ? data.getPedidosByCliente.list[0] : ''
              console.log('mi user id', dataId.prfsnl.UserID)
                this.setState({
                    useridpush: dataId.prfsnl.UserID
                })
            }
            
        })
  }


  SendPushNotificationvalorar = () => {
    fetch(`${LOCAL_API_URL}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexte}`)
      .catch(err => console.log(err))

      console.log(this.state.useridpush, this.state.messageTexte)
  };

  SendPushNotificationresolucion = () => {
    fetch(`${LOCAL_API_URL}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteresolucion}`)
      .catch(err => console.log(err))
      console.log(this.state.useridpush, this.state.messageTexteresolucion)
  };


  handleChange = rate => {
    this.setState({ rate });
    console.log(rate)
  };

  showAcceptModal = (e, data) => {
    e.preventDefault();
    this.setState({
      currentPedidos: data,
      visible: true,
    });
  };

  
  handleCancel = () => {
    this.setState({
      currentPedidos: null,
      visible: false,
    });
  };

  hideDetailModal1 = () => {
    this.setState({
      currentPedidos: null,
      visible: false,
    });
  };

  showDetailModal = (e, data) => {
    e.preventDefault();
    this.setState({
      currentPedidos: data,
      visible1: true,
    });
  };

  hideDetailModal = () => {
    this.setState({
      currentPedidos: null,
      visible1: false,
    });
  };

 

  showDetailModal11 = (e, data) => {
    e.preventDefault();
    this.setState({
      currentPedidos: data,
      visible2: true,
    });
  };

  hideDetailModal11 = () => {
    this.setState({
      currentPedidos: null,
      visible2: false,
    });
  };

  formatResult = data => {
    if (data && data.getPedidosByCliente && data.getPedidosByCliente.success) {
      this.setState({
        dataLoading: false,
        ordenes: data.getPedidosByCliente.list
      });
    }
  }



  render() {

    const desc = ["Terrible", "MAlo", "Normal", "Buena", "Maravillosa"];
    const { rate, coment} = this.state;
    const columns = [
      {
        title: "Nº Pedido",
        key: "id",
        render: data => <p style={{color: '#3b83bd'}} onClick={e => this.showDetailModal(e, data)}>{data.id}</p>
      },
      {
        title: "Fecha",
        dataIndex: "created_at",
        render: created_at => new Date(Number(created_at)).toLocaleDateString()
      },
      {
        title: "Estado",
        dataIndex: "estado"
      },
      {
        title: "Total",
        key: "total",
        render: dataIndex => dataIndex.cantidad * dataIndex.product.number + '€'
      },
      {
        title: "Acción",
        key: "action",

        render: dataIndex => {
          return (


            <span className="btnflex">
              {dataIndex && dataIndex.estado === 'Pendiente de pago' ?
                <Button
                  type="secondary"
                  className="btnflex"
                  shape="round"
                  icon="like"
                  href={'/payment/' + dataIndex.id}
                >
                  Completar el pago
                    </Button>
                : ''}

              {dataIndex && dataIndex.estado === 'Nueva' ?
                <Button
                  href="mailto:locatefit.es@gmail.com"
                  type="primary"
                  className="btnflex"
                  shape="round"
                  icon="sync"
                  style={{ marginRight: 10 }}
                >
                  Solicitar devolución
                  </Button>

                : ''}

              {dataIndex && dataIndex.estado === 'Finalizada' ?
                <Button
                  onClick={e => this.showAcceptModal(e, dataIndex)}
                  type="secondary"
                  className="btnflex"
                  shape="round"
                  icon="star"
                  style={{ marginRight: 10 }}
                >
                  Valorar profeisonal
                  </Button>
                : ''}

                {dataIndex && dataIndex.estado === 'Finalizada' ?
                <Button
                  onClick={e => this.showDetailModal11(e, dataIndex)}
                  type="danger"
                  className="btnflex"
                  shape="round"
                  icon="close"
                  style={{ marginRight: 10 }}
                >
                  Reportar un problema
                  </Button>
                : ''}

            </span>
          )
        }
      }
    ];

    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Query query={CLIENTE_PEDIDO_QUERY} variables={{ cliente: this.clienteId }} onCompleted={this.formatResult}>
          {({ refetch }) => {
            this.refetch = refetch;
            return (
              <div className="">
                <Table
                  rowKey={orden => orden.id}
                  rowSelection={rowSelection}
                  columns={columns}
                  loading={this.state.dataLoading}
                  dataSource={this.state.ordenes}
                />
              </div>
            )
          }}
        </Query>
        <Modal
          style={{ padding: 20 }}
          footer={null}
          title="Detalles del pedido"
          visible={this.state.visible1}
          onCancel={this.hideDetailModal}
        >
          <Modalpedidos data={this.state.currentPedidos} />
        </Modal>

        <Modal
          style={{ padding: 20 }}
          footer={null}
          title="Valorar profeisonal"
          visible={this.state.visible}
          onCancel={this.hideDetailModal1}
        >
          <div>
            <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
              {(ordenProceed) => (
                <div className="nooota">
                  <Form onSubmit={e => {
                    e.preventDefault();
                    this.props.form.validateFields((err, values) => {
                      console.log('values: ', values);
                      if (!err) {
                        this.setState({ formLoading: true });
                        const formData = {
                          ordenId: this.state.currentPedidos.id,
                          estado: 'Valorada',
                          progreso: '100',
                          status: 'success',
                          coment: values.coment,
                          rate: this.state.rate
                        }
                        ordenProceed({ variables: formData }).then(res => {
                          if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                            this.setState({ visible: false, formLoading: false, dataLoading: true });
                            message.success('Has valorado correctamente este profesional');
                            if (this.refetch) {
                              this.refetch().then(res => {
                                if (res && res.data && res.data.getPedidosByCliente && res.data.getPedidosByCliente.success) {
                                  this.setState({ ordenes: res.data.getPedidosByCliente.list, dataLoading: false });
                                  this.setState({
                                    rate: ''
                                  });
                                  const { currentPedidos } = this.state
                                  this.props.client.mutate({
                                    mutation: CREATE_NOTIFICATION,
                                    variables: {input:{ orden: currentPedidos.id, user: currentPedidos.prfsnl.id,cliente:localStorage.getItem('id') ,profesional:currentPedidos.prfsnl.id,type:'valored_order' }}
                                  })
                                  .then(async (results)=> {
                                    console.log("results",results)
                                  }).catch(err=>{
                                    console.log("err",err)
                                  })
                                  this.SendPushNotificationvalorar()
                                }
                              });
                            }
                          }
                        }).catch(err => {
                          this.setState({ visible: false, formLoading: false });
                          message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
                        })
                      }
                    });
                  }}>

                    <p>Valora el servios brindado por el profeisonal para ayudar a otros clientes como tu.</p>

                    <Card style={{ width: '100%', marginTop: 16 }}>
                      <Meta
                        avatar={
                          <Avatar src={ this.state.currentPedidos ? LOCAL_API_URL + "/assets/images/" + this.state.currentPedidos.prfsnl.foto_del_perfil : '' } />
                        }
                      title={<h6 style={{marginTop: 7}}>{this.state.currentPedidos ? this.state.currentPedidos.prfsnl.nombre : '' } {this.state.currentPedidos ? this.state.currentPedidos.prfsnl.apellidos : ''}</h6>}
                      />
                    </Card>

                    <div style={{ marginTop: 30, marginBottom: 30 }}>
                      <p style={{ marginBottom: 5 }}>Dinos cual fue tu experiencia con el servicio</p>
                      <Rate
                        allowHalf
                        tooltips={desc}
                        onChange={this.handleChange}
                        value={rate}
                      />
                      {rate ? (
                        <span className="ant-rate-text">{desc[rate - 1]}</span>
                      ) : (
                          ""
                        )}
                    </div>

                    <Form.Item>
                      {getFieldDecorator("coment", {
                        rules: [
                          {
                            required: true,
                            message: "Deja un comentarío al profeisonal"
                          }
                        ],
                      })(<TextArea  placeholder="Comentarío" />)}
                    </Form.Item>

                    <Button
                      htmlType="submit"
                      loading={this.state.formLoading}
                      type="secondary"
                      className="btnflex"
                      shape="round"
                      icon="star"
                      style={{ marginRight: 10 }}
                    >Valorar profesional</Button>

                  </Form>
                </div>
              )}
            </Mutation>
          </div>
        </Modal>

        <Modal
          style={{ padding: 20 }}
          footer={null}
          title="Reportar un problema con el pedido"
          visible={this.state.visible2}
          onCancel={this.hideDetailModal11}
        >
          <div>
            <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
              {(ordenProceed) => (
                <div className="nooota">
                  <Form onSubmit={e => {
                    e.preventDefault();
                    this.props.form.validateFields((err, values, descripcionproblem) => {
                      if (!err) {
                        this.setState({ formLoading: true });
                        const formData = {
                          ordenId: this.state.currentPedidos.id,
                          estado: 'Resolución',
                          progreso: '0',
                          status: 'exception',
                          descripcionproblem: values.descripcionproblem,
                        }
                        ordenProceed({ variables: formData }).then(res => {
                          if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                            this.setState({ visible2: false, formLoading: false, dataLoading: true });
                            message.success('Te resolición de problema ha sido enviado con éxito');
                            if (this.refetch) {
                              this.refetch().then(res => {
                                if (res && res.data && res.data.getPedidosByCliente && res.data.getPedidosByCliente.success) {
                                  this.setState({ ordenes: res.data.getPedidosByCliente.list, dataLoading: false })
                                  const { currentPedidos } = this.state
                                    this.props.client.mutate({
                                      mutation: CREATE_NOTIFICATION,
                                      variables: {input:{ orden: currentPedidos.id, user: currentPedidos.prfsnl.id,cliente:localStorage.getItem('id') ,profesional:currentPedidos.prfsnl.id,type:'resolution_order' }}
                                    })
                                    .then(async (results)=> {
                                      console.log("results",results)
                                    }).catch(err=>{
                                      console.log("err",err)
                                    })
                                    this.SendPushNotificationresolucion()
                                }
                              });
                            }
                          }
                        }).catch(err => {
                          this.setState({ visible2: false, formLoading: false });
                          message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
                        })
                      }
                    });
                  }}>
                    <p>Lamentamos que hayas tenido un problema con tu pedido describenos que paso y lo resolveremos lo antes posible.</p>

                    <Card style={{ width: '100%', marginTop: 16, marginBottom: 40 }}>
                      <Meta
                        avatar={
                          <Avatar src={this.state.currentPedidos ? LOCAL_API_URL + "/assets/images/" + this.state.currentPedidos.prfsnl.foto_del_perfil : ''} />
                        }
                        title={<h6 style={{marginTop: 7}}>{this.state.currentPedidos ? this.state.currentPedidos.prfsnl.nombre : ''} {this.state.currentPedidos ? this.state.currentPedidos.prfsnl.apellidos : ''}</h6>}
                      />
                    </Card>

                    <Form.Item style={{marginBottom: 30}}>
                      {getFieldDecorator("descripcionproblem", {
                        rules: [
                          {
                            required: true,
                            message: "Describe el problema con tu pedido"
                          }
                        ],
                      })(<TextArea placeholder="Describe el problema con tu pedido" />)}
                    </Form.Item>
                    <Button
                      htmlType="submit"
                      loading={this.state.formLoading}
                      type="danger"
                      className="btnflex"
                      shape="round"
                      icon="close"
                      style={{ marginRight: 10 }}
                    >
                      Reportar un problema
                  </Button>

                  </Form>
                </div>
              )}
            </Mutation>
          </div>
        </Modal>
      </div>
    );
  }
}

const WrappedTablaPedidos = Form.create({ name: "TablaPedidosForm" })(TablaPedidos);

export default withApollo(WrappedTablaPedidos);
