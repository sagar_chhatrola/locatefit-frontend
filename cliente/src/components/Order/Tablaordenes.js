import React, { Component } from "react";
import { Table, Button, Modal, DatePicker, Form, message, Popover, notification } from "antd";
import { Query, Mutation } from 'react-apollo';
import { withApollo } from 'react-apollo';
import { useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { PROFESSIONAL_ORDENES_QUERY, USER_DETAIL } from "../../queries";
import { PROFESSIONAL_ORDEN_PROCEED, CREATE_NOTIFICATION } from "../../mutations";
import "./Order.css"
import TextArea from "antd/lib/input/TextArea";
import Modalordenes from './modalordenes';
import { LOCAL_API_URL } from '../../config';
const { MonthPicker, RangePicker } = DatePicker;
const { confirm } = Modal;

function range(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}


function disabledDate(current) {
  // Can not select days before today and today
  return current && current < moment().startOf('day');
}

function disabledDateTime() {
  return {
    disabledHours: () => range(0, 24).splice(24, 24),
    disabledMinutes: () => range(60, 60),
  };
}


// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  getCheckboxProps: record => ({
    disabled: record.name === "Disabled User", // Column configuration not to be checked
    name: record.name
  })
};


class TablaOrdenes extends Component {
  constructor(props) {
    super(props);
    console.log('this.props in tableordenes: ', this.props)
    this.state = {
      visible: false,
      visible1: false,
      dataLoading: true,
      formLoading: false,
      currentOrder: null,
      ordenes: [],
      recipient: '',
      messageTexte: 'Locatefit S.L., tu profeisonal de Locatefit a aceptado la orden contratada',
      messageTexteFinish: 'Locatefit S.L., La orden de Locatefit ha sido finalizada con éxito',
      messageTexteRechazar: 'Locatefit S.L., el profesional ha rechazado la orden lamentamos los incovenientes.',
      useridpush: '',
      Usere: ''
    };
  }


  refetch = null;
  profesionalId = localStorage.getItem('id');

  componentDidUpdate({ dateRange }) {
    console.log('dateRange: ', dateRange)
    if (dateRange && this.refetch) {
      this.setState({ dataLoading: true });
      this.refetch({ profesional: this.profesionalId, dateRange }).then(res => {
        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
          this.setState({ ordenes: res.data.getOrdenesByProfessional.list, dataLoading: false });
        }
      });
    }
  }

  componentDidMount(){
    this.props.client
    .query({
            query: USER_DETAIL,
            variables: { id: localStorage.getItem('id') }
            })
        .then(async (results)=> {
            const { loading, error, data } = results
            if (data) {
                this.setState({
                    useridpush: data && data.getUsuario ? data.getUsuario.UserID
                    : ''
                })
            }
        })
  }

  showAcceptModal = (e, data) => {
    console.log(data)
    e.preventDefault();
    this.setState({
      currentOrder: data,
      visible: true,
      recipient: data.client.telefono,
    });
  };

  showDetailModal = (e, data) => {
    e.preventDefault();
    this.setState({
      currentOrder: data,
      visible1: true,
    });
  };

  handleCancel = () => {
    this.setState({
      currentOrder: null,
      visible: false,
    });
  };

  hideDetailModal = () => {
    this.setState({
      currentOrder: null,
      visible1: false,
    });
  };

  SendTextSMS = () => {
    fetch(`${LOCAL_API_URL}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexte}`)
      .catch(err => console.log(err))
  };

  SendTextSMSFinish = () => {
    fetch(`${LOCAL_API_URL}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexteFinish}`)
      .catch(err => console.log(err))
  };

  SendPushNotificationAceptar = () => {
    fetch(`${LOCAL_API_URL}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexte}`)
      .catch(err => console.log(err))

      console.log(this.state.useridpush, this.state.messageTexte)
  };

  SendPushNotificationFinish = () => {
    fetch(`${LOCAL_API_URL}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteFinish}`)
      .catch(err => console.log(err))
      console.log(this.state.useridpush, this.state.messageTexteFinish)
  };




  

  formatResult = data => {
    if (data && data.getOrdenesByProfessional && data.getOrdenesByProfessional.success) {
      this.setState({
        dataLoading: false,
        ordenes: data.getOrdenesByProfessional.list,        
      });
    }
  }


  showRejectConfirmModal(e, ordenProceed, data) {
    e.preventDefault();
    const that = this;
    confirm({
      title: '¿Esta seguro que deseas rechazar esta orden?',
      content: 'Lamentamos que no haya podido cumplir con esta orden',
      okText: 'Si',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        return new Promise((resolve, reject, SendTextSMSRechazar, SendPushNotificationRechazar) => {
          const formData = {
            ordenId: data.id,
            estado: 'Rechazado',
            progreso: '0',
            status: 'exception',
          }
          that.props.client
          .query({
            query: USER_DETAIL,
            variables: { id: localStorage.getItem('id') }
            })
          .then(async (results)=> {
            const { loading, error, data } = results
            if (data) {
              const userIDPRO = data && data.getUsuario ? data.getUsuario.UserID : ''
                console.log('ergefrerf<<<<<<>>>>>>>>>>>', userIDPRO) 
                that.SendPushNotificationRechazar = () => {
                  fetch(`${LOCAL_API_URL}/send-push-notification?IdOnesignal=${userIDPRO}&textmessage=${that.state.messageTexteRechazar}`)
                    .catch(err => console.log(err))
                    console.log('notificaciones enviada', userIDPRO, that.state.messageTexteRechazar)
                };  
            }
        })

          
          that.SendTextSMSRechazar = () => {
            fetch(`${LOCAL_API_URL}/send-message?recipient=${data.client.telefono}&textmessage=${that.state.messageTexteRechazar}`)
              .catch(err => console.log(err))
              console.log('sending:  ', data.client.telefono, that.state.messageTexteRechazar)
          };
          ordenProceed({ variables: formData }).then(res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
              that.props.client
                .mutate({
                  mutation: CREATE_NOTIFICATION,
                  variables: { input: { orden: data.id, user: data.cliente, cliente: data.cliente, profesional: data.profesional, type: 'reject_order' } }
                })
                .then(async (results) => {
                  console.log("results", results)
                }).catch(err => {
                  console.log("err", err)
                })
                that.SendTextSMSRechazar()
                that.SendPushNotificationRechazar()
              setTimeout(() => {
                message.success(res.data.ordenProceed.message);
                if (that.refetch) {
                  that.refetch().then(res => {
                    if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                      that.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                    }
                  });
                }
              }, 300)
              resolve();
            }
          }).catch(err => {
            console.log(err)
            setTimeout(() => {
              message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
            }, 300)
          })
        })
      },

      onCancel() {
        console.log('Cancel');
      },
    });
  }



  finalizar(e, ordenProceed, data) {
    e.preventDefault();

    const formData = {
      ordenId: data.id,
      estado: 'Finalizada',
      progreso: '100',
      status: 'success',
    }
    ordenProceed({ variables: formData }).then(res => {
      if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
        this.props.client
          .mutate({
            mutation: CREATE_NOTIFICATION,
            variables: { input: { orden: data.id, user: data.cliente, cliente: data.cliente, profesional: data.profesional, type: 'finish_order' } }
          })
          .then(async (results) => {
            console.log("results", results)
          }).catch(err => {
            console.log("err", err)
          })
         this.SendTextSMSFinish()
         this.SendPushNotificationFinish()
        message.success('La orden ha sido marcada como finalizada éxitosamente');
        if (this.refetch) {
          this.refetch().then(res => {
            if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
              this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
            }
          });
        }
      }
    }).catch(err => {
      message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
    })

  }

  render() {

    const content = (
      <div>
        <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
          {(ordenProceed) => (
            <div className="nooota">
              <Form onSubmit={e => {
                e.preventDefault();
                this.props.form.validateFields((err, values) => {
                  if (!err) {
                    this.setState({ formLoading: true });
                    const formData = {
                      ordenId: this.state.currentOrder.id,
                      estado: 'Aceptado',
                      progreso: '75',
                      status: 'active',
                    }
                    ordenProceed({ variables: formData }).then(res => {
                      if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                        this.setState({ visible: false, formLoading: false });
                        this.props.client
                          .mutate({
                            mutation: CREATE_NOTIFICATION,
                            variables: { input: { orden: this.state.currentOrder.id, user: this.state.currentOrder.cliente, cliente: this.state.currentOrder.cliente, profesional: this.state.currentOrder.profesional, type: 'accept_order' } }
                          })
                          .then(async (results) => {
                            console.log("results", results)
                          }).catch(err => {
                            console.log("err", err)
                          })
                          this.SendTextSMS()
                          this.SendPushNotificationAceptar()
                        message.success(res.data.ordenProceed.message);
                        if (this.refetch) {
                          this.refetch().then(res => {
                            if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                              this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                            }
                          });
                        }
                      }
                    }).catch(err => {
                      this.setState({ visible: false, formLoading: false });
                      message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
                    })
                  }
                });
              }}>
                <p>Recuerda brindar el mejor servicio a tu cliente</p>

                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.formLoading}
                  className="btnflex"
                  shape="round"
                  icon="check"
                  style={{ marginRight: 10 }}
                >Aceptar orden</Button>

              </Form>
            </div>
          )}
        </Mutation>
      </div>
    );

    const columns = [
      {
        title: "Nº Pedido",
        key: "id",
        render: data => <p style={{ color: '#3b83bd' }} onClick={e => this.showDetailModal(e, data)}>{data.id}</p>
      },
      {
        title: "Fecha",
        dataIndex: "created_at",
        render: created_at => new Date(Number(created_at)).toLocaleDateString()
      },
      {
        title: "Estado",
        dataIndex: "estado"
      },
      {
        title: "Total",
        key: "total",
        render: dataIndex => dataIndex.cantidad * dataIndex.product.number + '€'
      },
      {
        title: "Acción",
        key: "action",

        render: dataIndex => {
          if (dataIndex && dataIndex.estado) {
            return (
              <span className="btnflex">
                <Popover content={content} trigger="click" title="Aceptar orden">
                  {dataIndex && dataIndex.estado === 'Nueva' ?
                    <Button
                      onClick={e => this.showAcceptModal(e, dataIndex)}
                      type="primary"
                      className="btnflex"
                      shape="round"
                      icon="check"
                      style={{ marginRight: 10 }}
                    >
                      Aceptar orden
                  </Button>

                    : ''}
                </Popover>

                {dataIndex && dataIndex.estado === 'Nueva' ?
                  <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                    {(ordenProceed) => (
                      <Button
                        type="danger"
                        className="btnflex"
                        shape="round"
                        icon="close"
                        style={{ marginRight: 10 }}
                        onClick={e => this.showRejectConfirmModal(e, ordenProceed, dataIndex)}
                      >Rechazar</Button>
                    )}
                  </Mutation>
                  : ''}

                {dataIndex && dataIndex.estado === 'Aceptado' ?
                  <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                    {(ordenProceed) => (
                      <Button
                        type="secondary"
                        className="btnflex"
                        shape="round"
                        icon="like"
                        onClick={e => this.finalizar(e, ordenProceed, dataIndex)}
                        style={{ marginRight: 10 }}
                      >Finalizada</Button>
                    )}
                  </Mutation>
                  : ''}



              </span>
            )
          } else {
            return ("")
          }
        }
      }
    ];

    return (
      <div>
        <Query query={PROFESSIONAL_ORDENES_QUERY} variables={{ profesional: this.profesionalId }} onCompleted={this.formatResult}>
          {({ refetch }) => {
            this.refetch = refetch;
            return (
              <div className="">
                <Table
                  rowKey={orden => orden.id}
                  rowSelection={rowSelection}
                  columns={columns}
                  loading={this.state.dataLoading}
                  dataSource={this.state.ordenes}
                />
              </div>
            )
          }}
        </Query>
        <Modal
          style={{ padding: 20 }}
          footer={null}
          title="Detalles de la orden"
          visible={this.state.visible1}
          onCancel={this.hideDetailModal}
        >
          <Modalordenes data={this.state.currentOrder} />
        </Modal>
      </div>
    );
  }
}

const WrappedTablaOrdenes = Form.create({ name: "TablaOrdenesForm" })(TablaOrdenes);

export default withApollo(WrappedTablaOrdenes);
