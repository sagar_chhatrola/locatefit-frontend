import React, { Component } from 'react'
import { Progress, Switch, Card, Icon, Avatar, Button } from 'antd';
import { LOCAL_API_URL } from './../../config';

const { Meta } = Card;

class Modalordenes extends Component {

  constructor(props) {
    super(props);

    console.log('props: ', props)

  }
  render() {
    let link = null;
    if (this.props.data) {
      const description = this.props.data.nota;
      const ubicacion = this.props.data.client.ciudad;
      const titulo = this.props.data.product.title;
      const date = new Date(this.props.data.endDate + ' ' + this.props.data.time);
      const year = date.getFullYear();
      // const year = date.getUTCFullYear();
      let month = date.getMonth() + 1;
      // let month = date.getUTCMonth() + 1;
      month = (month < 10) ? '0' + month : month;
      const day = date.getDate();
      // const day = date.getUTCDate();
      let hours = date.getHours();
      // let hours = date.getUTCHours();
      hours = (hours < 10) ? '0' + hours : hours;
      const minutes = date.getMinutes();
      // const minutes = date.getUTCMinutes();

      const startDateString = year + '' + month + '0' + day + 'T' + hours + '' + minutes + '000';

      const productoTime = this.props.data.product.time.split(':');
      let endHours = Number(hours) + (Number(productoTime[0]) * Number(this.props.data.cantidad));
      let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(this.props.data.cantidad));
      if (endMinutes > 60) {
        endHours++;
        endMinutes -= 60;
      }

      endHours = (endHours < 10) ? '0' + endHours : endHours;
      endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
      const endDateString = year + '' + month + '0' + day + 'T' + endHours + '' + endMinutes + '00';

      link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString +  "/" + endDateString + "&details=" + description + "&location=" + ubicacion + "&sprop=name:Locatefit&sf=true";
    }


    return (
      <div style={{padding: 20}}>
        <h6>Progreso de la orden</h6>
        <Progress strokeColor={{ from: '#108ee9', to: '#87d068', }}
          percent={this.props.data ? this.props.data.progreso : ''}
          status={this.props.data ? this.props.data.status : ''}
        />
        <Card style={{ width: '100%', marginTop: 16, marginBottom: 16 }}>
          <Meta
            title={
              <div>
                <h6>
                  Fecha del realización del servicio:
                </h6>
                <p>
                  {this.props.data ? this.props.data.endDate : ''} a las  {this.props.data ? this.props.data.time : ''}
                </p>
              </div>
            }
          />
          
          <Button
            type="secondary"
            className="btnflex"
            shape="round"
            icon="calendar"
          >
            {this.props.data ?
              <a style={{ textDecoration: 'none', marginLeft: 10 }} target="_blank" href={link}> Añadir a Google Calendar</a>
              : 'Añadir a Google Calendar'
            }

          </Button>
        </Card>
        
        <h6>Informacón del cliente</h6>
        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            title={this.props.data ? this.props.data.client.nombre : ''}
            description={
              <div>
                <p>
                  {this.props.data ? this.props.data.client.calle : ''}
                  <br />
                  {this.props.data ? this.props.data.client.ciudad : ''}, {this.props.data ? this.props.data.client.provincia : ''}
                  <br />
                  {this.props.data ? this.props.data.client.codigopostal : ''}
                  <br />
                  {this.props.data ? this.props.data.client.telefono : ''}
                </p>
              </div>
            }
          />
        </Card>
        <h6 style={{ marginTop: 16 }}>Informacón del servicio</h6>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={this.props.data && this.props.data.product.fileList.length > 0 ?
              <Avatar src={LOCAL_API_URL + "/assets/images/" + this.props.data.product.fileList[0]} /> :
              <Avatar src=" " />
            }
            title={this.props.data ? this.props.data.product.title : ''}
            description={this.props.data ? "Cantidad: " + this.props.data.cantidad + " " + this.props.data.product.currency : ''}
          />
        </Card>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            description={
              <div>
                <h6>Nota del cliente:</h6>
                <p>{this.props.data ? this.props.data.nota : ''}</p>
              </div>
            }
          />
        </Card>

      </div>
    )
  }
}

export default Modalordenes;
