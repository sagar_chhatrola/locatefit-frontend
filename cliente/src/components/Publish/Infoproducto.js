import React from "react";
import "antd/dist/antd.css";
import "./Publish.css";
import { Input, Form, Select, Popover, TimePicker } from "antd";
import { Query } from 'react-apollo';
import { CATEGORIES_QUERY } from '../../queries';
import * as moment from 'moment';
import locale from 'antd/es/date-picker/locale/es_ES'

const { Option } = Select;
const precio = [];
for (let i = 3; i === 3; i++) {
  precio.push(
    <Option key={i} value="/Hora">/Hora</Option>,
    <Option key={i} value="/Servicio">/Servicio</Option>,
    <Option key={i} value="/M²">/M²</Option>
  );
}

const ciudad = [];
for (let i = 3; i === 3; i++) {
  ciudad.push(
  
  );
}

const infocate = (
  <div>
    <p>Seleciona la categría que más de asemeje a tu servicio.</p>
  </div>
);

const infotittle = (
  <div>
    <p>
      Elige un titulo fácil con palabras llanas para mayor resultado en la
      busqueda.
    </p>
  </div>
);

const infoprecio = (
  <div>
    <p>Elige el precion de tu servicio que sea justo y considerable.</p>
  </div>
);

const infomoneda = (
  <div>
    <p>Aquí podras seleccionar el modo de tiempo.</p>
  </div>
);

const infodescricion = (
  <div>
    <p>
      La descricion es la parte fundamental a la hora de contratar un servicio
      se epecifico.
    </p>
  </div>
);

const infociudad = (
  <div>
    <p>Seleciona la ciudad donde quieres prestar tus servicios.</p>
  </div>
);

const infociudadUbicacion = (
  <div>
    <p>La ciudad ha sido ingresada por el servicio de ubicación.</p>
  </div>
);

const infohora = (
  <div>
    <p>Elige la duración de tu servicio Ejemplo (01:00) para una hora.</p>
  </div>
);

const { TextArea } = Input;

function onChange(value) {
  // console.log(`selected ${value}`);
}

function onBlur() {
  // console.log("blur");
}

function onFocus() {
  // console.log("focus");
}

function onSearch(val) {
  // console.log("search:", val);
}

class Infodelproducto extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: (this.props.value && this.props.value.title) || null,
      time: (this.props.value && this.props.value.time) || null,
      number: (this.props.value && this.props.value.number) || 0,
      currency: (this.props.value && this.props.value.currency) || "/Hora",
      category_id: (this.props.value && this.props.value.category_id) || null,
      city: (this.props.value && this.props.value.city) || null,
      description: (this.props.value && this.props.value.description) || null,
      errors: {},
      geoLocationEnabled: false
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    this.onTimeChange = this.onTimeChange.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onCityChange = this.onCityChange.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);
    // this.onErrorsChange = this.onErrorsChange.bind(this);
    this.triggerChange = this.triggerChange.bind(this);
    this.onChange = this.onChange.bind(this);

    this.getGeoLocation();

  }

  getGeoLocation() {
    if ("geolocation" in navigator) {

      navigator.geolocation.getCurrentPosition(
        (position) => {

          let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
          fetch(apiUrlWithParams)
            .then(response => response.json())
            .then(data => {

              let cityFound = false;

              for (let index = 0; index < data.results.length; index++) {

                for (let i = 0; i < data.results[index].address_components.length; i++) {

                  if (data.results[index].address_components[i].types.includes('locality')) {

                    this.setState({ city: data.results[index].address_components[i].long_name });
                    this.setState({ geoLocationEnabled: true });
                    cityFound = true;

                  }

                  if (cityFound) break;
                }

                if (cityFound) break;
              }

            })
            .catch(error => console.log('error in google geocode api: ', error));

        },
        (error) => console.log('geolocation error', error),


      );
    } else {
      console.log('this browser not supported HTML5 geolocation API')
    }
  }

  static getDerivedStateFromProps(nextProps) {
    // Should be a controlled component.
    if ("value" in nextProps) {
      return {
        ...(nextProps.value || {})
      };
    }
    return null;
  }

  handleTitleChange = e => {
    const title = e.target.value;
    this.setState(prevState => ({
      errors: { ...prevState.errors, title: false }
    }))

    if (!("value" in this.props)) {
      this.setState({ title }, () => {
        this.triggerChange({ title });
      });
    }
  };

  handleNumberChange = e => {
    const number = parseInt(e.target.value || 0, 10);
    if (isNaN(number)) {
      return;
    }
    this.setState(prevState => ({
      errors: { ...prevState.errors, number: false }
    }));
    if (!("value" in this.props)) {
      this.setState({ number }, () => {
        this.triggerChange({ number });
      });
    }
  };

  handleCurrencyChange = currency => {
    if (!("value" in this.props)) {
      this.setState({ currency }, () => {
        this.triggerChange({ currency });
      });
    }
  };

  triggerChange = changedValue => {
    // Should provide an event to pass value to Form.
    const { onChange } = this.props;
    if (onChange) {
      onChange({
        ...this.state,
        ...changedValue
      });
    }
  };

  onTimeChange = (date, time) => {

    this.setState(prevState => ({
      errors: { ...prevState.errors, time: false }
    }))

    this.setState({ time: time }, () => {
      this.triggerChange({ time: time });
    });
  };

  onCategoryChange = (value) => {

    this.setState(prevState => ({
      errors: { ...prevState.errors, category_id: false }
    }))

    this.setState({ category_id: value }, () => {
      this.triggerChange({ category_id: value });
    });
  };

  onCityChange = (value) => {

    this.setState(prevState => ({
      errors: { ...prevState.errors, city: false }
    }))

    this.setState({ city: value }, () => {
      this.triggerChange({ city: value });
    });
  };

  onDescriptionChange = ({ target: { value } }) => {

    this.setState(prevState => ({
      errors: { ...prevState.errors, description: false }
    }))

    this.setState({ description: value }, () => {
      this.triggerChange({ description: value });
    });
  };

  // onErrorsChange = ({ target: { value } }) => {
  //   this.setState({ errors: value }, () => {
  //     this.triggerChange({ errors: value });
  //   });
  // };

  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  validateFields() {
    if (!this.state.title) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, title: true }
      }))
    }

    if (!this.state.number) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, number: true }
      }))
    }

    if (!this.state.time) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, time: true }
      }))
    }

    if (!this.state.category_id) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, category_id: true }
      }))
    }

    if (!this.state.city) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, city: true }
      }))
    }

    if (!this.state.description) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, description: true }
      }))
    }

  }

  

  render() {
    const { size } = this.props;
    const { currency, number, errors } = this.state;
    const { time } = this.state;

    return (
      <div>
        <div className="titulodelprod">
          <Form.Item label="¿Qué estás servicio estas ofresiendo?"
            validateStatus={errors.title ? 'error' : 'success'}
            help={errors.title ? 'Este campo es requerido' : ''}
          >
            <Popover content={infotittle} trigger="click">
              <Input required placeholder="En pocas palabras" onChange={this.handleTitleChange} />
            </Popover>
          </Form.Item>
        </div>

        <span>
          <Form.Item label="Precio y duración del servicio"
            validateStatus={(errors.number || errors.time) ? 'error' : 'success'}>
            <Popover content={infoprecio} trigger="click">
              <Input
                placeholder="0,00"
                type="text"
                size={size}
                value={number}
                onChange={this.handleNumberChange}
                style={{ width: "20%", marginRight: "1%" }}
              />
            </Popover>

            <Popover content={infomoneda} trigger="hover">
              <Select
                placeholder="/Hora"
                value={currency}
                size={size}
                style={{ width: "15%", marginRight: "3%" }}
                onChange={this.handleCurrencyChange}
              >
                {precio}
              </Select>
            </Popover>

            <Popover content={infohora} trigger="click">
              <TimePicker defaultValue={moment('01:00', 'HH:mm')} locale={locale} onChange={this.onTimeChange} />
            </Popover>
          </Form.Item>
        </span>

        <div className="categi">
          <p>Categoría:</p>

          <Query query={CATEGORIES_QUERY}>
            {({ loading, error, data, refetch }) => {
              if (loading) return null;
              if (error) return console.log('error in CATEGORIES_QUERY: ', error);
              return <Form.Item label="" validateStatus={errors.category_id ? 'error' : 'success'}>
                <Popover content={infocate} trigger="hover">
                  <Select
                    showSearch
                    style={{ width: "100%" }}
                    placeholder="Seleciona una categoría"
                    optionFilterProp="children"
                    onChange={this.onCategoryChange}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onSearch={onSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {
                      data.getCategories.map(
                        (cat, i) => (
                          <Option key={i} value={cat.id}><img style={{width: 20, marginRight: 10}} src={cat.image} /> {cat.title}</Option>
                        )
                      )
                    }
                  </Select>
                </Popover>
              </Form.Item>
            }}
          </Query>
        </div>
        <div className="categi">
          <p>Ciudad:</p>
          <Form.Item label="" validateStatus={errors.city ? 'error' : 'success'}>
            {this.state.geoLocationEnabled ?
              <Popover content={infociudadUbicacion} trigger="hover">
                <Input disabled={true} value={this.state.city} />
              </Popover>
              :
              <Popover content={infociudad} trigger="hover">
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  placeholder="Seleciona tu ciudad"
                  optionFilterProp="children"
                  onChange={this.onCityChange}
                  onFocus={onFocus}
                  onBlur={onBlur}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {ciudad}
                </Select>
              </Popover>
            }
          </Form.Item>
        </div>

        <Form.Item label="Descripción" validateStatus={errors.description ? 'error' : 'success'}>
          <Popover content={infodescricion} trigger="click">
            <TextArea
              value={this.state.description}
              onChange={this.onDescriptionChange}
              placeholder="Añade detalles ecenciales de tu sevicio."
              autosize={{ minRows: 3, maxRows: 5 }}
            />
          </Popover>
        </Form.Item>
      </div>
    );
  }
}

export default Infodelproducto;
