import React from "react";
import { Select, Radio, Checkbox, TimePicker, Button, message } from "antd";
import "./Publish.css";
import moment from 'moment';
import locale from 'antd/es/date-picker/locale/es_ES';

const { Option } = Select;
const children = [];
for (let i = 7; i === 7; i++) {
  children.push(
    <Option value="Lunes">Lunes</Option>,
    <Option value="Martes">Martes</Option>,
    <Option value="Miercoles">Miercoles</Option>,
    <Option value="Jueves">Jueves</Option>,
    <Option value="Viernes">Viernes</Option>,
    <Option value="Sábado">Sabádo</Option>,
    <Option value="Domingo">Domingo</Option>
  );
}

class Disponibilidad extends React.Component {
  state = {
    loading: false,
    iconLoading: false
  };

  onChange = (key, time, timeString) => {
    let state = this.state;
    state[key] = timeString;
    this.setState(state, () => {
      this.triggerChange(state);
    });
  }
  triggerChange = changedValue => {
    // Should provide an event to pass value to Form.
    const { onChange } = this.props;
    if (onChange) {
      onChange({
        ...this.state,
        ...changedValue
      });
    }
  };


  handlePublish = () => {
    this.enterLoading(); // temp
    const { onPublish } = this.props;
    if (onPublish) {
      onPublish(this.state, (result) => {
        console.log('result', result);
        this.exitLoading();
        if (result && result.data && result.data.crearProducto && result.data.crearProducto.success) {
          message.success(result.data.crearProducto.message);

          setTimeout(() => {
            window.location = '/profile';
          }, 2000);

        } else if (result && result.data && result.data.crearProducto && !result.data.crearProducto.success) {
          message.error(result.data.crearProducto.message)
          setTimeout(() => {
            window.location = '/login';
          }, 1000);
        }
      });
    }
  };

  enterLoading = () => {
    this.setState({ loading: true });
  };

  exitLoading = () => {
    this.setState({ loading: false });
  };

  enterIconLoading = () => {
    this.setState({ iconLoading: true });
  };

  handleChange = (key, e) => {
    let state = this.state;
    if (e.target.checked)
      state[key] = e.target.value;
    else
      state[key] = null;

    this.setState(state, () => {
      this.triggerChange(state);
    });
  };

  render() {
    const {
      lunes,
      martes,
      miercoles,
      jueves,
      viernes,
      sabado,
      domingo
    } = this.state;

    return (
      <div>
        <div className="dias">
          <p>Seleciona los días que tienes diponible</p>
        </div>
        <div className='containerhoras'>
        <div className="horas">
          <Checkbox.Group>
            <Checkbox style={{width: 100}} value="Lunes" onChange={(e) => { this.handleChange("lunes", e) }}
               >
              Lunes
            </Checkbox>
          </Checkbox.Group>
          
          
          {/* <Radio.Group value={lunes} onChange={(e) => { this.handleChange("lunes", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Lunes"
            >
              Lunes
            </Radio.Button>
          </Radio.Group> */}
      
          <TimePicker
            use24Hours
            style={{marginRight: 20}}
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('lunes_from', time, timeString) }}
          />
          <TimePicker
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('lunes_to', time, timeString) }}
          />
          
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}}value="Martes" onChange={(e) => { this.handleChange("martes", e) }}
               >
              Martes
            </Checkbox>
          </Checkbox.Group>

          {/* <Radio.Group value={martes} onChange={(e) => { this.handleChange("martes", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Martes"
            >
              Martes
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('martes_from', time, timeString) }}
          />
          <TimePicker
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('martes_to', time, timeString) }}
          />
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}}value="Miércoles" onChange={(e) => { this.handleChange("miercoles", e) }}
               >
              Miércoles
            </Checkbox>
          </Checkbox.Group>

          {/* <Radio.Group value={miercoles} onChange={(e) => { this.handleChange("miercoles", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Miércoles"
            >
              Miércoles
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('miercoles_from', time, timeString) }}
          />
          <TimePicker
            
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('miercoles_to', time, timeString) }}
          />
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}} value="Jueves" onChange={(e) => { this.handleChange("jueves", e) }}
               >
              Jueves
            </Checkbox>
          </Checkbox.Group>
          
          {/* <Radio.Group value={jueves} onChange={(e) => { this.handleChange("jueves", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Jueves"
            >
              Jueves
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('jueves_from', time, timeString) }}
          />
          <TimePicker
          
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('jueves_to', time, timeString) }}
          />
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}} value="Viernes" onChange={(e) => { this.handleChange("viernes", e) }}
               >
              Viernes
            </Checkbox>
          </Checkbox.Group>

          {/* <Radio.Group value={viernes} onChange={(e) => { this.handleChange("viernes", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Viernes"
            >
              Viernes
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('viernes_from', time, timeString) }}
          />
          <TimePicker
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('viernes_to', time, timeString) }}
          />
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}} value="Sábado" onChange={(e) => { this.handleChange("sabado", e) }}
               >
              Sábado
            </Checkbox>
          </Checkbox.Group>

          {/* <Radio.Group value={sabado} onChange={(e) => { this.handleChange("sabado", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Sábado"
            >
              Sábao
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('sabado_from', time, timeString) }}
          />
          <TimePicker
           
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('sabado_to', time, timeString) }}
          />
        </div>

        <div className="horas">
        <Checkbox.Group>
            <Checkbox style={{width: 100}} value="Domingo" onChange={(e) => { this.handleChange("domingo", e) }}
               >
              Domingo
            </Checkbox>
          </Checkbox.Group>

          {/* <Radio.Group value={domingo} onChange={(e) => { this.handleChange("domingo", e) }}>
            <Radio.Button
              style={{ top: "10px", bottom: "20px", width: "120px" }}
              value="Domingo"
            >
              Domingo
            </Radio.Button>
          </Radio.Group> */}
          <TimePicker
            style={{marginRight: 20}}
            use24Hours
            defaultValue={moment('10:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('domingo_from', time, timeString) }}
          />
          <TimePicker
          
            use24Hours
            defaultValue={moment('18:00', 'HH:mm')}
            onChange={(time, timeString) => { this.onChange('domingo_to', time, timeString) }}
          />
        </div>
        </div>

        <div className="botonpublic">
          <Button
            type="primary"
            loading={this.state.loading}
            onClick={this.handlePublish}
          >
            Publicar!
          </Button>
        </div>
      </div>
    );
  }
}

export default Disponibilidad;
