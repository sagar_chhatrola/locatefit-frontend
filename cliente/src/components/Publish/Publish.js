import React, { Component, createRef } from "react";
import "./Publish.css";
import { Icon, Alert } from "antd";
import Fotoproductos from "./fotoproductos";
import Disponibilidad from "./disponibilidad";
import Infoproducto from "./Infoproducto";
import { Mutation } from 'react-apollo';
import { NUEVO_PRODUCT } from '../../mutations';


class Publish extends Component {
  constructor(props) {
    super(props);
    this.InfoproductoElement = createRef();
    this.state = {
      city: null,
      category_id: null,
      currency: null,
      description: null,

      domingo: null,
      domingo_from: null,
      domingo_to: null,

      jueves: null,
      jueves_from: null,
      jueves_to: null,

      lunes: null,
      lunes_from: null,
      lunes_to: null,

      martes: null,
      martes_from: null,
      martes_to: null,

      miercoles: null,
      miercoles_from: null,
      miercoles_to: null,

      number: null,

      sabado: null,
      sabado_from: null,
      sabado_to: null,

      time: null,
      title: null,

      viernes: null,
      viernes_from: null,
      viernes_to: null,

      fileList: null,

      created_by: null,
      errors: {},
      fileError: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handlePublish = this.handlePublish.bind(this);

    let initialState = {
      city: null,
      category_id: null,
      currency: null,
      description: null,

      domingo: null,
      domingo_from: null,
      domingo_to: null,

      jueves: null,
      jueves_from: null,
      jueves_to: null,

      lunes: null,
      lunes_from: null,
      lunes_to: null,

      martes: null,
      martes_from: null,
      martes_to: null,

      miercoles: null,
      miercoles_from: null,
      miercoles_to: null,

      number: null,

      sabado: null,
      sabado_from: null,
      sabado_to: null,

      time: null,
      title: null,

      viernes: null,
      viernes_from: null,
      viernes_to: null,

      fileList: null,

      created_by: null,
      errors: {},
      fileError: false
    };
  }

  handleFileError() {
    if (!this.state.fileList) {
      this.setState({ fileError: true });
    } else if (this.state.fileList.length == 0) {
      this.setState({ fileError: true });
    }
  }

  handleChange(state) {
    this.setState({ ...this.state, ...state }, () => {
      // console.log('this.state1', this.state);
    });
  };

  handlePublish() {
    // console.log('this.state', this.state);
  };


  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>Publicar anuncio</h3>
          <p>Aquí podrás subir un servicios</p>
        </div>

        <div className="cont">
          <span className="subtitle">
            <Icon type="info" /> INFORMACIÓN DE TU PRODUCTO
          </span>
          <Infoproducto onChange={this.handleChange} data={this.state} ref={this.InfoproductoElement} />
        </div>
        <div className="cont">
          <span className="subtitle">
            <Icon type="picture" /> FOTOS
          </span>

          <Fotoproductos onChange={this.handleChange} data={this.state} />
          {this.state.fileError ? <Alert message="Debes agregar un mínimo de una foto." type="error" /> : null}

        </div>
        <div className="cont">
          <span className="subtitle">
            <Icon type="calendar" />
            INFORMACIÓN DE DISPONIBILIDAD
          </span>
          <Mutation
            mutation={NUEVO_PRODUCT}
          >
            {(crearProducto, { loading, error, data }) => {
              if (error) console.log('NUEVO_PRODUCT in error : ', error)
              return (
                <Disponibilidad onChange={this.handleChange} onPublish={async (state, callback) => {
                  console.log('this.state on submit: ', this.state)

                  this.handleFileError();
                  this.InfoproductoElement.current.validateFields();

                  let data;
                  if (this.state.title && this.state.number && this.state.time &&
                    this.state.category_id && this.state.city && this.state.description &&
                    (this.state.fileList && this.state.fileList.length > 0)) {
                    const input = {
                      city: this.state.city,
                      category_id: this.state.category_id,
                      currency: this.state.currency,
                      description: this.state.description,

                      domingo: this.state.domingo,
                      domingo_from: this.state.domingo_from,
                      domingo_to: this.state.domingo_to,

                      jueves: this.state.jueves,
                      jueves_from: this.state.jueves_from,
                      jueves_to: this.state.jueves_to,

                      lunes: this.state.lunes,
                      lunes_from: this.state.lunes_from,
                      lunes_to: this.state.lunes_to,

                      martes: this.state.martes,
                      martes_from: this.state.martes_from,
                      martes_to: this.state.martes_to,

                      miercoles: this.state.miercoles,
                      miercoles_from: this.state.miercoles_from,
                      miercoles_to: this.state.miercoles_to,

                      number: this.state.number,

                      sabado: this.state.sabado,
                      sabado_from: this.state.sabado_from,
                      sabado_to: this.state.sabado_to,

                      time: this.state.time,
                      title: this.state.title,

                      viernes: this.state.viernes,
                      viernes_from: this.state.viernes_from,
                      viernes_to: this.state.viernes_to,

                      fileList: this.state.fileList
                    }
                    data = await crearProducto({
                      variables: { input }
                    })
                  }
                  if (typeof callback === 'function') {
                    callback(data);
                  }
                  // console.log('data in after: ', data)
                  if (data) {
                    // window.location = '/services';
                    // this.props.history.push('/services')
                    this.setState(this.initialState);
                  }
                }} data={this.state} />
              )
            }}
          </Mutation>
        </div>
      </div>
    );
  }
}

Publish.propTypes = {};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default Publish;
