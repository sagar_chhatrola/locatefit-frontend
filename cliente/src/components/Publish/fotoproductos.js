import React, { Component } from "react";
import { Mutation } from 'react-apollo';
import { Icon, Button } from 'antd';
import { UPLOAD_FILE } from "../../mutations";
import "./Publish.css"
import { LOCAL_API_URL } from './../../config';

const imgBaseUrl = LOCAL_API_URL + '/assets/images/';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class Fotoproductos extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fileList: (this.props.value && this.props.value.fileList) || [],
      imgBlobs: (this.props.value && this.props.value.imgBlobs) || [],
      imageUploadLoading: false, 
    };
    this.handleChange = this.handleChange.bind(this);
    this.triggerChange = this.triggerChange.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = imgBlob => {
    this.setState({ imgBlobs: [...this.state.imgBlobs, imgBlob] })
  };

  handleChange = (file) => {
    this.setState({ fileList: [...this.state.fileList, file] }, () => {
      this.triggerChange({ file });
    });
  }

  triggerChange = changedValue => {
    // Should provide an event to pass value to Form.
    const { onChange } = this.props;
    if (onChange) {
      onChange({
        ...this.state,
        ...changedValue
      });
    }
  };

  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  handleEliminar(e, file) {

    e.preventDefault();

    const files = this.state.fileList.filter(f => f !== file);

    this.setState({ fileList: files }, () => {
      const { onChange } = this.props;
      if (onChange) {
        onChange({
          ...this.state,
        });
      }
    });

  }

  render() {

    const { fileList, imgBlobs } = this.state;
    const uploadButton = (
      <div className="ant-upload-1">
        <Icon type="plus" />
        <div className="ant-upload-text">Añadir imagenes</div>
      </div>
    );

    return (


      <div className="clearfix">
        {this.state.fileList.map(file => (
          <div className="imGpreview">
            <img style={{ width: '108px', height:'110px', marginTop: 10 }} src={imgBaseUrl + file} key={file} /><br />
            
             <Icon type="delete" onClick={e => this.handleEliminar(e, file)} />
           
          </div>
        ))}
        {/* {imgBlobs} */}
        <Mutation mutation={UPLOAD_FILE}>
          {(singleUpload) => (
            <label>
              <input
                style={{ display: 'none' }}
                type="file"
                required
                onChange={async ({ target: { validity, files: [file] } }) => {
                  this.setState({ imageUploadLoading: true });
                  let imgBlob = await getBase64(file);
                  // this.handlePreview(imgBlob);
                  validity.valid && singleUpload({ variables: { imgBlob } }).then((res) => {
                    this.handleChange(res.data.singleUpload.filename)
                    this.setState({ imageUploadLoading: false });
                  }).catch(error => {
                    console.log('fs error: ', error)
                    this.setState({ imageUploadLoading: false });
                  })
                  
                }
                }
              />
              {fileList.length <= 4 ? this.state.imageUploadLoading ? <div className="image-loader"></div> : uploadButton : null}

            </label>
          )}
        </Mutation>
      </div>
    );
  }
}

export default Fotoproductos;
