import React, { Component } from "react";
import Formulario from "./Formulario";
import "./Address.css";
import {Icon} from 'antd';

class Address extends Component {

  state = {
    direccion: {}
  };

  setDireccion = ({ direccion = {} }) => {
    console.log('setDireccion', { direccion });
    this.setState({ direccion });
  };

  eliminarDireccion = () => {
    console.log('eliminarDireccion');
    this.setState({ direccion: {} }, () => {
      console.log('formularioPayment -->', this.state);
    });
  };

  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>DIRECCIONES</h3>
          <p>
            Aquí podras gestionar tus pedidos, ver su estado y todo sus detalles
          </p>
        </div>
        <div className="cont">
        <span className="subtitle">
            <Icon type="environment" /> Dirección
          </span>
          <Formulario
          setDireccion={this.setDireccion}
          eliminarDireccion={this.eliminarDireccion}
          direccion={this.state.direccion} />
        </div>
      </div>
    );
  }
}

Address.propTypes = {};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default Address;
