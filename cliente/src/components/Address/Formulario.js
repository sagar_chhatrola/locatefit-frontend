import React, { Component } from 'react';
import { Form, Button, Row, Col, Input, message, Spin } from 'antd';
import { Query, Mutation } from 'react-apollo';
import { useQuery } from '@apollo/react-hooks';
import _get from 'lodash.get';

import { USUARIO_DIRECCION_QUERY } from '../../queries';
import {
  NUEVO_USUARIO_DIRECCION,
  ELIMINAR_USUARIO_DIRECCION
} from '../../mutations';

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class Formulario extends Component {
  state = {
    direccion: {}
  };

  handleSubmit = async (
    event,
    crearUsuarioDireccion,
    refetch = () => console.log('no refetch')
  ) => {
    event.preventDefault();
    this.refetch = refetch;
    const { setDireccion } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const input = { ...values };

        crearUsuarioDireccion({ variables: { input } }).then(
          async ({ data: res }) => {
            if (
              res &&
              res.crearUsuarioDireccion &&
              res.crearUsuarioDireccion.success
            ) {
              message.success(res.crearUsuarioDireccion.message);
              refetch();
              this.props.form.resetFields();
              const direccionNueva = _get(
                res,
                'crearUsuarioDireccion.data',
                null
              );
              if (direccionNueva && direccionNueva.id) {
                setDireccion({ direccion: direccionNueva });
              }

              console.log('crearUsuarioDireccion response', res);
            } else if (
              res &&
              res.crearUsuarioDireccion &&
              !res.crearUsuarioDireccion.success
            )
              message.error(res.crearUsuarioDireccion.message);
          }
        );
      }
    });
  };

  handleEliminar = (e, eliminarUsuarioDireccion, id, refetch = () => {}) => {
    e.preventDefault();

    const { eliminarDireccion } = this.props;

    eliminarUsuarioDireccion({ variables: { id } }).then(async ({ data }) => {
      if (
        data &&
        data.eliminarUsuarioDireccion &&
        data.eliminarUsuarioDireccion.success
      ) {
        message.success(data.eliminarUsuarioDireccion.message);
        eliminarDireccion();
      } else if (
        data &&
        data.eliminarUsuarioDireccion &&
        !data.eliminarUsuarioDireccion.success
      ) {
        message.error(data.eliminarUsuarioDireccion.message);
      }
    });
  };

  setRefetch = refetch => {
    this.refetch = refetch;
  };

  render() {
    const { getFieldDecorator, getFieldsError } = this.props.form;
    const { direccion, setDireccion } = this.props;
    console.log('direccion in props', { direccion });
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 }
    };

    const isDisableForm = !!direccion && !!direccion.id;

    return (
      <Row type='flex' gutter={16}>
        <Col md={12}>
          <Mutation mutation={NUEVO_USUARIO_DIRECCION}>
            {crearUsuarioDireccion => {
              return (
                <Form
                  {...formItemLayout}
                  onSubmit={e => {
                    e.preventDefault();
                    this.handleSubmit(e, crearUsuarioDireccion);
                  }}
                >
                  <Form.Item {...formItemLayout} label='Nombre'>
                    {getFieldDecorator('nombre', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade tu nombre'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Nombre completo'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label='Calle'>
                    {getFieldDecorator('calle', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade una calle'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Añade una calle'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label='Ciudad'>
                    {getFieldDecorator('ciudad', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade una ciudad'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Añade una ciudad'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label='Provincia'>
                    {getFieldDecorator('provincia', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade una provincia'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Añade una provincia'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label='Código Postal'>
                    {getFieldDecorator('codigopostal', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade un código postal'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Añade un código postal'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label='Teléfono'>
                    {getFieldDecorator('telefono', {
                      rules: [
                        {
                          required: true,
                          message: 'Añade una teléfono'
                        }
                      ]
                    })(
                      <Input
                        placeholder='Añade una teléfono'
                        disabled={isDisableForm}
                      />
                    )}
                  </Form.Item>

                  <Form.Item className='Centred-address-button'>
                    <Button
                      disabled={isDisableForm || hasErrors(getFieldsError())}
                      type='primary'
                      htmlType='submit'
                    >
                      Añadir Dirección
                    </Button>
                  </Form.Item>
                </Form>
              );
            }}
          </Mutation>
        </Col>
        <Col md={12}>
          {(!direccion || !direccion.id) && (
            <DireccionExistente
              refetch={this.refetch}
              handleEliminar={this.handleEliminar}
              setDireccion={setDireccion}
              setRefetch={this.setRefetch}
            />
          )}
          {!!direccion && (
            <DireccionNueva
              handleEliminar={this.handleEliminar}
              direccion={direccion}
            />
          )}
        </Col>
      </Row>
    );
  }
}

const DireccionExistente = ({
  handleEliminar = () => {},
  setDireccion = () => {},
  setRefetch = () => {}
}) => {
  const {
    loading,
    error,
    data,
    refetch = () => console.log('DireccionExistente -> no refetch')
  } = useQuery(USUARIO_DIRECCION_QUERY, {
    fetchPolicy: 'network-only'
  });
  setRefetch(refetch);
  if (loading) return (<div className="page-loader">
  <Spin size="large"></Spin>
</div>);
  if (error) return `Error! ${error.message}`;
  const direccionId = _get(data, 'getUsuarioDireccion.data.id', null);
  if (direccionId) {
    const {
      id,
      nombre,
      calle,
      ciudad,
      provincia,
      codigopostal,
      telefono
    } = data.getUsuarioDireccion.data;
    setDireccion({
      direccion: {
        id,
        nombre,
        calle,
        ciudad,
        provincia,
        codigopostal,
        telefono
      }
    });
    return (
      <div className='viewDirection'>
        <h5>Dirección Actual</h5>
        <address key={id}>
          <br />
          <h5>{nombre}</h5>
          <p className='addres'>
            {calle}
            <br />
            {ciudad}, {provincia}
            <br />
            {codigopostal},<br />
            {telefono}
          </p>
          <Mutation mutation={ELIMINAR_USUARIO_DIRECCION}>
            {(eliminarUsuarioDireccion, { loading, error, data }) => {
              return (
                <Button
                  type='danger'
                  className='btnflex'
                  shape='round'
                  icon='delete'
                  style={{ marginRight: 10 }}
                  onClick={e =>
                    handleEliminar(e, eliminarUsuarioDireccion, id, refetch)
                  }
                >
                  Eliminar
                </Button>
              );
            }}
          </Mutation>
        </address>
      </div>
    );
  }
  return <div />;
};

const DireccionNueva = ({
  handleEliminar = () => {},
  refetch = () => {},
  direccion = {}
}) => {
  if (direccion && direccion.id) {
    const {
      id,
      nombre,
      calle,
      ciudad,
      provincia,
      codigopostal,
      telefono
    } = direccion;
    return (
      <div className='viewDirection'>
        <h5>Dirección Actual</h5>
        <address key={id}>
          <br />
          <h5>{nombre}</h5>
          <p className='addres'>
            {calle}
            <br />
            {ciudad}, {provincia}
            <br />
            {codigopostal},<br />
            {telefono}
          </p>
          <Mutation mutation={ELIMINAR_USUARIO_DIRECCION}>
            {(eliminarUsuarioDireccion, { loading, error, data }) => {
              return (
                <Button
                  type='danger'
                  className='btnflex'
                  shape='round'
                  icon='delete'
                  style={{ marginRight: 10 }}
                  onClick={e => handleEliminar(e, eliminarUsuarioDireccion, id)}
                >
                  Eliminar
                </Button>
              );
            }}
          </Mutation>
        </address>
      </div>
    );
  }
  return <div />;
};

const WrappedDemo = Form.create({ name: 'validate_other' })(Formulario);

export default WrappedDemo;
