import React from 'react';
import { Query } from 'react-apollo';
import { USUARIO_ACTUAL } from '../queries';

const Session = Component => props => (
  <Query query={USUARIO_ACTUAL}>
    {({ loading, error, data, refetch }) => {
      console.log('in session');
      console.log(loading);
      console.log(error);
      console.log(data);
      console.log(refetch);
      if (loading) return null;
      if (error) return console.log(error);
      return <Component {...props} refetch={refetch} session={data} />;
    }}
  </Query>
);

export default Session;
