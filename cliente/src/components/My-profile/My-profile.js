import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  FaLink,
  FaMap,
  FaPhone,
  FaEnvelope,
  FaStar,
  FaCheckCircle
} from "react-icons/fa";
import { connect } from "react-redux";
import profileImg from "./../../assets/images/profile_pic/profile_pic.png";

import "./My-profile.css";

class MyStats extends Component {
  render() {
    return (
      <div className="pefiluser">
        <div className="sellercard">
          <div className="online">
            <p> · En Línea</p>
          </div>

          <img className="profilepic" src={profileImg} alt="" />

          <div className="profilename">
            <h3>
              Jose Martinez
              <span className="verifiedcheck">
                <FaCheckCircle />
              </span>
            </h3>
          </div>

          <p>
            Mienbro desde 2015 en<span className="Locatefit"> Locatefit </span>
            <span className="point">
              
              4,2 <FaStar />
            </span>
          </p>

          <div className="profilebutton">
            <a className="profilebutton1" href="/public-profile">
              <FaLink /> Vista de perfil público
            </a>
          </div>

          <div className="from">
            <p>
              <FaMap /> Desde Burgos
            </p>
            <p>
              
              <FaEnvelope /> josemartinez@gmail.com
            </p>
            <p>
              <FaPhone /> +34 692 92 47 67
            </p>

            <p></p>
          </div>
        </div>

        <div className="profilecontact">
          <div className="profilecontactdes">
            <h4 className="h4">Descripción</h4>
          </div>
          <div className="profilecontactpa">
            <p className="p1">
              Lorem Ipsum es simplemente un texto de relleno de la industria de
              impresión y tipografía. Lorem Ipsum ha sido el texto ficticio
              estándar de la industria desde la década de 1500, cuando una
              impresora desconocida tomó una galera de tipos y la mezcló para
              hacer un libro de muestras tipo.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

MyStats.propTypes = {};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default MyStats;
