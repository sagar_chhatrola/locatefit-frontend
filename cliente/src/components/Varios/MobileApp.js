import React, { Component } from "react";
import customerService1png from "./../../assets/images/customerService/customerService1 copia.png";
import mapPinImg from "./../../assets/images/locationpin/locationpin.png";
import playStoreImg from "./../../assets/images/play_store/play_store.png";
import appStoreImg from "./../../assets/images/apple_store/apple_store.png";
import Movil from './movilG.png';

import "./MobileApp.css";

export default class MobileApp extends Component {
  render() {
    return (
      <section className="mobileAppSection">
        <div className="container">
          <div className="customerServiceCard">
            <div className='container1'>
              <div className="serviceTitle">
                <h3>
                  Si eres un profesional, brinda tus servicios a millones de
                  personas cerca de ti, utiliza nuestra herramienta para
                  trabajar por tu cuenta.
              </h3>
              </div>
              <div className="workLabel">
                <p>Empieza a trabajar por tu cuenta se libre.</p>
              </div>
              <div className="startButton">
                <a href="/become-profesional">Empezar ahora</a>
              </div>
            </div>
            <div className='container1'>
              <img className='custorme' src={customerService1png} />
            </div>
          </div>
          <div className="MovilApp">
            <div className='movilsec'>
              <div className='textee'>
                <h1>¡Descárgate la app!</h1>
                <p>Gestiona tus ordenes cómodamente y contrata cuando y desde dónde quieras, los mejores profeisonales en tu manos, ¿ya te la has descargado?</p>
                <div className="app-btns">
                  <a title="Apple Store" href="https://apps.apple.com/us/app/locatefit/id1499263781?mt=8&ign-mpt=uo%3D4" className="app-store">
                    <img alt="Apple Store" height={40} src={appStoreImg} />
                  </a>
                  <a title="Play Store" href="https://play.google.com/store/apps/details?id=es.locatefit">
                    <img alt="Play Store" height={40} src={playStoreImg} />
                  </a>
                </div>
              </div>
            </div>
            <div className='movilsec'>
              <img className='phone' src={Movil} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}
