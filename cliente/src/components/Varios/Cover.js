import React, { Component } from "react";
import "./Cover.css";

import coverpageImg from "./../../assets/images/cover_page/chica.png";
import smallCheckImg from "./../../assets/images/check.svg";

export default class Cover extends Component {
  render() {
    return (
      <section className="coverSection">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="cover-left">
                <h2>
                  Encuentra personas de confianza <br />
                  en Locatefit, y contrate con seguridad.
                </h2>
              </div>
              <ul className="ul-cover">
                <li>
                  <img src={smallCheckImg} height={20} alt="" />
                  <b className="ul-cover1"> Protección de pago garantizado.</b>
                  <br />
                  <span>
                    El pago se libera al profesional independientemente una vez
                    esta satisfecho y obtiene el trabajo.
                  </span>
                </li>
                <li>
                  <img src={smallCheckImg} height={20} alt="" />
                  <b className="ul-cover1">
                    
                    Conosca el precio por adelantado.
                  </b>
                  <br />
                  <span>
                    Encuentra cualquier servicio en cuestión de minuto y sepa
                    exactamente lo que pagará, las tarifa son por hora o servicio en concreto. 
                  </span>
                </li>
                <li>
                  <img src={smallCheckImg} height={20} alt="" />
                  <b className="ul-cover1"> Estamos aquí para usted 24/7.</b>
                  <br />
                  <span>
                    Locatefit esta quí para usted, desde responder cualquier
                    pregunta hasta resolver cualquier problema en cualquier
                    momento.
                  </span>
                </li>
              </ul>
            </div>
            <div className="col-md-6">
              <div className="cover-right">
                <img
                  className="cover-right"
                  width={"100%"}
                  src={coverpageImg}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
