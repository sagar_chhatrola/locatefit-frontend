import React, { Component } from "react";
import "./Quet.css";
import { Query } from 'react-apollo';
import { CATEGORIES_QUERY } from '../../queries';

export default class Quet extends Component {
  render() {
    let categorialink = "/services/"
    return (
      <section className="quet-section">
        <div className="container">
          <div className="qutio-title">
            <div className="quts-name">
              <h2>¿Quieres más opciones?</h2>
              <p className="quts-name2">
                Aquí tenemos todas nuestras categorías
              </p>
            </div>
            <Query query={CATEGORIES_QUERY}>
              {({ loading, error, data, refetch }) => {
                if (loading) return null;
                if (error) return console.log(error);
                return <div className="category-cards">
                  <ul className="qut-li">
                    {
                      data.getCategories.map(
                        (cat, i) => (
                          <li key={i}>
                            <a href={categorialink + cat.id}>{cat.title}</a>
                          </li>
                        )
                      )
                    }
                  </ul>

                </div>

              }}
            </Query>
          </div>
        </div>
      </section>
    );
  }
}
