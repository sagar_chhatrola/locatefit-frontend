import noEncontrada from '../../assets/images/image-not-found.png';
import React, { Component } from 'react';
class Img extends Component {
  constructor(props) {
    super(props);

    this.state = {
      src: noEncontrada
    };
  }

  componentDidMount() {
    const img = document.createElement('img');
    const { fullResImage } = this.props;

    // when image loads, push actual URL to state
    img.onload = e => {
      if (fullResImage) {
        this.setState({ src: fullResImage });
      }
    };

    // start loading image
    img.src = fullResImage;
  }

  render() {
    const { fullResImage, ...rest } = this.props;
    return <img {...rest} src={this.state.src} />;
  }
}

export default Img;
