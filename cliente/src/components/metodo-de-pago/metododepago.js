import React from 'react';
import PasarelaPayment from "./../../containers/Payment/pasarela";
import { Icon } from "antd";
import Paypal from "./../../assets/images/PayPal-logo/PayPal-logo.png";
import MasterCardImg from "./../../assets/images/Master-Card/Master-Card.png";


export default class MyApp extends React.Component {
  render() {

    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>MÉTODO DE PAGO</h3>
          <p>Añade una tarjeta de crédito para mas facilidad al momento de realizar una compra</p>
        </div>
        <div className="cont">
          <span className="subtitle">
            <Icon type="credit-card" /> Añade una tarjeta de crédito o débito <span className="lo"><img src={MasterCardImg} alt="" /></span>
          </span>

          <PasarelaPayment />

        </div>
      </div>
    );
  }
}