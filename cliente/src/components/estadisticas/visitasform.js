import { Chart, Tooltip, Axis, Line, Point } from 'viser-react';
import * as React from 'react';
import { Query } from 'react-apollo';
import { GET_PRODUCTO_VISITAS } from '../../queries';

const initialChartData = [
  { mes_id: 0, mes: 'Ene', Visitas: 0 },
  { mes_id: 1, mes: 'Feb', Visitas: 0 },
  { mes_id: 2, mes: 'Mar', Visitas: 0 },
  { mes_id: 3, mes: 'Abr', Visitas: 0 },
  { mes_id: 4, mes: 'Mayo', Visitas: 0 },
  { mes_id: 5, mes: 'Jun', Visitas: 0 },
  { mes_id: 6, mes: 'Jul', Visitas: 0 },
  { mes_id: 7, mes: 'Ago', Visitas: 0 },
  { mes_id: 8, mes: 'Sep', Visitas: 0 },
  { mes_id: 9, mes: 'Oct', Visitas: 0 },
  { mes_id: 10, mes: 'Nov', Visitas: 0 },
  { mes_id: 11, mes: 'Dic', Visitas: 0 },
];

const scale = [{
  dataKey: 'Visitas',
  min: 0,
}, {
  dataKey: 'mes',
  min: 0,
  max: 1,
}];

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      chartData: initialChartData
    }
  }

  formatResult = res => {
    if (res && res.getProductoVisitas && res.getProductoVisitas.success) {
      this.setState(state => {
        let chartData = state.chartData;
        chartData.map(item => {
          const visitExisted = res.getProductoVisitas.list.find(element => element.mes_id === item.mes_id);
          if (visitExisted) {
            item.Visitas = visitExisted.visitas;
          }else {
            item.Visitas = 0
          }
        });
        return { chartData };
      });
    }
  }

  render() {
    return (
      <Query query={GET_PRODUCTO_VISITAS} variables={{ productId: this.props.productId }} onCompleted={this.formatResult}>
        {({ loading, error }) => {
          if (loading) console.log('loading products visits');
          if (error) console.log('error getting product visits: ', error);

          return (
            <Chart forceFit height={400} data={this.state.chartData} scale={scale}>
              <Tooltip />
              <Axis />
              <Line position="mes*Visitas" />
              <Point position="mes*Visitas" shape="circle" />
            </Chart>
          )
        }}
      </Query>


    );
  }
}