import { Chart, Tooltip, Axis, Legend, Bar } from 'viser-react';
import * as React from 'react';
import { withApollo,Query } from 'react-apollo';
import { GET_STATISTICS } from '../../queries';
const DataSet = require('@antv/data-set');



class Estadisticas extends React.Component {
  render() {
    // const sourceData = [
    //   { name: 'Ordenes', 'Ene': 100, 'Feb': 34, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0,'Nov': 0, 'Dic': 0 },
    //   { name: 'Ganacias', 'Ene': 4587, 'Feb': 45, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0,'Nov': 0, 'Dic': 0 },
    //   { name: 'Devoluciones', 'Ene': 56, 'Feb': 56, 'Mar': 0, 'Abr': 0, 'May': 0, 'Jun': 0, 'Jul': 0, 'Aug': 0, 'Sep': 0, 'Oct': 0,'Nov': 0, 'Dic': 0 },
    // ];
    
    
   return <Query query={GET_STATISTICS} variables={{ userType:'cliente',userId: localStorage.getItem('id') }}>
    {({ loading, error, data, refetch }) => {
      this.refetch = refetch;
      if (loading) return null;
      if (error)
    return console.log('error getting user detail: ', error);
    const dv = new DataSet.View().source(data.getStatistics.data);
    dv.transform({
      type: 'fold',
      fields: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'],
      key: 'locatefit',
      value: 'estadistica',
    });
    const sourceData = dv.rows;
    return (
      <Chart forceFit height={400} data={sourceData}>
        <Tooltip />
        <Axis />
        <Legend />
        <Bar position="locatefit*estadistica" color="name" adjust={[{ type: 'dodge', marginRatio: 1 / 32 }]} />
      </Chart>
    );
    }}
    </Query>
  }
}

export default withApollo(Estadisticas)
