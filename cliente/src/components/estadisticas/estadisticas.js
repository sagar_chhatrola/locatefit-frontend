import React, { Component } from "react";
import { Icon } from "antd";
import Estadisticasfrom from './formaestadistica';
import Visitas from './visitasform';

class Estadisticas extends Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>ESTADÍSTICAS</h3>
          <p>
            Hechemos número, aquí te dejamos todo los detalles de lo que has hecho en Locatefit.
          </p>
        </div>
        <div className="cont">
          <span className="subtitle">
            <Icon type="bar-chart" /> Estadística
          </span>

          <Estadisticasfrom />
  
        </div>
      </div>
    );
  }
}

export default Estadisticas;
