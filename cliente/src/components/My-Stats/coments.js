import React from "react";
import "antd/dist/antd.css";
import { Comment, Pagination, Tooltip, Avatar, Rate, Icon, Empty} from "antd";
import moment from "moment";
import "./My-Stats.css"
import { LOCAL_API_URL } from './../../config';
import 'moment/locale/es';


class Coments extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data ? this.props.data : []
    }

    console.log('this.state: ', this.state)

  }

  updateCommentState = (e, item, commentState) => {
    e.preventDefault();

    this.setState(state => {
      let data = state.data;
      data.map(el => {
        if (el.id === item.id) {
          el.state = commentState;
          if (!el.likes) el.likes = 0;
          if (!el.likes) el.dislikes = 0;

          if (commentState === 'liked') el.likes++;
          if (commentState === 'disliked') el.dislikes++;
        }
      });
      return data;
    });

  };

  render() {

    const actions = item => [
      <span key="comment-basic-like">
        <Tooltip title="Útil">
          <Icon
            type="like"
            theme={item.state === 'liked' ? 'filled' : 'outlined'}
            onClick={e => this.updateCommentState(e, item, 'liked')}
          />
        </Tooltip>
        <span style={{ paddingLeft: 8, cursor: 'auto' }}>{item.likes ? item.likes : 0}</span>
      </span>,
      <span key=' key="comment-basic-dislike"'>
        <Tooltip title="No útil">
          <Icon
            type="dislike"
            theme={item.state === 'disliked' ? 'filled' : 'outlined'}
            onClick={e => this.updateCommentState(e, item, 'disliked')}
          />
        </Tooltip>
        <span style={{ paddingLeft: 8, cursor: 'auto' }}>{item.dislikes ? item.dislikes : 0}</span>
      </span>,
      <span key="comment-basic-reply-to">¿Te ha resultado útil?</span>,
    ];

    return (
      <div style={{ marginBottom: 50 }}>
      { this.state.data.length > 0 ?
      <div>
        {
          this.state.data.map(
            (item) => (
              <Comment className='valor'
                actions={actions(item)}
                author={<p>{item.customer.nombre} {item.customer.apellidos}</p>}
                avatar={
                  <Avatar
                    src={LOCAL_API_URL + "/assets/images/" + item.customer.foto_del_perfil}
                    alt="Leudy"
                  />
                }
                content={

                  <p>{item.coment}</p>
                }
                datetime={
                  <Tooltip title={() => new Date(Number(item.updated_at)).toLocaleDateString()}>
                    <div>
                      <span className="inFo">{moment(Number(item.updated_at)).fromNow()}</span>
                      <Rate disabled value={Number(item.rate)} />
                    </div>
                  </Tooltip>
                }
              />
              
              
            ))
        }
      
        <Pagination className="paginacion" defaultCurrent={10} total={this.state.data.length} />
        </div>: null
      }
      {(this.state.data.length === 0 || !this.state.data) ?
        <div style={{textAlign: 'center', justifyContent: 'center'}}>
        <Empty
        image='https://user-images.githubusercontent.com/507615/54591670-ac0a0180-4a65-11e9-846c-e55ffce0fe7b.png'
        imageStyle={{
          height: 60
        }}
        description={
          <span>
            <h3>
              <Icon type='star' theme='twoTone' />
            </h3>
            Aún no te han dejado valoración.
           </span>
        }
      ></Empty>
        </div>
        : null
      }
      </div>
    );
  }
}

export default Coments;
