import React, { Component } from "react";
import { Query } from 'react-apollo';
import { PROFESSIONAL_RATING_QUERY } from "../../queries";
import Check from "./../../assets/images/corregir.svg";
import Coments from "./coments";
import "antd/dist/antd.css";
import { Pagination, Icon } from "antd";

import "./My-Stats.css";

class MyStats extends Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="card-title">
          <h3>OPINIONES DE LOS CLIENTES</h3>
          <p>Aquí tiene todas las opiniones de los clientes hacia ti.</p>

          <div className="cont">
            <span className="subtitle">
              <Icon type="star" /> Valoraciones de los clientes
          </span>
            <div className="startverify">
              <div className="startverifychek">
                <img height="50" src={Check} alt="" />
              </div>
              <div className="startverifytittle">
                <h7>Opiniones 100% Verificadas </h7>
                <p className="verified">
                  Todas las opiniones pertenecen a clientes que han contratado a
                  este Profesional.
                </p>
              </div>
            </div>
            <Query query={PROFESSIONAL_RATING_QUERY}>
              {({ data }) => {
                if (data && data.getProfessionalRating && data.getProfessionalRating.success) {
                  return <Coments data={data.getProfessionalRating.list} />
                } else {
                  return '';
                }
              }}
            </Query>
          </div>
        </div>
      </div>
    );
  }
}

export default MyStats;