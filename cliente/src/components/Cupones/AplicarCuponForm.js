import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button } from 'antd';
import { Mutation } from 'react-apollo';
import { CREAR_MODIFICAR_ORDEN } from '../../mutations';
import _get from 'lodash.get';

class AplicarCuponForm extends Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = async (e, mutacion, refetch = () => {}) => {
    try {
      e.preventDefault();
      const {
        setOrder = () => {
          console.log('no setOrder prop');
        },
        order
      } = this.props;
      this.props.form.validateFields(async (err, values) => {
        if (err) {
          console.log('AplicarCuponForm -> handleSubmit -> error 1', err);
        } else {
          const input = { ...order, ...values };
          console.log('mi input', input)
          const orderId = _get(this.props, 'match.params.orden', null);
          if (!input.id && orderId) {
            input.id = orderId;
          }
          if (input.direccion && input.direccion.id) {
            input.direccion = input.direccion.id;
          }
          if (input.cliente) {
            delete input.cliente;
          }
          if (input.profesional) {
            delete input.profesional;
          }
          if (input.descuento || input.descuento === null) {
            delete input.descuento;
          }
          const { data } = await mutacion({ variables: { input } });
          if (data) {
            console.log({ data });
            if (data.crearModificarOrden) {
              setOrder({ ...data.crearModificarOrden });
            }
            //message.success(res.crearUsuarioDireccion.message);
            refetch();
            //this.props.form.resetFields();
          }
          //message.error(res.crearUsuarioDireccion.message);
        }
      });
    } catch (error) {
      console.log('AplicarCuponForm -> handleSubmit -> error 2', error);
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { disabled } = this.props;

    return (
      <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
        {(crearModificarOrden, { loading, error, data, refetch }) => (
          <Form
            layout='inline'
            onSubmit={e => this.handleSubmit(e, crearModificarOrden, refetch)}
          >
            <Form.Item label='Cúpon de descuento'>
              {getFieldDecorator(
                'clave',
                {}
              )(<Input placeholder='Aplicar cúpon' disabled={disabled} />)}
            </Form.Item>
            <Form.Item>
              <Button disabled={disabled} type='primary' htmlType='submit'>
                Aplicar cúpon
              </Button>
            </Form.Item>
          </Form>
        )}
      </Mutation>
    );
  }
}

const WrappedAplicarCuponForm = Form.create({ name: 'aplicar_cupon_orden' })(
  AplicarCuponForm
);

export default WrappedAplicarCuponForm;
