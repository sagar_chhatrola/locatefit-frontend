import React from "react";
import { Route, Redirect } from "react-router-dom";
/* import { connect } from "react-redux"; */
// import PropTypes from "prop-types";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (localStorage.getItem('token')) ? (
            <Component {...props} 
            favouriteDeletedFromWishListCounter={rest.favouriteDeletedFromWishListCounter} 
            onFavouriteAdd={rest.onFavouriteAdd}
            />
      ) : (
       <Redirect 
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default PrivateRoute;
