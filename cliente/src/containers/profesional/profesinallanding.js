import React, { Component } from "react";
import Footer from "./../../components/layout/Footer";
import "./Profesionallanding.css";
import { Input, Avatar } from "antd";
import { Icon } from 'antd';
import Register from "./regiterformprofesionl"
import coverpageImg from "./../../assets/images/cover_page/chica.png";



class Profesionallanding extends Component {


    render() {
        return (
            <div style={{ overflow: "hidden", width: "100%" }}>
             <section className="pagelogin11">
                <section className="banner1">


                    <div className="Comofuncional1">

                        <div className="itemS1">

                            <div className="SuBitems1">
                                <Icon className="ICOn" type="euro" />
                                <h3>ALTOS INGRESOS</h3>
                                <p>Gane hasta €20/hora,  trabaja como autónomo. Nuestros mejores profesionales ganan más de 1,000 € por semana.</p>

                            </div>

                            <div className="SuBitems1">
                                <Icon className="ICOn" type="calendar" />
                                <h3>HORARIO FLEXIBLE</h3>
                                <p>Tú eliges cuándo quieres trabajar y cuánto. Cree un cronograma completo de sus clientes o simplemente agregue algunos trabajos adicionales.</p>


                            </div>


                            <div className="SuBitems1">
                                <Icon className="ICOn" type="credit-card" />
                                <h3>PAGOS SENCILLOS</h3>
                                <p>No más seguimiento de sus clientes para pagos. Sus pagos se depositan directamente en su cuenta bancaria poco después de completar el trabajo.</p>

                            </div>
                        </div>
                    </div>

                </section>
                <div className="wave wave1"></div>
                    <div className="wave wave2"></div>
                    <div className="wave wave3"></div>
                    <div className="wave wave4"></div>  
             </section>
                    <div className="comentariopro">
                        <Icon className="icco" type="message" />
                        <div className="line">

                        </div>

                        <div className="loque">
                            <h4>Lo que dicen nuestros profesionales</h4>
                            <p>Me mudé de una agencia a Locatefit el mes pasado. ¡Pagan mejor, tienen muchos trabajos y me encanta la flexibilidad!</p>
                            <Avatar size={64} className="Avatarprof" src="https://www.webconsultas.com/sites/default/files/styles/wc_adaptive_image__small/public/articulos/perfil-resilencia.jpg" />
                            <h5>Alejandra Martinez</h5>
                            <p>Burgos, España</p>
                        </div>

                        <div className="loque">

                            <p>Lo que realmente me gusta es que puedo tomar un par de trabajos al día, y no tengo que lidiar con el dolor de cabeza de encontrar mis propios clientes y lidiar con los pagos.</p>
                            <Avatar size={64} className="Avatarprof" src="https://www.trecebits.com/wp-content/uploads/2019/02/Persona-1-445x445.jpg" />
                            <h5>Javier Hermandez</h5>
                            <p>Madrid, España</p>
                        </div>

                        <div className="loque">

                            <p>En Locatefit tengo la facilidad de trabajar cuándo yo quiero, por lo que valoro mucho la flexibilidad de horario que puedo elegir.</p>
                            <Avatar size={64} className="Avatarprof" src="https://www.sylvain-renard.com/wp-content/uploads/2017/05/SR20160129-623.jpg" />
                            <h5>Pedro Francisco</h5>
                            <p>Barcelona, España</p>
                        </div>

                    </div>

                    


                    <div className="comentariopro">
                        <Icon className="icco" type="file" />
                        <div className="line">

                        </div>

                        <div className="loque">
                            <h4>Requisitos</h4>
                            <ul>
                                <li>
                                    <p>
                                     Debe tener experiencia en el servicio que vas a brindar.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Debe estar autorizado para trabajar en el país en el que solicita.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Debe ser autónomo.
                                    </p>
                                </li>
                            </ul>
                        </div>

                        <p className="locat">Locatefit no es un empleador, sino que simplemente conecta profesionales de servicio independientes con clientes.</p>
                    </div>

                    <div className="profesi">
                    <div className="set">
                    <div className="cover-right">
                        <img
                            className="cover-right1"
                            width={"100%"}
                            src={coverpageImg}
                            alt=""
                        />
                    </div>


                    </div>

                    <div className="set">
                        <h1>
                            ¡Sé un profesional <br />
                            en Locatefit!
                        </h1>
                        <p>Obtenga acceso a cientos de trabajos en <br />su ciudad y cree su propio horario.</p>
                        <Register />
                    </div>
                    </div>




                    <Footer />
                </div>
                );
              }
            }
            
            
            
export default Profesionallanding;