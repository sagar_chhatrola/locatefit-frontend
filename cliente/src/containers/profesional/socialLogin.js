import React, { Component } from 'react';
import './socialLogin.css';
import { Icon } from 'antd';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import {
  FacebookLoginButton,
  GoogleLoginButton,
  TwitterLoginButton
} from 'react-social-login-buttons';
import TwitterLogin from 'react-twitter-auth';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { GoogleLogin } from 'react-google-login';
import { FACEBOOK_APP_ID, GOOGLE_CLIENT_ID, LOCAL_API_URL } from '../../config';


class ScocialLogin extends Component {
  state = {
    isAuthenticated: false,
    user: null,
    token: ''
  };

  onFailure = error => {
    console.log(error);
  };

  twitterResponse = response => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {
      console.log(user.token);

      if (user.token) {
        this.props.logon(user.nuevoUsuario.email, user.token);
      }
    });
  };

  facebookResponse = response => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: 'application/json' }
    );
    const options = {
      method: 'POST',
      body: tokenBlob,
      mode: 'cors',
      cache: 'default'
    };
    fetch( LOCAL_API_URL + '/api/v1/auth/facebook', options).then(r => {
      const token = r.headers.get('x-auth-token');
      r.json().then(async user => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  googleResponse = response => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: 'application/json' }
    );
    const options = {
      method: 'POST',
      body: tokenBlob,
      mode: 'cors',
      cache: 'default'
    };
    fetch( LOCAL_API_URL + '/api/v1/auth/google', options).then(r => {
      const token = r.headers.get('x-auth-token');
      r.json().then(user => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  render() {
    if (this.state.isAuthenticated) {
      return <Redirect to='/profile' />;
    } else
      return (
        <div className='contLogin1'>
          <p>Tambien puedes continuar usando tus redes sociales</p>
          <TwitterLogin
            loginUrl={LOCAL_API_URL + '/api/v1/auth/twitter'}
            onFailure={this.onFailure}
            onSuccess={this.twitterResponse}
            requestTokenUrl={ LOCAL_API_URL + '/api/v1/auth/twitter/reverse'}
            // customHeaders={customHeader}
            style={{ background: 'transparent', border: 'none', width: '100%' }}
          >
            <div className='socia'>
            <TwitterLoginButton>
                <span>Continúa con Twitter</span>
            </TwitterLoginButton>
            </div>
          </TwitterLogin>
          <FacebookLogin
            appId={FACEBOOK_APP_ID}
            autoLoad={false}
            fields='name,email,picture'
            callback={this.facebookResponse}
            render={renderProps => (
              <div className='socia' onClick={renderProps.onClick}>
                <FacebookLoginButton>
                  <span>Continúa con Facebook</span>
                </FacebookLoginButton>
              </div>
            )}
          />
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText='Login'
            onSuccess={this.googleResponse}
            onFailure={this.onFailure}
            render={renderProps => (
              <div
                className='socia'
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                <GoogleLoginButton>
                  <span>Continua con Google</span>
                </GoogleLoginButton>
              </div>
            )}
          />
        </div>
      );
  }
}

export default ScocialLogin;
