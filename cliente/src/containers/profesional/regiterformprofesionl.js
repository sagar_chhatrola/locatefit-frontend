import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { NUEVO_USUARIO } from '../../mutations';
import { withRouter } from 'react-router-dom';
import './Profesionallanding.css';
import SocialLogin from './socialLogin';
import LoginForm from '../Login/loginform';
import { Form, Input, message, Select, Checkbox, Button, Spin } from 'antd';

const { Option } = Select;

class RegistrationForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    email: 'leudy123456@gmail.com',
    nombre: 'Leudy Martes',
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('¡Las dos contraseñas que ingresaste son inconsistentes!');
    } else {
      callback();
    }
  };

  validateToNextPassword = async (rule, value) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }

    let passwordRegex = new RegExp('^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,})');
    if (!passwordRegex.test(value)) {
      throw new Error(
        'Caracteres de la contraseña debe tener mínimo 8 (números y letras)'
      );
    }
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(
        domain => `${value}${domain}`
      );
    }
    this.setState({ autoCompleteResult });
  };

  handleSubmit(e, crearUsuario) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {
          usuario,
          password,
          email,
          telefono,
          nombre,
          apellidos
        } = values;

        const input = {
          usuario,
          password,
          email,
          telefono,
          nombre,
          apellidos
        };
        console.log('input in handleSubmit: ', input);

        crearUsuario({ variables: { input } }).then(async ({ data }) => {
          if (data && data.crearUsuario && data.crearUsuario.success) {
            message.success(data.crearUsuario.message);
            this.props.history.push('/confirmatuemail');
          } else if (data && data.crearUsuario && !data.crearUsuario.success)
            message.error(data.crearUsuario.message);
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '34'
    })(
      <Select style={{ width: 100 }}>
        <Option value='34'>+34</Option>
      </Select>
    );

    return (
      <Mutation mutation={NUEVO_USUARIO}>
        {(crearUsuario, { loading, error, data }) => {
          if (loading) {
            return (
              <div className='page-loader'>
                <Spin size='large'></Spin>
              </div>
            );
          }

          return (
            <>
              <Form
                {...formItemLayout}
                onSubmit={e => this.handleSubmit(e, crearUsuario)}
              >
                {error ? (
                  <Alert
                    message='Error'
                    description='El usuario ya existe en el sistema'
                    type='error'
                    showIcon
                  />
                ) : (
                  ''
                )}

                <Form.Item>
                  {getFieldDecorator('nombre', {
                    rules: [
                      {
                        required: true,
                        message: 'Por favor ingrese su nombre!',
                        whitespace: true
                      }
                    ]
                  })(<Input placeholder='Nombre' />)}
                </Form.Item>

                <Form.Item>
                  {getFieldDecorator('apellidos', {
                    rules: [
                      {
                        required: true,
                        message: 'Por favor ingrese sus apellidos!',
                        whitespace: true
                      }
                    ]
                  })(<Input placeholder='Apellidos' />)}
                </Form.Item>

                <Form.Item>
                {getFieldDecorator('usuario', {
                  rules: [
                    {
                      required: true,
                      message: 'Por favor ingrese un usuario!',
                      whitespace: true
                    }
                  ]
                })(<Input placeholder='Nombre de usuario' />)}
              </Form.Item>

                <Form.Item>
                  {getFieldDecorator('email', {
                    rules: [
                      {
                        type: 'email',
                        message: 'La entrada del E-mail no es válida !'
                      },
                      {
                        required: true,
                        message: 'Por favor ingrese su correo electrónico!'
                      }
                    ]
                  })(<Input placeholder='Correo electrónico' />)}
                </Form.Item>
                <Form.Item hasFeedback>
                  {getFieldDecorator('password', {
                    rules: [
                      {
                        required: true,
                        message: 'Por favor ingrese su contraseña!'
                      },
                      {
                        validator: this.validateToNextPassword
                      }
                    ]
                  })(<Input.Password placeholder='Contraseña' />)}
                </Form.Item>
                <Form.Item hasFeedback>
                  {getFieldDecorator('confirm', {
                    rules: [
                      {
                        required: true,
                        message: '¡Por favor, confirme su contraseña!'
                      },
                      {
                        validator: this.compareToFirstPassword
                      }
                    ]
                  })(
                    <Input.Password
                      placeholder='Confirme su contraseña'
                      onBlur={this.handleConfirmBlur}
                    />
                  )}
                </Form.Item>

                <Form.Item>
                  {getFieldDecorator('telefono', {
                    rules: [
                      {
                        required: false,
                        message: 'Por favor ingrese su número de teléfono!'
                      }
                    ]
                  })(
                    <Input
                      placeholder='Número móvil'
                      addonBefore={prefixSelector}
                      style={{ width: '100%' }}
                    />
                  )}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                  {getFieldDecorator('agreement', {
                    valuePropName: 'checked',
                    rules: [
                      {
                        required: true,
                        message:
                          'Antes de continuar debes aceptar nuestra politica de provacidad y condiciones de uso.!'
                      }
                    ]
                  })(
                    <Checkbox>
                      Registrándote aceptas nuestras
                      <a
                        href='https://about.locatefit.es/condiciones-de-uso'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        {' '}
                        Condiciones de uso
                      </a>{' '}
                      y{' '}
                      <a
                        href='https://about.locatefit.es/privacidad'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        {' '}
                        Políticas de privacidad
                      </a>
                    </Checkbox>
                  )}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                  <Button type='primary' htmlType='submit'>
                    Crear cuenta
                  </Button>
                  <br />
                  ¿Ya tienes una cuenta? <a href='/login'>Inicia seción</a>
                </Form.Item>
                {/* <SocialLogin /> */}
              </Form>
              <div className='contLogin1'>
              <LoginForm refetch={this.props.refetch} fromSignup={true} />
              </div>
            </>
          );
        }}
      </Mutation>
    );
  }
}

const WrappedRegistrationForm = Form.create({ name: 'register' })(
  RegistrationForm
);

export default withRouter(WrappedRegistrationForm);
