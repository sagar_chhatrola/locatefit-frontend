import React, { Component } from "react";
import { Link } from 'react-router-dom';
import "./all-product.css";
import Footer from "./../../components/layout/Footer";
import ProductList from "../product/product-list";
import { Query } from 'react-apollo';
import { CATEGORIES_QUERY, CATEGORY_QUERY } from '../../queries';
import Category from '../Landing/Categories'
import Map from "./maps"
import {
  Breadcrumb,
  Select,
  Slider,
  Row,
  Col,
  Rate,
  Radio,
  Icon,
  Menu,
  Dropdown,
  Result,
  Button,
  Spin,
  Switch,
  message,
  Pagination,
  Drawer,
  Checkbox
} from "antd";
import GoogleMapReact from "google-map-react";

const { Option } = Select;

function onChange(value) {
  console.log(`selected ${value}`);
}

function onBlur() {
  console.log("blur");
}

function onFocus() {
  console.log("focus");
}

function onSearch(val) {
  console.log("search:", val);
}

const content = <Rate allowHalf />;

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Allproduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      categoryId: this.props.match.params.id,
      visible: false,
      price: null,
      geoLocationEnabled: false,
      city: null,
      latitude: null,
      longitude: null,
      locationFilterChecked: true
    };

    this.getGeoLocation();

  }

  refetch = null;

  

handleChange = (value) =>{
  this.setState({
    categoryId: value,
  });
  console.log('selected', value)
}

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };


  componentWillReceiveProps(props) {
    if (this.refetch && props.favouriteDeletedFromWishListCounter) this.refetch();
  }

  getGeoLocation() {
    if ("geolocation" in navigator) {

      navigator.geolocation.getCurrentPosition(
        (position) => {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;

          this.setState({ latitude });
          this.setState({ longitude });

          let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
          fetch(apiUrlWithParams)
            .then(response => response.json())
            .then(data => {

              let cityFound = false;

              for (let index = 0; index < data.results.length; index++) {

                for (let i = 0; i < data.results[index].address_components.length; i++) {

                  if (data.results[index].address_components[i].types.includes('locality')) {

                    this.setState({ city: data.results[index].address_components[i].long_name });
                    this.setState({ geoLocationEnabled: true });
                    cityFound = true;

                    let input = { id: this.state.categoryId, city: this.state.city };
                    this.refetch(input)

                  }

                  if (cityFound) break;
                }

                if (cityFound) break;
              }

            })
            .catch(error => {
              this.setState({ locationFilterChecked: false });
              console.log('error in google geocode api: ', error);

            })

        },
        (error) => console.log('geolocation error', error),


      );
    } else {
      console.log('this browser not supported HTML5 geolocation API')
    }
  }

  onLocationFilterChange = checked => {

    if (!this.state.geoLocationEnabled) {
      message.error('Habilite amablemente el servicio de ubicación para filtrar productos con la ubicación.');
    } else {

      this.setState({ locationFilterChecked: checked });

      let input = { id: this.state.categoryId, price: this.state.price };

      if (checked) input.city = this.state.city;
      else input.city = '';

      this.refetch(input);
    }

  }

  onChange = e => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  };


  onPriceChange = (value) => {
    this.setState({ price: value });
  };

  filterByPrice = (e, refetch) => {

    e.preventDefault();
    let input = { id: this.state.categoryId, price: this.state.price }
    if (this.state.locationFilterChecked) input.city = this.state.city;
    refetch(input);
  };

  onCategoryChange({ key }, refetch) {
    if (key) {
      this.setState({
        categoryId: key,
      });
      refetch({ id: key });
    }
  }

  render() {
    let categorialink = "/services/"
    return (
      <Query query={CATEGORY_QUERY} variables={{ id: this.state.categoryId, city: this.state.city }}>
        {({ loading, error, data: res, refetch }) => {
          this.refetch = refetch;
          if (loading) {
            return (
              <div className="page-loader">
                <Spin size="large"></Spin>
              </div>
            );
          }
          if (error) console.log('loading category error: ', error);

          return (

            <div className="barber-shop-product">
              <div className="filters-container">
                
                <div className='section_Ser'>
                  <div className="Sec_title">
                    {res && res.getCategory ?
                      (<div>
                        <div>
                          <h3 >{res.getCategory.data.title}
                            {this.state.geoLocationEnabled ? <span className="reul"> en {this.state.city}</span> : ''}
                          </h3>
                        </div>
                        <p className="titlebus">{res.getCategory.data.productos.length}, resultado de la busqueda</p>
                      </div>)
                      : null}
                  </div>
                  <div className="Sec_migas">
                  <Breadcrumb>
                    <Breadcrumb.Item>
                      <a href="/">Inicio</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                      <a href="/category">Categoría</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>{res.getCategory.data.title}</Breadcrumb.Item>
                  </Breadcrumb>
                </div>
                </div>
              </div>

              <div className="section_Ser">
                <div className="filter_section">
                  <div className="resul_from">
                    <Icon style={{ marginTop: 17, marginLeft: 15, marginRight: 10, color: "#95ca3e" }} type="search" />
                    <p>Resultados en {this.state.city}</p>
                  </div>
                  <div className="maps">
                    <Map lat={this.state.latitude} lng={this.state.longitude} city={this.state.city} />
                  </div>
                  <div className="filt_location">
                    <div>
                      <span className="reul">Activar ubicación </span>
                      <Switch style={{ marginLeft: '88%', marginTop: -50, backgroundColor: '#95ca3e'}} checked={this.state.locationFilterChecked} onChange={this.onLocationFilterChange} />
                    </div>
                  </div>
                  <div className="sec_price">
                    <div style={{ marginBottom: 20 }}>
                      <span className="reul">Filtrar por precio</span>
                      <Slider
                        min={1}
                        max={300}
                        onChange={this.onPriceChange}
                        value={typeof this.state.price === 'number' ? this.state.price : 300}
                      />
                      <Button type='dashed' onClick={e => this.filterByPrice(e, refetch)}>Aplicar</Button>
                    </div>
                  </div>
                  <div className="Sec_category">
                    <div>
                      <span className="reul">Selecciona una categoría</span>
                      <Query query={CATEGORIES_QUERY}>
                        {({ loading, error, data, refetch }) => {
                          if (loading) return (
                            <div className="page-loader">
                              <Spin size="large"></Spin>
                            </div>
                          );
                          if (error) return console.log(error);
                          return <div className='ulca'>
                            <ul>
                              {
                                data.getCategories.map(
                                  (cat, i) => (
                                    <li key={i}>
                                      <Checkbox><a href={categorialink + cat.id}>{cat.title} ({cat.productos.length})</a></Checkbox>

                                    </li>
                                  )
                                )
                              }
                            </ul>

                          </div>

                        }}
                      </Query>

                    </div>
                  </div>
                </div>

                <div className="Services_section">

                <div className="Orde_sec">
                <div style={{marginTop: 10, marginLeft: 10}}>
                <Select defaultValue="Ordenar por" style={{ width: 120 }} onChange={this.handleChange}>
                  <Option value="new">Más nuevo</Option>
                  <Option value="antiguo">Más antiguo</Option>
                  <Option value="relevcantes">Más relevantes</Option>
                  <Option value="contratado">Más contratado</Option>
                </Select>
                </div>
                <div style={{marginTop: 10, marginLeft: 20}}>
                <Query query={CATEGORIES_QUERY}>
                {({ loading, error, data, refetch }) => {
                  if (loading) return (
                    <div className="page-loader">
                      <Spin size="large"></Spin>
                    </div>
                  );
                  if (error) return console.log(error);
                  return <div>
                  <Select defaultValue={res.getCategory.data.title} style={{ width: 140 }} onChange={this.handleChange}>
                      {
                        data.getCategories.map(
                          (cat, i) => (
                              <Option key={i} value={cat.id}>{cat.title}</Option>
                          )
                        )
                      }
                    </Select>

                  </div>

                }}
              </Query>
                </div>
                <div style={{marginTop: 10, marginLeft: 20}}>
                <Select defaultValue="Valoración" style={{ width: 120 }} onChange={this.handleChange}>
                  <Option value="valorado">Mejor valorado</Option>
                  <Option value="valoraciones">Más valoraciones</Option>
                  <Option value="rating">Mayor rating</Option>
                </Select>
                </div>
                <div className="Sec_orien">
                  <div className="Sec_border">
                    <Icon type="pic-left" style={{fontSize: 18, paddingLeft: 5}}/>
                  </div>
                  <div className="Sec_border">
                   <Icon type="pic-center" style={{fontSize: 18, paddingLeft: 5}}/>
                  </div>
                  <div className="Sec_border">
                   <Icon type="pic-right" style={{fontSize: 18, paddingLeft: 5}}/>
                  </div>
                </div>
                </div>
                  {/* Display products section starts */}
                  {res && res.getCategory && res.getCategory.data.productos.length > 0 ?
                    <ProductList session={this.props.session} data={res.getCategory.data.productos} />
                    : null
                  }
                  {/* Display products section end */}

                  {/* Dispaly no products section starts */}
                  {res && res.getCategory && (res.getCategory.data.productos.length === 0 || !res.getCategory.data.productos) ?
                    <div className="no-products">
                      <Result
                        status="404"
                        title="Aún no tenemos profesionales para esta categoría"
                        subTitle="No se encontraron servicios para esta categoría."
                        extra={<Button href="/" type="primary">Volver al inico</Button>}
                      />
                    </div>
                    : null
                  }
                  {/* Dispaly no products section end */}

                  <div className="pagination">
                    {res && res.getCategory && res.getCategory.data.productos.length > 0 ?
                      <Pagination className="paginacion" defaultCurrent={40} total={res && res.getCategory && res.getCategory.data.productos.length} />
                      : null
                    }
                  </div>
                </div>
              </div>

              <Footer />
            </div>


          )

        }}
      </Query>
    );
  }
}

export default Allproduct;


