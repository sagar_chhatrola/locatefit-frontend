import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import './all-product.css'
const mapObj = {
  center: {lat: null,  lng: null},
  city: ''
};


class SimpleMap extends Component {

  constructor(props){
    super(props);

    mapObj.center.lat = this.props.lat;
    mapObj.center.lng = this.props.lng;
    mapObj.city = this.props.city;

  }

  static defaultProps = {
    zoom: 13
  };

  renderMarkers(map, maps) {
    let marker = new maps.Marker({
      position: mapObj.center,
      map,
      title: mapObj.city
    });
  }

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: "100%", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg" }}
          defaultCenter={mapObj.center}
          defaultZoom={this.props.zoom}
          onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
        >
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;
