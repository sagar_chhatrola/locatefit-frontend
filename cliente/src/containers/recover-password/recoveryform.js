import React, { Component } from 'react';
import { Form, Icon, Input, Button } from 'antd';
import './recover-password.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { LOCAL_API_URL } from './../../config';


class Formulario extends Component {
  state = {
    redirect: false,
    noEmail: false,
    message: ''
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);
        const url = LOCAL_API_URL + '/forgotpassword';
        axios
          .post(url, values)
          .then(res => {
            if (res.data.noEmail) {
              this.setState({
                noEmail: res.data.noEmail,
                message: res.data.message
              });
            } else {
              this.setState({
                redirect: true
              });
            }
          })
          .catch(err => {
            console.log('error:', err);
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    if (this.state.redirect) {
      return <Redirect to='/login' />;
    } else
      return (
        <Form onSubmit={this.handleSubmit} className='login-form'>
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [
                { required: true, message: 'Por favor ingrese su Email!' }
              ]
            })(
              <Input
                prefix={
                  <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                onChange={() => {
                  this.setState({
                    noEmail: false,
                    message: ''
                  });
                }}
                placeholder='Correo electrónico'
              />
            )}
            {this.state.noEmail && (
              <p
                style={{
                  margin: '0px',
                  color: 'red'
                }}
              >
                {this.state.message}
              </p>
            )}
          </Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            className='login-form-button'
          >
            Enviadme el enlace
          </Button>
          <br />

          <div className='forgot'>
            ¿No tienes una cuenta? <a href='/register'>Registrate ahora!</a>
          </div>
        </Form>
      );
  }
}

const WrappedDemo = Form.create({ name: 'validate_other' })(Formulario);

export default WrappedDemo;
