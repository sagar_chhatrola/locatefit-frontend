import React, { Component } from 'react';
import './recover-password.css';
import Recoveryfomr from './recoveryform';
import Footer from './../../components/layout/Footer';

class Recoverpassword extends Component {
  render() {
    return (
      <div className="register-page-content">
        <div className="containerLogin">
          <div className="contLogin">
            <h3>Recuperar contraseña</h3>
            <p>
              Introduce tu dirección de correo electrónico y te enviaremos un
              enlace para restablecer tu contraseña.
            </p>

            <Recoveryfomr />
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
export default Recoverpassword;
