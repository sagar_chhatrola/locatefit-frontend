import React, { Component } from 'react';
import './recover-password.css';
import Recoveryfomr from './recoveryform';
import Footer from '../../components/layout/Footer';
import Forgot from '../../components/Account-Detail/forgot';
import axios from 'axios';
import { LOCAL_API_URL } from './../../config';

class Forgotpublic extends Component {
  state = {
    email: '',
    isValid: false,
    set: false
  };

  componentDidMount = () => {
    // console.log(this.props.match.params.token);
    const url = LOCAL_API_URL + '/tokenValidation';
    axios
      .post(url, this.props.match.params)
      .then(res => {
        // console.log(res.data);
        this.setState({
          isValid: res.data.isValid,
          email: res.data.email,
          set: true
        });
      })
      .catch(err => {
        console.log('err:', err);
      });
  };
  render() {
    return (
      <div className="register-page-content">
        <div className="containerLogin">
          <div className="contLogin">
            {this.state.isValid && (
              <Forgot
                email={this.state.email}
                token={this.props.match.params.token}
              />
            )}
            {!this.state.isValid && this.state.set && (
              <p>Invalid reset password token.</p>
            )}
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
export default Forgotpublic;
