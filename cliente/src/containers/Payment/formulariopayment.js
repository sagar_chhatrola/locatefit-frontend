import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Steps, Button, message, Icon } from 'antd';
import './Payment.css';
import Direccion from './../../components/Address/Formulario';
import Pasarela from './metododepago';
import Confirmarpedido from './confirmarpedido';
import { Mutation } from 'react-apollo';
import { CREAR_MODIFICAR_ORDEN } from '../../mutations';
import _get from 'lodash.get';

const { Step } = Steps;

class PaymentForm extends Component {
  state = {
    current: 0,
    order: {},
    direccion: {}
  };

  setOrder = order => this.setState({ order });

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  setDireccion = ({ direccion = {} }) => {
    console.log('setDireccion', { direccion });
    this.setState({ direccion });
  };

  eliminarDireccion = () => {
    console.log('eliminarDireccion');
    this.setState({ direccion: {} }, () => {
      console.log('formularioPayment -->', this.state);
    });
  };

  stepsContent = ({
    setOrder,
    order,
    setDireccion,
    direccion,
    eliminarDireccion,
    props,
    orderId
  }) => [
    <div className='paymentmetod-tittle'>
      <h4>Dirección donde quieres que vayamos</h4>
      <p>
        Por favor, rellena tu dirección de facturación antes de finalizar tu
        compra
      </p>
      <Direccion
        setDireccion={setDireccion}
        eliminarDireccion={eliminarDireccion}
        direccion={direccion}
        {...props}
      />
    </div>,
    <Confirmarpedido setOrder={setOrder} order={{ ...order }} {...props} />,
    <Pasarela {...props} orderId={orderId} />
  ];

  puedoIrAlSiguientePaso = () => {
    const { current, order, direccion } = this.state;
    switch (current) {
      case 0:
        if (direccion && direccion.id) return true;
        else if (order && order.direccion && order.direccion.id) return true;
        else return false;
      case 1:
        if (order && order.aceptaTerminos) return true;
        return false;
      default:
        return true;
    }
  };

  guardarOrden = async (_e, mutacion, refetch = () => {}) => {
    try {
      const input = { ...this.state.order };
      const orderId = _get(this.props, 'match.params.orden', null);
      if (!input.id && orderId) {
        input.id = orderId;
      }
      if (input.direccion && input.direccion.id) {
        input.direccion = input.direccion.id;
      }
      if (!input.direccion && this.state.direccion && this.state.direccion.id) {
        input.direccion = this.state.direccion.id;
      }
      if (input.cliente) {
        delete input.cliente;
      }
      if (input.profesional) {
        delete input.profesional;
      }
      if (input.descuento || input.descuento === null) {
        delete input.descuento;
      }
      console.log({ input });
      const { data } = await mutacion({ variables: { input } });
      if (data) {
        console.log({ data });
        if (data.crearModificarOrden) {
          this.setOrder({ ...data.crearModificarOrden });
          this.next();
        }
        refetch();
      }
    } catch (error) {
      console.log('PaymentForm -> guardarOrden -> error', error);
    }
  };

  render() {
    const { current, order, direccion } = this.state;
    const orderId = _get(this.props, 'match.params.orden', null);
    const steps = this.stepsContent({
      setOrder: this.setOrder,
      order,
      setDireccion: this.setDireccion,
      direccion,
      eliminarDireccion: this.eliminarDireccion,
      props: this.props,
      orderId
    });
    return (
      <div>
        <Steps current={current}>
          <Step title='Dirección' />
          <Step title='Confirmar Pedido' />
          <Step title='Método de pago' />
        </Steps>
        <div className='steps-content'>{steps[current]}</div>
        <div className='steps-action'>
          {current < steps.length - 1 && (
            <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
              {(crearModificarOrden, { loading, error, data, refetch }) => (
                <Button
                  type='primary'
                  disabled={!this.puedoIrAlSiguientePaso()}
                  onClick={e => {
                    switch (current) {
                      case 0:
                        this.guardarOrden(e, crearModificarOrden, refetch);
                        break;
                      case 1:
                        this.guardarOrden(e, crearModificarOrden, refetch);
                        break;
                      default:
                        this.next();
                    }
                  }}
                >
                  Próximo paso
                </Button>
              )}
            </Mutation>
          )}
          {current === steps.length - 1 && (
           <div>

           </div>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Paso anterior
            </Button>
          )}
        </div>
      </div>
    );
  }
}

export default PaymentForm;
