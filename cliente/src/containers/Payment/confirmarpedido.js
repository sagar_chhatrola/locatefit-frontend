import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Card, Radio, Form, Icon } from 'antd';
import './Payment.css';
import Productpayment from './productpayment';
import TextArea from 'antd/lib/input/TextArea';
import Detallesprecio from './detallesdeprecio';
import { Mutation } from 'react-apollo';
import { CREAR_MODIFICAR_ORDEN } from '../../mutations';
import AplicarCuponForm from '../../components/Cupones/AplicarCuponForm';
import { GET_ORDEN, PRODUCT_QUERY } from '../../queries';
import { useQuery } from '@apollo/react-hooks';
import _get from 'lodash.get';

class Confirmarpedido extends Component {
  handleOrderChange = e => {
    let order = { ...this.props.order };
    const { setOrder } = this.props;
    order[e.target.name] =
      e.target.type === 'radio' ? e.target.checked : e.target.value;
    setOrder({ ...order });
  };

  render() {
    const { setOrder, order } = this.props;
    return (
      <div>
        <DatosOrden ordenId={order.id} descuento={order.descuento} />
        <Card style={{ marginTop: 16 }}>
          <p>¿Tienes un cúpon de descuento? Este es el momento de canjearlo</p>
          <div className='flex-row'>
            <AplicarCuponForm
              setOrder={setOrder}
              disabled={!!order && !!order.cupon}
              order={order}
            />
            {!!order && !!order.cupon && (
              <h4>
                {' '}
                <Icon
                  type='check-circle'
                  theme='twoTone'
                  twoToneColor='#52c41a'
                  className='cupon-icon'
                />{' '}
                Cúpon añadido con éxito
              </h4>
            )}
          </div>
        </Card>

        <Card style={{ marginTop: 16 }}>
          <h3>Descripción de la tarea</h3>
          <p>
            Añade la descripcion de la tarea a tu pedido para que el profesional tenga
            una idea más clara.
          </p>

          <Form layout='inline'>
            <TextArea
              placeholder='Descripción de la tarea'
              name='nota'
              onChange={this.handleOrderChange}
            />
          </Form>
        </Card>

        <Card style={{ marginTop: 16 }}>
          <p>
            Tus datos personales se utilizarán para procesar tu pedido, mejorar
            tu experiencia en esta web y otros propósitos descritos en nuestra
            <a href='https://about.locatefit.es/privacidad'>
              {' '}
              política de privacidad.
            </a>
          </p>

          <p>
            <label>
              <Radio name='aceptaTerminos' onChange={this.handleOrderChange}>
                He leído y estoy de acuerdo con los
                <a href='https://about.locatefit.es/condiciones-de-uso'>
                  {' '}
                  términos y condiciones
                </a>{' '}
                de la web
                <span className='aste'>*</span>
              </Radio>
            </label>
          </p>
        </Card>
      </div>
    );
  }
}

const DatosOrden = ({ ordenId = null, descuento = {} }) => {
  const {
    loading,
    error,
    data,
    refetch = () => console.log('DireccionExistente -> no refetch')
  } = useQuery(GET_ORDEN, {
    variables: { id: ordenId }
  });
  
  if (!ordenId) {
    return <div>No se encuentra la orden en la base de datos</div>;
  }
 
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  //const direccionId = _get(data, 'getUsuarioDireccion.data.id', null);
  if (data) console.log('DatosOrden -->', { data });
  const productoId = _get(data, 'getOrden.producto', null);
  const cantidad = _get(data, 'getOrden.cantidad', null);
  console.log('hey descuento', { descuento });
  return (
    <div>
      <DatosProducto
        productoId={productoId}
        cantidad={cantidad}
        descuento={descuento}
      />
    </div>
  );
};

const DatosProducto = ({ productoId = null, cantidad = 0, descuento = {} }) => {
  if (!productoId) {
    return <div>No se encuentra el producto en la base de datos</div>;
  }
  console.log({ descuento });
  const {
    loading,
    error,
    data,
    refetch = () => console.log('DireccionExistente -> no refetch')
  } = useQuery(PRODUCT_QUERY, {
    variables: { id: productoId }
  });
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  //const direccionId = _get(data, 'getUsuarioDireccion.data.id', null);
  if (data) console.log('DatosProducto -->', { data });
  const {
    id,
    title,
    creator,
    number,
    fileList: [image = null, ...rest]
  } = _get(data, 'getProducto.data', {});
  return (
    <div>
      <Productpayment
        productoId={id}
        title={title}
        creator={creator}
        image={image}
      />
      <Detallesprecio
        precioUnitario={number}
        cantidad={cantidad}
        descuento={descuento}
      />
    </div>
  );
};

export default Confirmarpedido;
