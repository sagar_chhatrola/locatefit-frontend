import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Tabs, Button } from 'antd';
import PayPalImg from './../../assets/images/PayPal-logo/PayPal-logo.png';
import MasterCardImg from './../../assets/images/Master-Card/Master-Card.png';
import PasarelaPayment from './pasarela';
import PayPalPasarela from './../../components/Payment-Type/paypal';

const { TabPane } = Tabs;

class Pasarela extends Component {

  render() {
    const { orderId } = this.props;
    return (

      <Tabs>
        <TabPane size='large' tab={<div><p>Pago con tarjeta</p> <img src={MasterCardImg} alt='Tarjeta de crédito' /></div>} key="1">
            <PasarelaPayment orderId={orderId} />
        </TabPane>
        <TabPane tab={<div><p>Pago con Paypal</p> <img style={{width: 50}} src={PayPalImg} alt='Paypal' /></div>} key="2">
          <PayPalPasarela orderId={orderId} />
        </TabPane>
      </Tabs>
      
    );
  }
}

export default Pasarela;
