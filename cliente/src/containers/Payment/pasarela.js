import React, { Component } from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import { Row, Icon, Checkbox, Tooltip } from "antd";
import './Payment.css'

const text = <span>Añadir como método de pago predeterminado</span>;

class App extends Component {
  render() {
    return (
      <div>
        <StripeProvider apiKey="pk_live_l2imKnAkvWpjXopC3A7Z1NnV002pWA8LAK">
          <div className="example">
            <Elements>
              <CheckoutForm orderId={this.props.orderId} />
            </Elements>
          </div>
        </StripeProvider>
      </div>

    );
  }
}

export default App;
