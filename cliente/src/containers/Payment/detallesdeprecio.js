import React, { Component } from 'react';
import './Payment.css';
import { Card, Icon, Descriptions } from 'antd';
import _get from 'lodash.get';

class RevisionRequest extends Component {
  render() {
    const { precioUnitario = 0, cantidad = 0, descuento = {} } = this.props;
    const valorDescuento = _get(descuento, 'descuento', null);
    const tipo = _get(descuento, 'tipo', null);

    const subtotal = precioUnitario * cantidad;
    let displayDescuento = `0 €`;
    let descuentoFinal = 0;
    let total = subtotal;
    if (valorDescuento && tipo) {
      switch (tipo) {
        case 'dinero':
          displayDescuento = `${valorDescuento} €`;
          total = subtotal - valorDescuento;
          break;
        case 'porcentaje':
          displayDescuento = `${valorDescuento} %`;
          descuentoFinal = valorDescuento / 100;
          total = subtotal - subtotal * descuentoFinal;
          break;
      }
    }
    return (
      <div>
        <Card style={{ marginTop: 16 }}>
          <Descriptions title='Detalles de la orden'>
            <Descriptions.Item label='Precion unitario'>
              {precioUnitario}€
            </Descriptions.Item>
            <Descriptions.Item label='Cantidad'>{cantidad}</Descriptions.Item>
            <Descriptions.Item label='Subtotal'>{subtotal}€</Descriptions.Item>
            <Descriptions.Item label='Descuento'>
              {displayDescuento}
            </Descriptions.Item>
            <Descriptions.Item label='Total'>{total} €</Descriptions.Item>
            <Descriptions.Item label='Total a pagar'>
              {total} €
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </div>
    );
  }
}

export default RevisionRequest;
