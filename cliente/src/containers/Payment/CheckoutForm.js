import React, {Component} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import { Redirect } from 'react-router-dom';
import { Button, Input } from 'antd'
import _get from 'lodash.get';
import './Payment.css'
import { LOCAL_API_URL } from './../../config';
import { withApollo } from 'react-apollo';
import { GET_FULL_ORDEN } from '../../queries';
import { CREATE_NOTIFICATION } from '../../mutations';

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {complete: false};
    this.submit = this.submit.bind(this);
  }


  async submit(ev) {
   
    this.props.client
      .query({
          query: GET_FULL_ORDEN,
          variables: { id: this.props.orderId }
      })
      .then(async (results)=> {
        const { loading, error, data } = results
        let cantidad = null;
        let descuento = null;
        let precioUnidad = null;
        let total = 0;
        let tipoDescuento = null;
        let descuentoFinal = 0;
        if (data) {
          cantidad = _get(data, 'getFullOrden.cantidad', null);
          tipoDescuento = _get(data, 'getFullOrden.cupon.tipo', null);
          descuento = _get(data, 'getFullOrden.cupon.descuento', null);
          precioUnidad = _get(data, 'getFullOrden.producto.number', null);
          total = cantidad * precioUnidad;
          switch (tipoDescuento) {
            case 'porcentaje':
              descuentoFinal = total * (descuento / 100);
              break;
            case 'dinero':
              descuentoFinal = descuento;
              break;
          }
          total = total - descuentoFinal;
          console.log('calculos stripe', {
            cantidad,
            tipoDescuento,
            precioUnidad,
            descuento,
            descuentoFinal,
            total
          });
        }
        console.log("total",total,this.props.stripe)
        let {token} = await this.props.stripe.createToken({name: "Name"});
        let response = await fetch(LOCAL_API_URL + "/stripe/chargeToken", {
          method: "POST",
          headers:{"Content-Type": 'application/json',},
          mode: 'cors',
          cache: 'default',
          body: JSON.stringify({stripeToken:token.id,orderId:this.props.orderId,amount:(total.toFixed(2)*100)})
        });
        console.log(response)
        if(response.ok){
          setTimeout(() => {
            window.location.href = '/thank-you';
          }, 1000);
        }
      })
      .catch(error => {
        alert(`error : ${error}`);
      })

  }

  render() {

    if (this.state.complete) return <Redirect to="/thank-you" />;

    return (
      <div style={{marginTop: 30, width: 350}}>
        <Input style={{backgroundColor: 'transparent'}} type='text' placeholder='Nombre titular' className='inputtarjet' />

        <CardElement />
        <div style={{marginTop: 30}}>
            <Button type="primary" onClick={this.submit}>Hacer el pago</Button>
        </div>
        
      </div>
    );
  }
}

export default injectStripe(withApollo(CheckoutForm));