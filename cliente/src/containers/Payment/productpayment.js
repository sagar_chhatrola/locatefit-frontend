import React, { Component } from 'react';
import './Payment.css';
import { Card } from 'antd';
import { IMAGES_PATH } from '../../config';
import Img from '../../components/Varios/Img';

const { Meta } = Card;

class RevisionRequest extends Component {
  render() {
    const { id = '', title = '', creator = {}, image = null } = this.props;
    const { nombre = '', apellidos = '' } = creator;
    const imageUrl = IMAGES_PATH + image;
    return (
      <div>
        <Card style={{ marginTop: 16 }}>
          <Meta
            avatar={
              <Img fullResImage={imageUrl} className='imagen-confirmar' />
            }
            title={<h4>{title}</h4>}
            description={
              <p>
                Ofrecido por:{' '}
                <span>
                  {nombre} {apellidos}
                </span>
              </p>
            }
          />
        </Card>
      </div>
    );
  }
}

export default RevisionRequest;
