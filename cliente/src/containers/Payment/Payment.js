import React, { Component } from 'react';
import Footer from './../../components/layout/Footer';
import { Icon } from 'antd';
import './Payment.css';
import FormularioPayment from './formulariopayment';

class Payment extends Component {
  render() {
    return (
      <div className='container-payment'>
        <div className='container mt-5'>
          <FormularioPayment {...this.props} />
        </div>

        <div style={{textAlign: 'center', marginBottom: 50}}>
          <Icon theme="twoTone" twoToneColor="#52c41a" type="safety-certificate" /> Pago 100% seguro y cifrado.
         </div>

        <Footer />
      </div>
    );
  }
}

export default Payment;
