import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import './Register.css';
import Footer from './../../components/layout/Footer';
import RegistrationForm from './../profesional/regiterformprofesionl';
import axios from 'axios';
class Register extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    password2: '',
    errors: {},
    verifyError: false
  };

  componentDidMount = () => {
    console.log(this.props);
    if (this.props.match.path === '/confirm/:token') {
      var url = 'https://server.locatefit.es/verfiyEmail';
      axios
        .post(url, this.props.match.params)
        .then(res => {
          console.log(res);
          if (res.data.verified) {
            this.props.history.push('/login');
          } else {
            this.setState({
              verifyError: true
            });
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({
            verifyError: true
          });
        });
    }
  };

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.errors) {
      return {
        errors: nextProps.errors
      };
    }
    return null;
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };

    this.props.registerUser(newUser, this.props.history);
  };

  render() {
    return (
      <div className='register-page-content'>
        <div className='containerRegister'>
          <div className='regiterfomrs1'>
            {this.state.verifyError && <h2>Enlace no válido o caducado</h2>}
            {!(this.props.match.path === '/confirm/:token') &&
              !this.state.verifyError && (
                <>
                  <h3>Inscríbete en Locatefit</h3>
                  <p>Crea tu cuenta totalmente gratis!</p>
                  <RegistrationForm refetch={this.props.refetch} />
                
                </>
              )}
              
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default withRouter(Register);
