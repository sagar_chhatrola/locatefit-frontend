import React, { Component } from 'react';
import Footer from './../../components/layout/Footer';
import './Register.css'
import Imgemail from './../Landing/covers.png'
import { Steps, Icon } from 'antd';

const { Step } = Steps;

class Confirmaremail extends Component {
  render() {
    return (
      <div>
      <div className='containerRegisteremail'>
        <div className='containerRegisteremail'>
            <h2>Verifica tu dirección de correo electrónico</h2>
            <p>Ve a tu bandeja de entrada, te hemos dejado un enlace para verificar tu email</p>
        </div>
        
        <div className='containerRegisteremail'>
          <img className="ImgEmail" src={Imgemail} />
        </div>
        <div className='btnloGinF'>
            <p>¿Ya has verificado tu Email?</p>
            <a href='/login' className='btnloGin'>Iniciar sesión</a>
        </div>
        
      </div>
      <Footer />
      </div>
    )
  }
}

export default Confirmaremail;