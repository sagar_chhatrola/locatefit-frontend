import React, { Component } from "react";
import "./notfound.css";
import Footer from "./../../components/layout/Footer";
import "antd/dist/antd.css";
import { Result, Button } from "antd";

class Notfound extends Component {
  render() {
    return (
      <div className="notfound-page-content">
        <Result
          status="404"
          title="404"
          subTitle="Lo sentimos, la página que visitaste no existe."
          extra={<Button type="primary">Volver al inico</Button>}
        />
        <Footer />
      </div>
    );
  }
}

export default Notfound;
