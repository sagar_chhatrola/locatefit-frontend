import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import './Login.css';
import { Input, Checkbox, message, Spin } from 'antd';
import Footer from './../../components/layout/Footer';
import SocialLogin from './../profesional/socialLogin';

/* import Error from '../Alertas/Error';
 */
import { Mutation } from 'react-apollo';
import { AUTENTICAR_USUARIO } from '../../mutations';

const initialState = {
  email: '',
  password: ''
};

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState
    };
  }

  actualizarState = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value
    });
  };

  limpiarState = () => {
    this.setState({ ...initialState });
  };

  iniciarSesion = (e, usuarioAutenticar) => {
    e.preventDefault();

    usuarioAutenticar().then(async ({ data: res }) => {
      if (res.autenticarUsuario.success) {
        localStorage.setItem('token', res.autenticarUsuario.data.token);
        localStorage.setItem('id', res.autenticarUsuario.data.id);

        // console.log(this.props);
        // ejecutar el query una vez iniciado sesion
        await this.props.refetch();

        // Limpiar state
        this.limpiarState();

        // Redireccionar el contenido
        setTimeout(() => {
          if (this.props.location && this.props.location.search) {
            const params = new URLSearchParams(this.props.location.search);
            const productId = params.get('productId');
            if (productId) this.props.history.push(`/deals/${productId}`);
            else this.props.history.push('/profile');
          } else {
            this.props.history.push('/profile');
          }
        }, 500);
      } else {
        message.error(res.autenticarUsuario.message);
      }
    });
  };
  logon = async (email, password) => {
    this.setState(
      {
        email: email,
        password: password
      },
      () => {
        document.getElementById('loginSubmit').click();
      }
    );
  };
  validarForm = () => {
    const { email, password } = this.state;

    const noValido = !email || !password;
    console.log(noValido);
    return noValido;
  };

  render() {
    console.log(this.props);
    const { email, password } = this.state;

    return (
      <div className={this.props.fromSignup ? '' : 'register-page-content'}>
        <Fragment>
          <Mutation
            mutation={AUTENTICAR_USUARIO}
            variables={{ email, password }}
          >
            {(usuarioAutenticar, { loading, error, data }) => {
              if (loading) {
                return (
                  <div className={this.props.fromSignup ? '' : 'page-loader'}>
                    <Spin size={this.props.fromSignup ? '' : 'large'}></Spin>
                  </div>
                );
              }

              return (
                <div className={this.props.fromSignup ? '' : 'containerLogin'}>
                  <div className={this.props.fromSignup ? '' : 'contLogin'}>
                    {!this.props.fromSignup && (
                      <>
                        <h3>Iniciar sesión</h3>
                        <p>inicia sesión con tu cuenta</p>
                      </>
                    )}
                    <form
                      onSubmit={e => this.iniciarSesion(e, usuarioAutenticar)}
                      className='col-md-8'
                      style={{
                        display: this.props.fromSignup ? 'none' : 'block'
                      }}
                    >
                      <div className='form-group'>
                        <label>Correo electrónico</label>
                        <Input
                          onChange={this.actualizarState}
                          value={email}
                          type='text'
                          name='email'
                          className='form-control'
                          placeholder='Correo electrónico'
                        />
                      </div>

                      <div className='form-group'>
                        <label>Contraseña</label>
                        <Input.Password
                          onChange={this.actualizarState}
                          value={password}
                          type='password'
                          name='password'
                          className='form-control'
                          placeholder='Contraseña'
                        />
                      </div>

                      <div className='forgot'>
                        <Checkbox>Recordarme</Checkbox>
                        <a
                          className='login-form-forgot'
                          href='/recoverpassword'
                        >
                          ¿Lo olvidaste?
                        </a>
                      </div>

                      <button
                        disabled={loading || this.validarForm()}
                        type='submit'
                        className='btn btn-primary'
                        id='loginSubmit'
                      >
                        Iniciar sesión
                      </button>

                      <div className='forgot'>
                        ¿No tienes una cuenta?{' '}
                        <a href='/register'>Regístrate ahora!</a>
                      </div>
                    </form>

                    <SocialLogin logon={this.logon} />
                  </div>
                </div>
              );
            }}
          </Mutation>
        </Fragment>
        {!this.props.fromSignup && <Footer />}
      </div>
    );
  }
}

export default withRouter(LoginForm);
