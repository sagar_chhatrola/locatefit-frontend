import React, { Component } from 'react';
import './Login.css';

import LoginForm from './loginform';

class Login extends Component {
  render() {
    return (
      <div className='register-page-content'>
        <div className='containerLogin'>
          <div className='contLogin'>
            <LoginForm />
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
