import React from "react";
import "antd/dist/antd.css";
import { Drawer, List, Avatar, Divider, Col, Row } from "antd";
import { Query } from 'react-apollo';
import { PRODUCT_QUERY } from './../../queries';



const pStyle = {
  fontSize: 20,
  color: "rgba(0,0,0,0.85)",
  lineHeight: "22px",
  display: "block",
  marginBottom: 16
};

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: "26px",
      marginBottom: 7,
      color: "rgba(0,0,0,0.65)"
    }}
  >
    <p
      style={{
        marginRight: 18,
        marginTop: 12,
        display: "inline-block",
        color: "rgba(0,0,0,0.85)"
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);

class Publicprofile extends React.Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
   
    return (
      <Query query={PRODUCT_QUERY} variables={{ id }}>
            {({ loading, error, data: res }) => {
              if (loading) return null;
              if (error) return console.log('error getting user detail: ', error);

              if (!res.getProducto.success) { // if any error occurs then show an error message from backend
                return (message.error(res.getProducto.message))
              }

              // If data found then show detail
              if (res.getProducto.success) {
                return (
              <div>
                
              </div>
         );
        }

      }}
    </Query>

    );
  }
}

export default Publicprofile;
