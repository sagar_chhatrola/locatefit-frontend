import React, { Component } from "react";
import PropTypes from "prop-types";
import { Redirect } from 'react-router-dom';
import _get from "lodash.get";
import { withApollo } from 'react-apollo';
import Footer from "./../../components/layout/Footer";
import Coments from "../../components/My-Stats/coments";
import Check from "./../../assets/images/corregir.svg";
import "./Product-Plan.css";
import { Query, Mutation } from 'react-apollo';
import { PRODUCT_QUERY } from '../../queries';
import { CREAR_USUARIO_FAVORITE_PRODUCTO, CREAR_MODIFICAR_ORDEN, CREATE_NOTIFICATION } from '../../mutations';
import { message, DatePicker } from "antd";
import "antd/dist/antd.css";
import moment from 'moment';
import locale from 'antd/es/date-picker/locale/es_ES'
import { LOCAL_API_URL } from './../../config';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Rate,
  Icon,
  Breadcrumb,
  Modal,
  Button,
  notification,
  InputNumber,
  Popover,
  Tag,
  List,
  Drawer, Avatar, Divider, Col, Row, Tooltip, Spin, Progress, Alert, Affix
} from "antd";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailShareButton
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
  EmailIcon
} from "react-share";
import GoogleMapReact from "google-map-react";

const openNotification = placement => {
  notification.success({
    message: 'Has recibido una nueva orden',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    placement,
  });
};


const dateFormat = 'MM/DD/YYYY'; // compatible con la fecha y hora del servidor para evitar conflictos de API
const timeFormat = 'HH:mm';
const dateFormatWithTime = dateFormat + ' ' + timeFormat;


const openNotificationdenuncia = () => {
  notification.open({
    message: 'Notification Title',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
  });
};

const text1 = <span>Autónomo verificado</span>;

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const { confirm } = Modal;


function onChange(a, b, c) {
  // console.log(a, b, c);
}

function showDeleteConfirm() {
  confirm({
    title: 'Esta seguro que deseas denunciar este anuncio?',
    content: 'Gracias por denunciar anuncios que inclumplen las políticas de la comunidad',
    okText: 'Si',
    okType: 'danger',
    cancelText: 'No',
    onOk() {
      message.success('Este anuncio ha sido denunciado correctamente gracias hacer de Locatefit un lugar mejor', 5);
    },

    onCancel() {
      console.log('Cancel');
    },
  });
}

const pStyle = {
  fontSize: 20,
  color: "rgba(0,0,0,0.85)",
  lineHeight: "22px",
  display: "block",
  marginBottom: 16
};

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: "26px",
      marginBottom: 7,
      color: "rgba(0,0,0,0.65)"
    }}
  >
    <p
      style={{
        marginRight: 18,
        marginTop: 12,
        display: "inline-block",
        color: "rgba(0,0,0,0.85)"
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);


const jsDaysValue = {
  domingo: 0,
  lunes: 1,
  martes: 2,
  miercoles: 3,
  jueves: 4,
  viernes: 5,
  sabado: 6
}

class ProductPlan extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      visibleChat: false,
      anadirFavorito: false,
      size: "largue",
      open: false,
      visible1: false,
      open: false,
      cantidad: 1,
      orden: null,
      selectedDayOfWeek: null,
      selectedDate: null,
      selectedTime: null,
      selectedHour: null,
      producto: null,
      minCantidad: 0,
      maxCantidad: 0,
      recipient: '',
      messageTexteNeworden: 'Locatefit S.L., has recibido una nueva orden ve, a tu perfil para procesarla. https://locatefit.es/profile',
      lat: '',
      lng: ''


    }
  }

  openDays = [];
  openDaysCalculated = false;

  static defaultProps = {
    zoom: 11,

  };

  range = (start, end) => {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  }

  getDisabledHours = (start, end) => {
    const result = [];

    if (this.state.producto) {
      const productoTime = this.state.producto.time.split(':');
      const productoTimeInHours = Number(productoTime[0]) + (Number(productoTime[1]) > 30) ? 1 : 0;
      this.state.producto.ordenes.forEach(orden => {
        if (orden.endDate == this.state.selectedDate) {
          start = Number(start) + Number(productoTimeInHours * orden.cantidad)
        }
      });
    }

    for (let i = 0; i <= 23; i++) {
      if (!(i >= start && i <= end))
        result.push(i);
    }

    return result;
  }

  disabledDateTime = () => {
    const { selectedDayOfWeek, selectedHour } = this.state;
    if (selectedDayOfWeek) {
      return {
        disabledHours: () => this.getDisabledHours(selectedDayOfWeek.start.hours, selectedDayOfWeek.end.hours),
        disabledMinutes: () => {
          if (selectedHour) {

            if (selectedHour == selectedDayOfWeek.start.hours) {
              return this.range(1, selectedDayOfWeek.start.minutes)
            } else if (selectedHour == selectedDayOfWeek.end.hours) {
              return this.range(selectedDayOfWeek.end.minutes, 60)
            } else {
              return [];
            }

          } else {
            return this.range(0, 60)
          }

        }
      };
    } else {
      return {
        disabledHours: () => this.range(0, 24),
        disabledMinutes: () => this.range(0, 60),
      }
    }
  }

  onDatePickerChange = (date) => {
    if (date && this.state.selectedDayOfWeek) {
      this.setState({
        selectedTime: date.format(timeFormat),
        selectedHour: date.hour()
      });
    }
    if (date && this.openDays.length > 0) {
      const day = this.openDays.find(index => index.dayOfWeek == date.day());
      let maxCantidad = 0;
      let minCantidad = 1;
      let diff = (Number(day.end.hours) + 1) - Number(day.start.hours);

      if (this.state.producto) {
        const productoTime = this.state.producto.time.split(':');
        const productoTimeInHours = Number(productoTime[0]) + (Number(productoTime[1]) > 30) ? 1 : 0;
        this.state.producto.ordenes.forEach(orden => {
          if (orden.endDate == this.state.selectedDate) {
            diff = Number(diff) - Number(productoTimeInHours * orden.cantidad);
          }
        });
      }

      if (diff > 0) {
        if (this.state.cantidad > diff) this.setState({ cantidad: diff });
        maxCantidad = diff;
      } else {
        maxCantidad = 0;
        minCantidad = 0;
        this.setState({ cantidad: 0 });
      }

      this.setState({ selectedDate: date.format(dateFormat), selectedDayOfWeek: day, maxCantidad, minCantidad });
      // this.setState({maxCantidad: result.length});
    } else {
      this.setState({ selectedDate: null, selectedDayOfWeek: null });
    }
  }

  calculateOpenDays = (producto) => {
    if (!this.openDaysCalculated) {

      this.setState({ producto });

      if (producto.domingo) {
        let day = { dayOfWeek: jsDaysValue.domingo };
        const startFrom = producto.domingo_from.split(":");
        const endTo = producto.domingo_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.lunes) {
        let day = { dayOfWeek: jsDaysValue.lunes };
        const startFrom = producto.lunes_from.split(":");
        const endTo = producto.lunes_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.martes) {
        let day = { dayOfWeek: jsDaysValue.martes };
        const startFrom = producto.martes_from.split(":");
        const endTo = producto.martes_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.miercoles) {
        let day = { dayOfWeek: jsDaysValue.miercoles };
        const startFrom = producto.miercoles_from.split(":");
        const endTo = producto.miercoles_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.jueves) {
        let day = { dayOfWeek: jsDaysValue.jueves };
        const startFrom = producto.jueves_from.split(":");
        const endTo = producto.jueves_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.viernes) {
        let day = { dayOfWeek: jsDaysValue.viernes };
        const startFrom = producto.viernes_from.split(":");
        const endTo = producto.viernes_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      if (producto.sabado) {
        let day = { dayOfWeek: jsDaysValue.sabado };
        const startFrom = producto.sabado_from.split(":");
        const endTo = producto.sabado_to.split(":");

        day['start'] = { hours: startFrom[0], minutes: startFrom[1] };
        day['end'] = { hours: endTo[0], minutes: endTo[1] };
        this.openDays.push(day);
      }
      this.openDaysCalculated = true;

    }
  }

  disabledDate = (current, producto) => {
    // Can not select days before today and today

    if (!this.openDaysCalculated) {
      this.calculateOpenDays(producto);
    }

    if (this.openDays.length > 0) {
      const dayExist = this.openDays.findIndex(index => index.dayOfWeek == current.day());
      if (dayExist == -1) {
        return current;
      }
    }

    return current && current < moment().endOf('day');
  }

  renderMarkers(map, maps, city) {
    let marker = new maps.Marker({
      position: {
        lat: this.state.lat,
        lng: this.state.lng
      },
      map,
      title: city
    });
  }

  refetch = null;

  anadirFavorito = (event, crearUsuarioFavoritoProducto, productoId, refetch, onFavouriteAdd) => {
    event.preventDefault();
    this.setState({ anadirFavorito: true });

    crearUsuarioFavoritoProducto({ variables: { productoId } }).then(async ({ data: res }) => {

      this.setState({ anadirFavorito: false });

      if (res && res.crearUsuarioFavoritoProducto && res.crearUsuarioFavoritoProducto.success) {
        notification.open({
          message: 'Servicio añadido a la lista de deseos',
          description:
            'tu servicio a sido añadido a la lista de deseo sastifactoriamente visita tu lista de deseos para completar la contratación',
          icon: <Icon type="heart" style={{ color: '#d73a49' }} />,
        });
        refetch();
        onFavouriteAdd();
      }
      else if (res && res.crearUsuarioFavoritoProducto && !res.crearUsuarioFavoritoProducto.success)
        message.error(res.crearUsuarioFavoritoProducto.message);

    });
  };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };


  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };


  handleOpenChange = open => {
    this.setState({ open });
  };

  handleClose = () => this.setState({ open: false });



  showModal = () => {
    this.setState({
      visible: true
    });
  };


  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };

  handleOpenChange = open => {
    this.setState({ open });
  };

  handleClose = () => this.setState({ open: false });

  componentWillReceiveProps(props) {
    if (this.refetch && props.favouriteDeletedFromWishListCounter) this.refetch();
  }

  getProductLatLng(city) {
    let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?address=${city}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
    fetch(apiUrlWithParams)
      .then(response => response.json())
      .then(data => {
        if (data.results.length > 0) {
          this.setState({
            lat: data.results[0].geometry.location.lat,
            lng: data.results[0].geometry.location.lng
          })

          console.log(this.state.lat, this.state.lng)
        }

      })
      .catch(error => {
        // this.setState({ locationFilterChecked: false });
        console.log('error in product-plan getting lat, lng: ', error);

      })
  }


  SendTextSMSNeworden = () => {
    fetch(`${LOCAL_API_URL}/send-message?recipient=${this.state.producto.creator.telefono}&textmessage=${this.state.messageTexteNeworden}`)
      .catch(err => console.log(err))
    console.log('sending:  ', this.state.producto.creator.telefono, this.state.messageTexteNeworden)
  };


  crearOrden = async (mutacion, productData, refetch = () => { }) => {
    const { id } = this.props.match.params;
    const { cantidad, selectedDate, selectedTime } = this.state;

    if (!selectedDate) {
      message.error('Por favor, seleccione una fecha para continuar.');
      return;
    } else if (!selectedTime) {
      message.error('Por favor, seleccione el tiempo para continuar.');
      return;
    } else if (!cantidad) {
      message.error('Por favor, selecciona la cantidad para continuar.');
      return;
    }
    if (id && cantidad) {
      try {
        const input = { producto: id, cantidad, endDate: selectedDate, time: selectedTime }
        const resultado = await mutacion({ variables: { input } });
        const orden = _get(resultado, 'data.crearModificarOrden.id', null);
        if (orden) {
          this.setState({ orden })
          this.props.client
            .mutate({
              mutation: CREATE_NOTIFICATION,
              variables: { input: { orden: orden.id, user: productData.created_by, cliente: localStorage.getItem('id'), profesional: productData.created_by, type: 'new_order' } }
            })
            .then(async (results) => {
              console.log("results", results)
            }).catch(err => {
              console.log("err", err)
            })
          this.SendTextSMSNeworden()
        } else {
          notification.open({
            message: 'Error al crear orden',
            description:
              'Oops hemos hecho algo mal y no pudimos crear tu orden, por favor intenta de nuevo!',
            icon: <Icon type="warning" style={{ color: '#d73a49' }} />,
          });
        }

      } catch (error) {
        console.log("error creando orden", error)
      }
    }
  }

  render() {
    const { id } = this.props.match.params;
    const { cantidad, orden } = this.state;

    const position = {
      lat: this.state.lat,
      lng: this.state.lng
    }
    let averageRating = 0;
    if (orden) {
      return <Redirect to={`/payment/${orden}`} {...this.props} />;
    }
    return (
      <div className='containerproductplan'>
        <section className="section">
          <Query query={PRODUCT_QUERY} variables={{ id, updateProductVisit: true }} onCompleted={res => this.calculateOpenDays(res.getProducto.data)}>
            {({ loading, error, data: res, refetch }) => {
              console.log("es.getProducto.data", res)
              this.refetch = refetch;
              if (loading) {
                return (
                  <div className="page-loader">
                    <Spin size="large"></Spin>
                  </div>
                );
              }
              if (error) return console.log('error getting user detail: ', error);

              if (!res.getProducto.success) { // if any error occurs then show an error message from backend
                return (message.error(res.getProducto.message))
              }

              // If data found then show detail
              if (res.getProducto.success) {
                this.getProductLatLng(res.getProducto.data.city);

                // calculate average rating
                let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };

                res.getProducto.data.professionalRating.forEach(item => {
                  if (item.rate == 1) rating['1'] += 1;
                  else if (item.rate == 2) rating['2'] += 1;
                  else if (item.rate == 3) rating['3'] += 1;
                  else if (item.rate == 4) rating['4'] += 1;
                  else if (item.rate == 5) rating['5'] += 1;
                });

                const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / res.getProducto.data.professionalRating.length;
                let averageRating = 0;
                if (res.getProducto.data.professionalRating.length) {
                  averageRating = ar.toFixed(1);
                }

                return (
                  <div className='consli'>
                    <div className='vendorinfo'>
                      <Avatar src={LOCAL_API_URL + "/assets/images/" + res.getProducto.data.creator.foto_del_perfil} size='large' className='ava' />
                      <h4>{res.getProducto.data.creator.nombre} {res.getProducto.data.creator.apellidos}</h4>
                      <div style={{alignSelf: 'cente', display: 'flex', justifyContent: 'center'}}>
                        <Icon className='IccONN' type="star" /><p style={{color: 'white', fontSize: 18}}>{averageRating} Valoración</p>
                        <p style={{color: 'white', fontSize: 18}}>  · {res.getProducto.data.category.title} · </p>
                        <Icon className='IccONN1' type="environment" /><p style={{color: 'white', fontSize: 18}}>{res.getProducto.data.city}</p>
                      </div>
                    </div>
                    <Carousel
                      autoPlay={true}
                      showArrows={false}
                      showStatus={false}
                      showIndicators={false}
                      showThumbs={false}
                      swipeable={true}
                      stopOnHover={true}
                      infiniteLoop={true}
                    >
                      {
                        res.getProducto.data.fileList.map(
                          (file, i) => (
                            <div className='opacity' key={i}>
                              <img className="ImagenPro" src={LOCAL_API_URL + "/assets/images/" + file} alt="" />
                            </div>
                          ))
                      }
                    </Carousel>
                    <div className="containerproductplan1">
                      <section className="col-lg-8 section1">
                        <div className="container mt-5 migas">
                          <div className='migas1'>
                            <Breadcrumb>
                              <Breadcrumb.Item>
                                <a href="/">Inicio</a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item>
                                <a href="/category">Categoria</a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item>
                                <a href={"/services/" + res.getProducto.data.category.id}>{res.getProducto.data.category.title}</a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item className="acti">
                                {res.getProducto.data.title}
                              </Breadcrumb.Item>
                            </Breadcrumb>
                          </div>
                        </div>

                        <div className="cont leudy">
                          <div className="container">
                            <h2>{res.getProducto.data.title}</h2>
                            <div className="product-info">
                              <p className="rating-nubmer">{averageRating}</p>
                              <Rate className="rate" disabled value={Number(averageRating)} />
                              <p>{res.getProducto.data.professionalRating.length} Opiniones</p>
                              <Icon
                                className="casa"
                                type="environment"
                                twoToneColor="#95ca3e"
                              />
                              <p>{res.getProducto.data.city}</p>
                            </div>
                          </div>

                          <div className="sticky-area">
                            <div className="main-side">
                              <div className="container slideshow">
                                <h3 className="mt-3">Descripción</h3>
                                {res.getProducto.data.description}
                              </div>



                              <div className="container mt-5">
                                <h3>Información del profesional</h3>
                                <div>
                                  <List
                                    dataSource={[
                                      {
                                        name: "User"
                                      }
                                    ]}
                                    bordered
                                    renderItem={item => (
                                      <List.Item
                                        key={item.id}
                                        actions={[
                                          <Button onClick={this.showDrawer} type="dashed" key={`a-${res.getProducto.data.creator.id}`}>
                                            <Icon className="SHAREICONDISK" type="user-add" /> MÁS DETALLES DEL PROFESIONAL
                                      </Button>
                                        ]}
                                      >
                                        <List.Item.Meta
                                          avatar={
                                            <Avatar src={LOCAL_API_URL + "/assets/images/" + res.getProducto.data.creator.foto_del_perfil} />
                                          }
                                          title={<a href="#Perfil_publico">{res.getProducto.data.creator.nombre} <Tooltip placement="right" title={text1}><Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" /></Tooltip></a>}
                                          description={res.getProducto.data.creator.profesion}
                                        />
                                      </List.Item>
                                    )}
                                  />
                                  <Drawer
                                    width={640}
                                    placement="right"
                                    closable={false}
                                    onClose={this.onClose}
                                    visible={this.state.visible}
                                  >
                                    <Avatar src={LOCAL_API_URL + "/assets/images/" + res.getProducto.data.creator.foto_del_perfil} />
                                    <p style={{ ...pStyle, marginBottom: 24 }}>Detalles del profesional</p>
                                    <p style={pStyle}>Información personal</p>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Nombre" content={res.getProducto.data.creator.nombre} />
                                      </Col>
                                      <Col span={12}>
                                        <DescriptionItem title="Apellidos" content={res.getProducto.data.creator.apellidos} />
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Ciudad" content={res.getProducto.data.creator.ciudad} />
                                      </Col>
                                      <Col span={12}>
                                        <DescriptionItem title="País" content="España 🇪🇸" />
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Cumpleaños" content={() => new Date(Number(res.getProducto.data.creator.fecha_de_nacimiento)).toLocaleDateString()} />
                                      </Col>
                                      <Col span={12}>
                                        <DescriptionItem title="Sitio web" content={<a href={res.getProducto.data.creator.propia_web_enlazar} target="_blank">{res.getProducto.data.creator.propia_web_enlazar}</a>} />
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col span={24}>

                                      </Col>
                                    </Row>
                                    <Divider />
                                    <p style={pStyle}>Información profesional</p>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Profesión" content={res.getProducto.data.creator.profesion} />
                                      </Col>
                                      <Col span={12}>
                                        <DescriptionItem title="Grado de experiencia" content={<Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} percent={res.getProducto.data.creator.grado} />} />
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Estudio" content={res.getProducto.data.creator.estudios} />
                                      </Col>
                                      <Col span={12}>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col span={24}>
                                        <DescriptionItem
                                          title="Descrición"
                                          content={res.getProducto.data.creator.descripcion}
                                        />
                                      </Col>
                                    </Row>
                                    <Divider />
                                    <p style={pStyle}>Contacto</p>
                                    <Row>
                                      <Col span={12}>
                                        <DescriptionItem title="Email" content={res.getProducto.data.creator.email} />
                                      </Col>
                                      <Col span={12}>
                                        <DescriptionItem
                                          title="Número movil"
                                          content={res.getProducto.data.creator.telefono}
                                        />
                                      </Col>
                                    </Row>
                                    <Col span={3}>
                                      <a href={res.getProducto.data.creator.fb_enlazar} target="_blank">
                                        <Icon className="faceb" type="facebook" theme="filled" />
                                      </a>
                                    </Col>

                                    <Col span={3}>
                                      <a href={res.getProducto.data.creator.twitter_enlazar} target="_blank" >
                                        <Icon className="twitter" type="twitter-square" theme="filled" />
                                      </a>
                                    </Col>

                                    <Col span={3}>
                                      <a href={res.getProducto.data.creator.instagram_enlazar} target="_blank">
                                        <Icon className="instagram" type="instagram" theme="filled" />
                                      </a>
                                    </Col>

                                    <Col span={3}>
                                      <a href={res.getProducto.data.creator.youtube_enlazar} target="_blank">
                                        <Icon className="youtube" theme="filled" type="linkedin" />
                                      </a>
                                    </Col>
                                  </Drawer>
                                </div>
                              </div>

                              <div className="container mt-5">
                                <h5><img className="seguro" src="https://imageneslocatefit.s3.eu-west-3.amazonaws.com/proteger.svg" alt="seguro" />Profesionales revisados ​​y con opiniones de clientes</h5>
                                <p>Las tareas contratada, reservadas y pagadas directamente a través de la plataforma Locatefit son realizadas por profesionales experimentados y  altamente calificados por clientes como usted.</p>
                              </div>
                              <div className="container mt-5">
                                <h3>Ubicación</h3>

                                <div style={{ height: "30vh", width: "100%" }}>
                                  <GoogleMapReact
                                    bootstrapURLKeys={{ key: "AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg" }}
                                    center={{ lat: position.lat, lng: position.lng }}
                                    defaultZoom={this.props.zoom}
                                    onGoogleApiLoaded={({ map, maps }) => this.renderMarkers(map, maps, res.getProducto.data.city)}
                                  >
                                  </GoogleMapReact>
                                </div>
                                <div className="container mt-5">
                                  <h3>Valoraciones</h3>

                                  <div className="startverify">
                                    <div className="startverifychek">
                                      <img height="50" src={Check} alt="" />
                                    </div>
                                    <div className="startverifytittle">
                                      <h7>Opiniones 100% Verificadas </h7>
                                      <p className="verified">
                                        Todas las opiniones pertenecen a clientes que han contratado a
                                        este Profesional.
                                      </p>
                                    </div>
                                  </div>

                                  <Coments data={res.getProducto.data.professionalRating} />
                                </div>


                              </div>
                            </div>

                            <div className="side-menu">
                              <div className="comprar">
                                <div className="features">
                                  <div>
                                    <Icon type="history" />
                                    <h6>Tiempo limitado!</h6>
                                  </div>
                                  <div>
                                    <Icon type="eye" />
                                    <h6>+{res.getProducto.data.visitas} visitas</h6>
                                    {/* <h6>+{Math.floor(Math.random() * (50, 70) + 20)} visitas</h6> */}
                                  </div>
                                  <div>
                                    <Icon type="star" theme="twoTone" twoToneColor="#fadb14" />
                                    <Popover content={<div>
                                      <p>{averageRating}</p>
                                      <Rate disabled defaultValue={Number(averageRating)} />
                                    </div>} trigger="hover">
                                      <h6>{res.getProducto.data.professionalRating.length} Opiniones</h6>
                                    </Popover>
                                  </div>
                                </div>

                                <h4>{res.getProducto.data.title}</h4>
                                <div className="product-price">
                                  <p>+ de {res.getProducto.data.ordenes.length} Ordenes</p>
                                  <div>
                                    <h4>{res.getProducto.data.number + "€" + res.getProducto.data.currency}</h4>
                                    <Tag className="tag" color="green">
                                      {Math.floor(Math.random() * (50, 70) + 20)} % DE DESCUENTO
                              </Tag>
                                  </div>

                                </div>
                              </div>
                              <div style={{ flexDirection: 'row', marginBottom: 20, marginTop: 20, width: '100%' }}>
                                <p className="iii1" ><Icon className="iii" type="calendar" />  ¿Cuándo quieres que vallamos?</p>
                                <DatePicker
                                  locale={locale}
                                  style={{ width: '100%' }}
                                  format={dateFormatWithTime}
                                  disabledDate={current => this.disabledDate(current, res.getProducto.data)}
                                  onChange={this.onDatePickerChange}
                                  disabledTime={this.disabledDateTime}
                                  showTime={{ format: timeFormat, defaultValue: moment('00:00', 'HH:mm') }}
                                />
                              </div>
                              <div style={{ marginBottom: 20, width: '100%' }}>
                                <Alert message={<p> Este profeisonal tiene {this.state.maxCantidad} horas disponibles para este día</p>} type="success" />
                              </div>
                              <div style={{ flexDirection: 'row', marginBottom: 20 }}>
                                <p className="iii1" ><Icon className="iii" type="clock-circle" />  ¿Cuántas horas te gustaría reservar?</p>
                                <InputNumber value={cantidad} min={this.state.minCantidad} max={this.state.maxCantidad} onChange={(value) => this.setState({ cantidad: value })} />
                              </div>
                              <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
                                {(crearModificarOrden) => (
                                  <Button
                                    type="primary"
                                    block
                                    className="main-button"
                                    onClick={(e) => {
                                      e.preventDefault();
                                      this.crearOrden(crearModificarOrden, res.getProducto.data);
                                    }}
                                  >
                                    CONTRATAR PROFESIONAL
                            </Button>
                                )}
                              </Mutation>
                              {res.getProducto.data.anadidoFavorito ?
                                <Button
                                  type="dashed"
                                  block
                                  className="main-button">
                                  <Icon type="heart" />  añadido a favorito
                                </Button>
                                :
                                <Mutation mutation={CREAR_USUARIO_FAVORITE_PRODUCTO}>
                                  {(crearUsuarioFavoritoProducto) => {
                                    return (
                                      <Button
                                        loading={this.state.anadirFavorito}
                                        type="danger"
                                        block
                                        onClick={e => this.anadirFavorito(e, crearUsuarioFavoritoProducto, res.getProducto.data.id, refetch, this.props.onFavouriteAdd)}
                                        className="main-button">
                                        <Icon type="heart" />  añadir a favorito
                              </Button>
                                    )
                                  }}
                                </Mutation>
                              }

                              <div className="contas">
                                <h6>COMPRATE ESTA OFERTA!</h6>
                                <Row>
                                  <Col span={4}>
                                    <FacebookShareButton url={"https://locatefit.es/deals/" + res.getProducto.data.id}>
                                      <FacebookIcon size={40} round={true} />
                                    </FacebookShareButton>
                                  </Col>
                                  <Col span={4}>

                                    <TwitterShareButton url={"https://locatefit.es/deals/" + res.getProducto.data.id}>
                                      <TwitterIcon size={40} round={true} />
                                    </TwitterShareButton>
                                  </Col>
                                  <Col span={4}>

                                    <LinkedinShareButton url={"https://locatefit.es/deals/" + res.getProducto.data.id}>
                                      <LinkedinIcon size={40} round={true} />
                                    </LinkedinShareButton>
                                  </Col>
                                  <Col span={4}>

                                    <WhatsappShareButton url={"https://locatefit.es/deals/" + res.getProducto.data.id}>
                                      <WhatsappIcon size={40} round={true} />
                                    </WhatsappShareButton>
                                  </Col>
                                  <Col span={4}>

                                    <EmailShareButton url={"https://locatefit.es/deals/" + res.getProducto.data.id}>
                                      <EmailIcon size={40} round={true} />
                                    </EmailShareButton>
                                  </Col>
                                </Row><br />


                                <Button onClick={showDeleteConfirm} type="dashed">
                                  <Icon className="SHAREICONDISK" type="dislike" /> DENUNCIAR ANUNCIO
                              </Button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div >
                  </div>
                );
              }

            }}
          </Query>
        </section>
        <Footer />
      </div>
    );
  }
}

ProductPlan.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default withApollo(ProductPlan);
