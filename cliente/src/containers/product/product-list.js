import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import "./product-list.css";
import { Icon, Avatar, Tag, Row, Col, Tooltip, message, Button, Rate } from "antd";
import { LOCAL_API_URL } from './../../config';


function onShowSizeChange(current, pageSize) {
  console.log(current, pageSize);
}

const text = <span>Autónomo verificado</span>;



class ProductList extends Component {

  constructor(props) {
    super(props);
  }

  onClick = (event, productoId) => {
    event.preventDefault();
    if (this.props.session.obtenerUsuario) {
      this.props.history.push('/deals/' + productoId);
    } else {
      message.warning('Debe iniciar sesión para continuar.');
      message.warning('Redireccionando a la página de inicio de sesión.');
      this.props.history.push(`/login?productId=${productoId}`);
    }
  }

  render() {
    return (
      <div className="">
        {
          this.props.data.map(
            (producto, index) => (
              <div className="card-services" key={index}>
                {producto.fileList.length > 0 ? <div>
                  <img
                    className="imagenprod"
                    alt="Imagen servicio"
                    src={producto.fileList.length > 0 ? LOCAL_API_URL + "/assets/images/" + producto.fileList[0] : ""}
                  />
                </div> : null}
                <div className="titleanddes">
                  <h4>{producto.title}</h4>
                  <div className="iconsec">
                  <div style={{display: 'flex'}}>
                  <Avatar style={{marginRight: 10}} size="small" src={LOCAL_API_URL + "/assets/images/" + producto.creator.foto_del_perfil} />
                  <p style={{marginTop: 3}}>{producto.creator.nombre} {producto.creator.apellidos}</p>
                </div>
                </div>
                  <p>{producto.description}</p>
                  <div className="iconsec">
                    <div style={{display: 'flex'}}>
                      <Icon style={{marginRight: 5, color: '#95ca3e', marginTop: 5}} type="environment" />
                      <p style={{marginTop: 3}}>{producto.city}</p>
                    </div>
                    <Icon className="wilist" type="heart" theme={producto.anadidoFavorito ? 'filled' : 'outlined'} />
                  </div>
                </div>
                <div className="btnSec1">
                <div className="btnsec">
                  <div style={{ display: 'flex' }}>
                    <Icon type="star" theme="filled" style={{ color: '#fadb14', marginTop: 5 }} />
                    <Icon type="star" theme="filled" style={{ color: '#fadb14', marginTop: 5 }} />
                    <Icon type="star" theme="filled" style={{ color: '#fadb14', marginTop: 5 }} />
                    <Icon type="star" theme="filled" style={{ color: '#fadb14', marginTop: 5 }} />
                    <Icon type="star" theme="filled" style={{ color: '#fadb14', marginRight: 10, marginTop: 5 }} />
                    <p>
                      ({producto.professionalRating.length})
                    </p>
                  </div>
                  <div className="price">
                  <h3>
                  {producto.number + '€'}
                  </h3>
                  <p>
                  {producto.currency}
                  </p>
                  </div>
                </div>
                <div className="sec_Btncontr">
                    <a className="Btncontr" onClick={e => this.onClick(e, producto.id)}>CONTRATAR</a>
                  </div>
              </div>
            </div>
            ))
        }
      </div>
    );
  }
}

export default withRouter(ProductList);