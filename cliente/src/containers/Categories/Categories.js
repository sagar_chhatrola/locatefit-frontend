import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Categories.css";
import Footer from "./../../components/layout/Footer";
import Categories from "./../../containers/Landing/Categories";
import { Breadcrumb, Icon, Input} from "antd";
import "antd/dist/antd.css";
import SearchHomme from "./../Landing/search";

class Categoriesall extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    password2: "",
    errors: {},
    active_index: 1
  };


  static getDerivedStateFromProps(nextProps) {
    if (nextProps.errors) {
      return {
        errors: nextProps.errors
      };
    }
    return null;
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };

    this.props.registerUser(newUser, this.props.history);
  };
  changeTab = index => {
    this.setState({ active_index: index });
  };
  render() {
    return (
      <div className="Category-sesion1">
        <section className="pagelogin111">

          <div className="searchcate">

            <div className="searchForm1">
                  <h4 className="searchFormtittle">
                    ¿Para qué necesitas un profesional?
                  </h4>
                  <div className="input-group">
                    <SearchHomme />
                  </div>
                  <label className="sdescritionserch">
                    Encuentra profesionales cerca de ti.  <span><a className="loc" href="/category">Ver todas las categorías</a></span>
                  </label>
                </div>

            </div>

        <div className="wave wave1"></div>
                    <div className="wave wave2"></div>
                    <div className="wave wave3"></div>
                    <div className="wave wave4"></div>  
      </section>

        <Categories />

        <Footer />
      </div>
    );
  }
}

Categoriesall.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default Categoriesall;
