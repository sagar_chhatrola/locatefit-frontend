import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Categories.css";
import { Query } from 'react-apollo';
import { CATEGORIES_QUERY } from '../../queries';
import { Spin } from "antd";

export default class Categories extends Component {


  render() {
    let categorialink = "/services/"
    return (
      <section className="category-cards">
        <Query query={CATEGORIES_QUERY}>
          {({ loading, error, data, refetch }) => {
            if (loading) {
              return(
                <div className="page-loader-category">
                <Spin size="large"></Spin>
             </div>
              );
            }
            if (error) return console.log(error);
            return <div className="category-cards">

              {
                data.getCategories.map(
                  (cat, i) => (
                    <Link key={i} to={categorialink + cat.id} className="card">
                      <img src={process.env.PUBLIC_URL + cat.image} alt="" />
                      <div className="card-info">
                        <h6>{cat.title}</h6>
                        <p>{cat.description}</p>
                      </div>
                    </Link>
                  )
                )
              }

            </div>

          }}
        </Query>
      </section>
    );
  }
}
