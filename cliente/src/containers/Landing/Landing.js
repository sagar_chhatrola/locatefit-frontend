import React, { Component } from "react";
import Footer from "./../../components/layout/Footer";
import "./Landing.css";
import Categories from "./Categories";
import Quet from "../../components/Varios/Quet";
import Cover from "../../components/Varios/Cover";
import MobileApp from "../../components/Varios/MobileApp";
import { Modal } from "antd";
import Publicar from "../../components/Publish/Publish";
import { Icon, Carousel} from 'antd';
import SearchHomme from "./search";
import BarnerIMG1 from './barner1.png'
import BarnerIMG2 from './Barner2png.png'
import BarnerIMG3 from './barner3.png'
import LoginForm from './../Login/loginformHome';



class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const id = localStorage.getItem('id');
    return (
      <div style={{ overflow: "hidden", width: "100%", justifyContent: 'center', alignSelf: 'center'}}>
        <div className='containerlanding'>
         <div className="barner1">
            <h1>
              Los mejores profesionales, tu eliges la fecha, hora y lugar.
            </h1>
              <p>Organización, decoración, pintura, montaje, limpieza y más…</p>
                <div className="searchForm">
                    <h4>
                      ¿Para qué necesitas un profesional?
                    </h4>
                    <div className="input-group">
                        <SearchHomme />
                    </div>
                  <p>
                    Encuentra profesionales cerca de ti. <span><a className="loc" href="/category">Ver todas las categorías</a></span>
                  </p>
                </div>
              </div>
              <div className="barner1">
              <Carousel dots={false} autoplay={true} effect='fade'>
              <div>
                <img className="coversLanding" src={BarnerIMG1}/>
              </div>
              <div>
                <img className="coversLanding" src={BarnerIMG2}/>
              </div>
              <div>
                <img className="coversLanding" src={BarnerIMG3}/>
              </div>
            </Carousel>
              </div>
        </div>
      <div className="category-title">
        <h2 className="category-title">Categorías destacadas</h2>
        <a href="/category">Ver todas</a>
      </div>
      <Categories />
      <Quet />
      <Cover />
      <div className="profesionl">
        <h3><img className="seguro" src="https://imageneslocatefit.s3.eu-west-3.amazonaws.com/proteger.svg" alt="seguro" />Profesionales revisados ​​y con opiniones de clientes</h3>
        <p>Las tareas contratada, reservadas y pagadas directamente a través de la plataforma Locatefit son realizadas por profesionales experimentados y  altamente calificados por clientes como usted.</p>
      </div>

      <div className="Comofuncional">
        <div className="comofunciona__title"><h1>¿Cómo funciona Locatefit?</h1></div>

        <div className="itemS">

          <div className="SuBitems">
            <img className="SuBitemsIcons" src="https://imageneslocatefit.s3.eu-west-3.amazonaws.com/hora.png" alt="1º" />
            <h3>Elige una hora y fecha</h3>
            <p>Seleccione el día y la hora de su servicio y obtenga precios instantáneos y asequibles.</p>

          </div>

          <div className="SuBitems">
            <img className="SuBitemsIcons" src="https://imageneslocatefit.s3.eu-west-3.amazonaws.com/reserva.png" alt="2º" />
            <h3>Reserve al instante</h3>
            <p>Confirmaremos su cita y nos encargaremos del pago de forma electrónica y segura.</p>


          </div>


          <div className="SuBitems">
            <img className="SuBitemsIcons" src="https://imageneslocatefit.s3.eu-west-3.amazonaws.com/profesionales.png" alt="3º" />
            <h3>Tu profesional llega</h3>
            <p>Un profesional experimentado y totalmente equipado se presentará a tiempo en su puerta.</p>

          </div>

        </div>


      </div>

      <MobileApp />
      {
        id ?
      <div className="mobile-new-service">
        <a
          title="Publicar"
          onClick={this.showModal}
          className="rounded_button"
          href="#Publicar"
        >
          <Icon className="plusii" type="plus-circle" />      Publicar un servicio
          </a>
      </div>
      : null
      }
      <Modal
        width="70%"
        title="Publicar servicio"
        visible={this.state.visible}
        onCancel={this.handleCancel}
        footer={null}
      >
    

      
        <Publicar /> 
       
      </Modal>
      <Footer />
      </div >
    );
  }
}



export default Landing;
