import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';
import { Icon, Input, AutoComplete } from 'antd';
import { Query } from 'react-apollo';
import { CATEGORIES_QUERY_FOR_AUTOCOMPLETE } from '../../queries';

const { Option, OptGroup } = AutoComplete;

let dataSource = [
  {
    title: 'Categorías',
    children: [],
  },

];

function renderTitle(title) {
  return (
    <span>
      {title}
      <Link
        style={{ float: 'right' }}
        to="/category"
        rel="noopener noreferrer"
      >
        Más
      </Link>
    </span>
  );
}

class SearchHomme extends Component {

  render() {
    return (
      <Query query={CATEGORIES_QUERY_FOR_AUTOCOMPLETE}>
        {({ loading, error, data }) => {
          let options = dataSource
            .map(group => (
              <OptGroup key={group.title} label={renderTitle(group.title)}>
                {group.children.map((opt, i) => (
                  <Option key={i} value={opt.id}>
                    {opt.title}
                  </Option>
                ))}
              </OptGroup>
            ))
            .concat([
              <Option disabled key="all" className="show-all">
                <span>
                  <Link to="/category" rel="noopener noreferrer">
                    Ver todos los resultados
                  </Link>
                </span>
              </Option>,
            ]);

          if (loading) console.log('loading categories');
          if (error) console.log('loading categories error: ', error);
          if (data && data.getCategories) {
            dataSource[0].children = data.getCategories;

            // prepare autocomplete options start
            options = dataSource
              .map(group => (
                <OptGroup key={group.title} label={renderTitle(group.title)}>
                  {group.children.map((opt, i) => (
                    <Option key={i} value={opt.title} id={opt.id}>
                      {opt.title}
                    </Option>
                  ))}
                </OptGroup>
              ))
              .concat([
                <Option disabled key="all" className="show-all">
                  <a href="/category" rel="noopener noreferrer">
                    Ver todos los resultados
                  </a>
                </Option>,
              ]);
            // prepare autocomplete options end

          }
          return (
            <div className="certain-category-search-wrapper" style={{ width: "100%" }}>
              <AutoComplete
                className="certain-category-search"
                dropdownClassName="certain-category-search-dropdown"
                dropdownMatchSelectWidth={false}
                dropdownStyle={{ width: 300 }}
                size="large"
                style={{ width: '100%' }}
                dataSource={options}
                placeholder="¿Qué necesitas?"
                optionLabelProp="value"
                filterOption={(inputValue, option) => {
                  if (typeof option.props.children === 'string')
                    return option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                  if (typeof option.props.children !== 'string' && option.key === 'all')
                    return option;
                }}
                onSelect={(value, option) => {
                  this.props.history.push('/services/' + option.props.id);
                }}
              >
                <Input suffix={<Icon type="search" className="certain-category-icon" />} />
              </AutoComplete>
            </div>
          )

        }}
      </Query>
    )
  }
}

export default withRouter(SearchHomme);