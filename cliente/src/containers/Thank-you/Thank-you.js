import React, { Component } from "react";
import Footer from "./../../components/layout/Footer";
import "antd/dist/antd.css";
import { Result, Button } from "antd";

import "./Thank-you.css";

class ThankYou extends Component {
  render() {
    return (
      <div>
      <Result
          status="success"
          title="¡Profesional contratado con éxito en unos minutos te estará tocando el telefonillo!"
          subTitle="En unos minutos nuestro prefesional estara aceptando la orden."
          extra={[
            <Button href="/profile" className="thankyou-page1" type="primary" key="console">
              Ir a mis pedidos
            </Button>,
            <Button href="/category" className="thankyou-page1" key="buy">
              Contratar otro profesional
            </Button>
          ]}
        />

        <Footer />
      </div>
    );
  }
}

export default ThankYou;



