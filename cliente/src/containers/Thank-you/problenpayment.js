import React, { Component } from "react";
import Footer from "../../components/layout/Footer";
import "antd/dist/antd.css";
import { Result, Button } from "antd";

import "./Thank-you.css";

class ProblemPayment extends Component {
  render() {
    return (
      <div>
      <Result
          status="error"
          title="¡Hay algunos problemas con su operación.!"
          extra={[
            <Button href="/" className="thankyou-page1" type="primary" key="console">
              Volver al inicio
            </Button>
          ]}
        />

        <Footer />
      </div>
    );
  }
}

export default ProblemPayment;

