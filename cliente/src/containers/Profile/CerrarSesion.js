import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { withRouter } from 'react-router-dom';

import { Button, Icon } from 'antd';

const cerrarSesionUsuario = (cliente, history) => {
    localStorage.removeItem('token', '');
    localStorage.removeItem('id', '');
    
    // Desloguear
    cliente.resetStore();

    history.push('/login');
}


const CerrarSesion = ({active_index, history}) => {

        const index = active_index;

       

        return (
            <ApolloConsumer>

            { cliente => {
                 console.log('cliente: ', cliente)
                return (
                    <Button
                    onClick={() => cerrarSesionUsuario(cliente, history)}
                    className={index === 10 ? "active simple" : "simple"}
                    style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center"
                    }}
                >
                    <Icon type="logout" />
                    
                    <h3 className="primary">CERRAR SESIÓN</h3>
              </Button>
                )
            }}

            </ApolloConsumer>
        )
}

 export default withRouter(CerrarSesion);