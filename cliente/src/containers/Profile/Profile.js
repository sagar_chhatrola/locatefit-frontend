import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import "./Topnav.css";
import Footer from "./../../components/layout/Footer";
import Address from "./../../components/Address/Address";
import ChatList from "../../components/ChatList/ChatList";
import PaymentType from "./../../components/Payment-Type/Payment-Type";
import Publish from "./../../components/Publish/Publish";
import Orders from "./../../components/Orders/Orders";
import MyAds from "./../../components/MyAds/MyAds";
import AccountDetail from "./../../components/Account-Detail/Account-Details";
import Order from "./../../components/Order/Order";
import MyStats from "../../components/My-Stats/My-Stats";
import MyProfile from "../../components/My-profile/My-profile";
import "./Profile.css";
import session from './../../components/Session';
import { Button, Icon } from "antd";
import CerrarSesion from "./CerrarSesion";
import Estadisticas from '../../components/estadisticas/estadisticas';


class Profile extends Component {

  state = {
    name: "",
    email: "",
    password: "",
    password2: "",
    errors: {},
    active_index: 1,
    // active_index: 0, // temporary for testing 
    menuShown: false
  };

  

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };

    this.props.registerUser(newUser, this.props.history);
  };
  changeTab = index => {
    this.setState({ active_index: index });
  };

  toggleMenuMobile = () =>
    this.setState(state => ({ menuShown: !state.menuShown }));

  render() {
    const { active_index } = this.state;
    return (
      <div>
        <div className="content main-wrapper">

          <div
            className={`topnav ${this.state.menuShown ? "visible-mobile" : ""}`}
          >
            {/* <li onClick={() => this.changeTab(8)} className={active_index === 8 ? "active" : ''}>
                  <span href="#"><img href="#miprefil" className="FOTOMenu" src={profileImg} /><br />PERFIL</span>
                </li> */}
            <div className="toggle-nav" onClick={this.toggleMenuMobile}>
              <Icon type="menu" />
            </div>
            <div className="nav-items" onClick={this.toggleMenuMobile}>
              <Button
                className={active_index === 1 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(1)}
              >
                <Icon type="user" />
                <h3 className="primary">MI CUENTA</h3>
              </Button>
              <Button
                className={active_index === 2 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(2)}
              >
                <Icon type="file-text" />
                <h3 className="primary">ORDENES</h3>
              </Button>

              <Button
                className={active_index === 3 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(3)}
              >
                <Icon type="file-text" />
                <h3 className="primary">PEDIDOS</h3>
              </Button>

              <Button
                className={active_index === 4 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(4)}
              >
                <Icon type="euro" />
                <h3 className="primary">MIS PAGOS</h3>
              </Button>
              <Button
                className={active_index === 9 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(9)}
              >
                <Icon type="bar-chart" />
                <h3 className="primary">ESTADÍSTICA</h3>
              </Button>

              <Button
                className={active_index === 5 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(5)}
              >
                <Icon type="contacts" />
                <h3 className="primary">DIRECCIÓN</h3>
              </Button>

              <Button
                className={active_index === 6 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(6)}
              >
                <Icon type="plus" />
                <h3 className="primary">PUBLICAR</h3>
              </Button>

              <Button
                className={active_index === 7 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(7)}
              >
                <Icon type="notification" />
                <h3 className="primary">ANUNCIOS</h3>
              </Button>
              <Button
                className={active_index === 0 ? "active simple" : "simple"}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onClick={() => this.changeTab(0)}
              >
                <Icon type="star" />
                <h3 className="primary">OPINIONES</h3>
              </Button>

              <CerrarSesion activeIndex={this.state.active_index} />
            </div>
          </div>
          <div className="page-content">
            {active_index === 0 ? <MyStats /> : null}
            {active_index === 1 ? <AccountDetail /> : null}
            {active_index === 2 ? <Orders /> : null}
            {active_index === 3 ? <Order /> : null}
            {active_index === 4 ? <PaymentType /> : null}
            {active_index === 5 ? <Address /> : null}
            {active_index === 6 ? <Publish /> : null}
            {active_index === 7 ? <MyAds /> : null}
            {active_index === 8 ? <MyProfile /> : null}
            {active_index === 9 ? <Estadisticas /> : null}
            {active_index === 10 ? <ChatList /> : null}
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}


export default Profile
