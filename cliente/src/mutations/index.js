import gql from 'graphql-tag';

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID!) {
    readNotification(notificationId: $notificationId) {
      success
      message
    }
  }
`;

export const CREATE_MESSAGE_CHAT = gql`
  mutation messageChat($input: messageChatInput) {
    messageChat(input: $input) {
      success
      message
    }
  }
`;

export const DELETE_CHAT = gql`
  mutation deleteChat($messageId: ID!) {
    deleteChat(messageId: $messageId) {
      success
      message
    }
  }
`;

export const CREAR_USUARIO_FAVORITE_PRODUCTO = gql`
  mutation crearUsuarioFavoritoProducto($productoId: ID!) {
    crearUsuarioFavoritoProducto(productoId: $productoId) {
      success
      message
    }
  }
`;

export const ELIMINAR_USUARIO_FAVORITE_PRODUCTO = gql`
  mutation eliminarUsuarioFavoritoProducto($id: ID!) {
    eliminarUsuarioFavoritoProducto(id: $id) {
      success
      message
    }
  }
`;

export const NUEVO_USUARIO_DIRECCION = gql`
  mutation crearUsuarioDireccion($input: UsuarioDireccionInput) {
    crearUsuarioDireccion(input: $input) {
      success
      message
      data {
        id
        nombre
        calle
        ciudad
        provincia
        codigopostal
        telefono
      }
    }
  }
`;

export const NUEVO_USUARIO_VALORACION = gql`
  mutation crearUsuarioValoracion($input: valoraciones) {
    crearUsuarioValoracion(input: $input) {
      success
      message
      list {
        id
        coment
        rating
        cliente
        likes
        dislikes
        Orden
        producto
        profesional
      }
    }
  }
`;


export const ELIMINAR_USUARIO_DEPOSITO = gql`
  mutation eliminarUsuarioDeposito($id: ID!) {
    eliminarUsuarioDeposito(id: $id) {
      success
      message
    }
  }
`;

export const ELIMINAR_USUARIO_DIRECCION = gql`
  mutation eliminarUsuarioDireccion($id: ID!) {
    eliminarUsuarioDireccion(id: $id) {
      success
      message
    }
  }
`;

export const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID!) {
    eliminarPago(id: $id) {
      success
      message
    }
  }
`;

export const NUEVO_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      success
      message
      data {
        id
        nombre
        iban
      }
    }
  }
`;

export const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const NUEVO_CATEGORY = gql`
  mutation crearCategory($input: CategoryInput) {
    crearCategory(input: $input) {
      title
      image
      description
    }
  }
`;

export const NUEVO_INSPIRATION = gql`
  mutation crearInspiration($input: InspirationInput) {
    crearInspiration(input: $input) {
      title
      image
      image1
      image2
      description
    }
  }
`;

export const ELIMINAR_CATEGORY = gql`
  mutation eliminarCategory($id: ID!) {
    eliminarCategory(id: $id)
  }
`;

export const NUEVO_PRODUCT = gql`
  mutation crearProducto($input: ProductoInput) {
    crearProducto(input: $input) {
      success
      message
      data {
        city
        category_id
        currency
        description

        domingo
        domingo_from
        domingo_to

        jueves
        jueves_from
        jueves_to

        lunes
        lunes_from
        lunes_to

        martes
        martes_from
        martes_to

        miercoles
        miercoles_from
        miercoles_to

        number

        sabado
        sabado_from
        sabado_to

        time
        title

        viernes
        viernes_from
        viernes_to

        fileList

        created_by
      }
    }
  }
`;

export const EDITAR_PRODUCT = gql`
  mutation editarProducto($input: EditarProductoInput) {
    editarProducto(input: $input) {
      success
      message
      data {
        city
        category_id
        currency
        description

        domingo
        domingo_from
        domingo_to

        jueves
        jueves_from
        jueves_to

        lunes
        lunes_from
        lunes_to

        martes
        martes_from
        martes_to

        miercoles
        miercoles_from
        miercoles_to

        number

        sabado
        sabado_from
        sabado_to

        time
        title

        viernes
        viernes_from
        viernes_to

        fileList

        created_by
      }
    }
  }
`;

export const ELIMINAR_PRODUCT = gql`
  mutation eliminarProducto($id: ID!) {
    eliminarProducto(id: $id) {
      success
      message
    }
  }
`;

export const NUEVO_CLIENTE = gql`
  mutation crearCliente($input: ClienteInput) {
    crearCliente(input: $input) {
      nombre
      apellido
      email
      telefonos {
        telefono
      }
    }
  }
`;

export const ACTUALIZAR_CLIENTE = gql`
  mutation actualizarCliente($input: ClienteInput) {
    actualizarCliente(input: $input) {
      id
      nombre
      apellido
      email
      telefonos {
        telefono
      }
      reservas {
        dia
        horainicio
        horafin
        tipo
      }
    }
  }
`;

export const ELIMINAR_CLIENTE = gql`
  mutation eliminarCliente($id: ID!) {
    eliminarCliente(id: $id) {
      success
      message
    }
  }
`;

export const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      message
    }
  }
`;

export const ACTUALIZAR_PRODUCTO_VISITAR = gql`
  mutation actualizarProductoVisitar($input: ProductVisitarInput) {
    actualizarProductoVisitar(input: $input) {
      success
      message
    }
  }
`;

/* export const NUEVO_USUARIO = gql`
    mutation crearUsuario($usuario: String!, $password: String!){
        crearUsuario(usuario: $usuario, password: $password)
    }
`; */

export const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        usuario
        password
        email
        nombre
        apellidos
        telefono
        tipo
      }
    }
  }
`;

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarUsuario($email: String!, $password: String!) {
    autenticarUsuario(email: $email, password: $password) {
      success
      message
      data {
        token
        id
      }
    }
  }
`;

export const CREAR_MODIFICAR_ORDEN = gql`
  mutation crearModificarOrden($input: OrdenCreationInput) {
    crearModificarOrden(input: $input) {
      id
      cupon
      nota
      aceptaTerminos
      direccion {
        id
        nombre
        calle
        ciudad
        provincia
        codigopostal
        telefono
        usuario_id
      }
      producto
      cantidad
      cliente
      profesional
      descuento {
        clave
        descuento
        tipo
      }
    }
  }
`;

export const AGREGAR_PAGO_PAYPAL_ORDEN = gql`
  mutation agregarPagoPaypalOrden($input: AgregarPagoOrdenInput) {
    agregarPagoPaypalOrden(input: $input) {
      id
      cantidad
      pagoPaypal {
        idPago
        estadoPago
      }
    }
  }
`;

export const PROFESSIONAL_ORDEN_PROCEED = gql`
  mutation ordenProceed($ordenId: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String, $coment: String, $rate: Int, $descripcionproblem: String) {
    ordenProceed(ordenId: $ordenId, estado: $estado, progreso: $progreso, status: $status, nota: $nota, coment: $coment, rate: $rate, descripcionproblem: $descripcionproblem) {
      success
      message
    }
  }
`;
