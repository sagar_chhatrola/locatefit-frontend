import React, { Component } from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import Landing from './containers/Landing/Landing';
import Profesionallanding from './containers/profesional/profesinallanding';
import RegistrationForm from './containers/Register/Register';
import LoginForm from './containers/Login/loginform';
import PrivateRoute from './containers/PrivateRoute';
import PublicRoute from './containers/PublicRoute';
import Profile from './containers/Profile/Profile';
import ProductPlan from './containers/Product-Plan/Product-Plan';
import AllProduct from './containers/all-product/all-product';
import Payment from './containers/Payment/Payment';
import Categories from './containers/Categories/Categories';
import Notfound from './containers/notfound404/notfound';
import ThankYou from './containers/Thank-you/Thank-you';
import ErrorPayment from './containers/Thank-you/problenpayment';
import RecoverPassword from './containers/recover-password/recover-password';
import Forgotpublic from './containers/recover-password/forgotpublick';
import Confirmatuemail from './containers/Register/confiremail';
import * as Sentry from '@sentry/browser';

// import ProductDetail  from "./containers/Product-Plan/Product-Plan";

import Session from './components/Session';

import './App.css';
import Header from './components/layout/Header';

Sentry.init({
  dsn: 'https://f955be9c777e4aecb34bb34a01e90f36@sentry.io/1778988'
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favouriteDeletedFromWishListCounter: 0,
      favouriteAddedToWishListCounter: 0
    };

    this.handleOnFavouriteDelete = this.handleOnFavouriteDelete.bind(this);
  }

  handleOnFavouriteDelete = () => {
    this.setState({
      favouriteDeletedFromWishListCounter: ++this.state
        .favouriteDeletedFromWishListCounter
    });
  };

  handleOnFavouriteAdd = () => {
    this.setState({
      favouriteAddedToWishListCounter: ++this.state
        .favouriteAddedToWishListCounter
    });
  };

  render() {
    console.log(this.props);
    return (
      <BrowserRouter>
        <div className='App'>
          <Header
            session={this.props.session}
            onFavouriteDelete={this.handleOnFavouriteDelete}
            favouriteAddedToWishListCounter={
              this.state.favouriteAddedToWishListCounter
            }
          />
          <Switch>
            <Route exact path='/' component={Landing} />
            <PublicRoute
              exact
              path='/register'
              refetch={this.props.refetch}
              component={RegistrationForm}
            />
            
            <PublicRoute
              exact
              path='/confirmatuemail'
              component={Confirmatuemail}
            />
            <Route
              exact
              path='/services/:id'
              render={routeProps => (
                <AllProduct {...routeProps} session={this.props.session} />
              )}
            />
            {/* <Route exact path='/services/:id' component={AllProduct} /> */}
            <PublicRoute
              exact
              path='/login'
              component={LoginForm}
              refetch={this.props.refetch}
            />
            <Route exact path='/category' component={Categories} />
            <Route exact path='/recoverpassword' component={RecoverPassword} />
            <Route exact path='/reset/:token' component={Forgotpublic} />
            <Route exact path='/confirm/:token' component={RegistrationForm} />
            <PublicRoute
              exact
              path='/become-profesional'
              component={Profesionallanding}
            />
            <PrivateRoute
              exact
              path='/deals/:id'
              component={ProductPlan}
              favouriteDeletedFromWishListCounter={
                this.state.favouriteDeletedFromWishListCounter
              }
              onFavouriteAdd={this.handleOnFavouriteAdd}
            />
            <PrivateRoute exact path='/profile' component={Profile} />
            <PrivateRoute exact path='/payment/:orden' component={Payment} />
            <PrivateRoute exact path='/thank-you' component={ThankYou} />
            <PrivateRoute
              exact
              path='/error-payment'
              component={ErrorPayment}
            />
            <Route component={Notfound} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const RootSession = Session(App);

export { RootSession };
